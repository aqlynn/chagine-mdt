package chagine.mdt.test;

import com.baomidou.mybatisplus.annotation.*;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author panzhizheng
 * @since 2022-11-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@TableName("TEST_TABLE")
public class TestTable implements Serializable {


    /**
     * 数据ID
     */
    @TableId(value = "ID", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 创建人，doctorID
     */
    @TableField(value = "CREATE_USER", fill = FieldFill.INSERT)
    private String createUser;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField(value = "CHAN_USER", fill = FieldFill.INSERT_UPDATE)
    private String chanUser;

    /**
     * 更新时间
     */
    @TableField(value = "CHAN_TIME", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime chanTime;

    /**
     * 变更引起的请求
     */
    @TableField(value = "CHAN_REQUEST", fill = FieldFill.INSERT_UPDATE)
    private Long chanRequest;

    /**
     * 租户号
     */
    @TableField(value = "TENA_ID", fill = FieldFill.INSERT)
    private String tenaId;

    /**
     * 关联的表名
     */
    @TableField("TEST_DATE")
    private LocalDateTime testDate;

    /**
     * 关联的表名
     */
    @TableField("PARAMS")
    private String params;

}

package chagine.mdt.test;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TestTableMapper extends BaseMapper<TestTable> {
}

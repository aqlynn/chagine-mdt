package chagine.mdt.mapper.yy;

import chagine.mdt.common.DbConstants;
import chagine.mdt.po.yy.YyDxDxtab;
import chagine.mdt.vo.yy.AdmissionDiagnosisV2Vo;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 诊断表 Mapper 接口
 * </p>
 *
 * @author lsf
 * @since 2023-04-13
 */
@DS(DbConstants.CHAGINEORACLE)
@Mapper
public interface YyDxDxtabMapper extends BaseMapper<YyDxDxtab> {
    List<AdmissionDiagnosisV2Vo> listYyDxDxtab(@Param("type") int type, @Param("in_hospital_id") String inHospitalId);
}

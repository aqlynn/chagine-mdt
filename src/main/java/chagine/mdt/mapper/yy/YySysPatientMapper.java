package chagine.mdt.mapper.yy;

import chagine.mdt.common.DbConstants;
import chagine.mdt.po.yy.YySysPatient;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 病人信息表 Mapper 接口
 * </p>
 *
 * @author lsf
 * @since 2023-04-13
 */
@DS(DbConstants.CHAGINEORACLE)
@Mapper
public interface YySysPatientMapper extends BaseMapper<YySysPatient> {

}

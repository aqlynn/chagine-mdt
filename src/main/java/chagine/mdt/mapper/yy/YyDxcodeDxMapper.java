package chagine.mdt.mapper.yy;

import chagine.mdt.common.DbConstants;
import chagine.mdt.po.yy.YyDxcodeDx;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lsf
 * @since 2023-04-13
 */
@DS(DbConstants.CHAGINEORACLE)
@Mapper
public interface YyDxcodeDxMapper extends BaseMapper<YyDxcodeDx> {

}

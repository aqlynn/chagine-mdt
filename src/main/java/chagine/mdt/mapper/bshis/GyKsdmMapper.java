package chagine.mdt.mapper.bshis;

import chagine.mdt.common.DbConstants;
import chagine.mdt.po.bshis.GyKsdm;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


@Mapper
@DS(DbConstants.CHAGINEORACLE)
public interface GyKsdmMapper extends BaseMapper<GyKsdm> {

}

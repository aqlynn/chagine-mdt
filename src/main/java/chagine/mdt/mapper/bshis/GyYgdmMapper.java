package chagine.mdt.mapper.bshis;

import chagine.mdt.common.DbConstants;
import chagine.mdt.po.bshis.GyYgdm;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;


@Mapper
@DS(DbConstants.CHAGINEORACLE)
public interface GyYgdmMapper extends BaseMapper<GyYgdm> {

}

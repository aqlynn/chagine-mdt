package chagine.mdt.vo.test;

import chagine.core.serialize.deserializer.*;
import chagine.core.serialize.serialize.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.Date;

@Data
public class TestVo {

    @JsonProperty("number")
    @JsonSerialize(using = Int2StringSerializer.class)
    @JsonDeserialize(using = String2IntDeserializer.class)
    private Integer number;

    @JsonProperty("date")
    @JsonSerialize(using = Date2TimestampSerializer.class)
    @JsonDeserialize(using = Timestamp2DateDeserializer.class)
    private Date date;

    @JsonProperty("date_formatter")
    @JsonSerialize(using = Timestamp2DateFormatterSerializer.class)
    @JsonDeserialize(using = DateFormatter2TimestampDeserializer.class)
    private Long dateFormatter;

    @JsonProperty("date_time_formatter")
    @JsonSerialize(using = Timestamp2DateTimeFormatterSerializer.class)
    @JsonDeserialize(using = DateTimeFormatter2TimestampDeserializer.class)
    private Long dateTimeFormatter;

    @JsonProperty("date_short_time_formatter")
    @JsonSerialize(using = Timestamp2DateShortTimeFormatterSerializer.class)
    private Long dateShortTimeFormatter;

    @JsonProperty("long_value")
    @JsonSerialize(using = Long2StringSerializer.class)
    @JsonDeserialize(using = String2LongDeserializer.class)
    private Long longValue;

    @JsonProperty("money")
    @JsonSerialize(using = MoneySerializer.class)
    @JsonDeserialize(using = MoneyDeserializer.class)
    private Integer money;

}

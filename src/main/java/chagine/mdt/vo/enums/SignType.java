package chagine.mdt.vo.enums;

/**
 * 签名类型
 */
public enum SignType {
    /**
     * 医生
     */
    DOCTOR(1),
    /**
     * 患者
     */
    PATIENT(2),
    /**
     * 患者家属
     */
    FAMILY(3);

    public final int code;
    public static final SignType[] VALUES = values();

    SignType(int code) {
        this.code = code;
    }

    public static SignType getType(Integer code) {
        if (code != null) {
            for (SignType v : VALUES) {
                if (v.code == code) {
                    return v;
                }
            }
        }
        return null;
    }

}
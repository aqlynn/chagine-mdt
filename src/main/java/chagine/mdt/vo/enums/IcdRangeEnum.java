package chagine.mdt.vo.enums;

/**
 * 数据元范围列表
 */
public enum IcdRangeEnum {

    ICD_10_MAIN("ICD-10", "国际疾病分类代码表(ICD10)"),
    ICD_10_1("ICD-10-1", "症状代码表(ICD10)"),
    ICD_10_2("ICD-10-2", "死因代码表(ICD10)"),
    ICD_10_3("ICD-10-3", "诊断代码表(ICD10)"),
    ICD_10_4("ICD-10-4", "职业病名称代码表(ICD10)"),
    ICD_10_5("ICD-10-5", "疾病代码表(ICD10)"),
    ICD_9_CM("ICD-9-CM", "手术(操作)代码表"),


    ;

    public static final Long TYPE_ID = 20005L;

    public final String code;
    public final String explain;
    private static final IcdRangeEnum[] VALUES = IcdRangeEnum.values();

    IcdRangeEnum(String code, String explain) {
        this.code = code;
        this.explain = explain;
    }

    /**
     * 匹配属性值
     */
    public static IcdRangeEnum matcherByCode(String code) {
        for (IcdRangeEnum v : VALUES) {
            if (v.code.equals(code)) {
                return v;
            }
        }
        return null;
    }

}

package chagine.mdt.vo.docmapping;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocMappingVo {

    /**
     * 类类型
     */
    @JsonIgnore
    private Class<?> clazz;

    /**
     * 类属性
     */
    @JsonIgnore
    private String classAttribute;

    /**
     * 表名称
     */
    @JsonIgnore
    private String tableName;

    /**
     * 字段名称
     */
    @JsonIgnore
    private String fieldName;

    /**
     * 段落编码
     */
    @JsonIgnore
    private String paragraphCode;

    /**
     * 是否是数组
     */
    @JsonIgnore
    private Boolean isArray;

    /**
     * 数据组，存在多组的分组Key，如果不是多组的则不传，或者传 NO_ARRAY_KEY
     */
    @JsonProperty("array_key")
    private String arrayKey;

    /**
     * 数据组Key
     */
    @JsonProperty("node_key")
    private String nodeKey;

    /**
     * 获得根节点的编码
     */
    public String getRootNodeCode() {
        if (StringUtils.isBlank(nodeKey)) {
            return null;
        }
        String[] split = nodeKey.split(">");
        if (split.length == 0) {
            return null;
        }
        return split[0].substring(1);
    }

    /**
     * 数据元Key
     */
    @JsonProperty("element_key")
    private String elementKey;

    /**
     * 数据元的值
     */
    @JsonProperty("value")
    private String value;

}

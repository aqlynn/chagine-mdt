package chagine.mdt.vo.yy;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


/**
 * 住院诊断
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdmissionDiagnosisWithStatusVo {

    @ApiModelProperty(value = "第三方系统是否有该患者:1是,2否,没有数据时当有处理", name = "has_3rd_patient", required = true)
    @JsonProperty("has_3rd_patient")
    private Integer has3rdPatient;

    @ApiModelProperty(value = "诊断列表", name = "admission_diagnosis_list", required = false)
    @JsonProperty(value = "admission_diagnosis_list")
    private List<AdmissionDiagnosisV2Vo> admissionDiagnosisList;

}

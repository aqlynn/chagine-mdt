package chagine.mdt.vo.yy;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 诊断表
 * </p>
 *
 * @author lsf
 * @since 2023-04-13
 */
@Data
@Accessors(chain = true)
public class AdmissionDiagnosisSaveListVo {

    @NotNull(message = "产妇ID不能为空")
    @ApiModelProperty(value = "产妇ID", name = "puerpera_id", required = false)
    @JsonProperty("puerpera_id")
    private Long puerperaId;

//    @JsonProperty("his_doctor_id")
//    private String hisDoctorId;

    /**
     * 20-术前 21-术后
     */
    @NotNull(message = "诊断类别不能为空")
//    @ApiModelProperty(value = "诊断类别: 20术前 21术后", name = "deagnostic_category_type", required = false)
    @JsonProperty(value = "deagnostic_category_type")
    private Integer deagnosticCategoryType;

    @NotNull(message = "诊断列表不能为空")
//    @ApiModelProperty(value = "诊断列表", name = "admission_diagnosis_list", required = false)
    @JsonProperty(value = "admission_diagnosis_list")
    private List<AdmissionDiagnosisV2Vo> admissionDiagnosisList;

}

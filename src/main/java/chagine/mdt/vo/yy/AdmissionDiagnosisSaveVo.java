package chagine.mdt.vo.yy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 诊断表
 * </p>
 *
 * @author lsf
 * @since 2023-04-13
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Accessors(chain = true)
public class AdmissionDiagnosisSaveVo implements Serializable {

    private static final long serialVersionUID = 1L;

//    @ApiModelProperty(value = "诊断表编号---主键ID", name = "id", required = false)
    @JsonProperty("id")
    private String id;

//    @ApiModelProperty(value = "病人序号", name = "patientid", required = true)
//    @JsonProperty("patientid")
//    private String patientid;

//    @ApiModelProperty(value = "诊断序号", name = "dxindex", required = true)
//    @JsonProperty("dxindex")
//    private String dxindex;

//    @ApiModelProperty(value = "诊断代码", name = "dxcode", required = true)
//    @JsonProperty("dxcode")
//    private String dxcode;

//    @ApiModelProperty(value = "ICD编码", name = "icd", required = true)
//    @JsonProperty("icd")
//    private String icd;

//    @ApiModelProperty(value = "诊断名称", name = "dxname", required = true)
//    @JsonProperty("dxname")
//    private String dxname;

//    @ApiModelProperty(value = "诊断名称补充", name = "dxnamesup", required = true)
//    @JsonProperty("dxnamesup")
//    private String dxnamesup;

//    @ApiModelProperty(value = "诊断确诊标记 1确认 2待查 0疑似", name = "dxdiagnosis", required = true)
    @JsonProperty("dxdiagnosis")
    private String dxdiagnosis;

//    @ApiModelProperty(value = "诊断治疗效果", name = "resultid", required = true)
//    @JsonProperty("resultid")
//    private String resultid;

//    @ApiModelProperty(value = "诊断类别 0初步 55补充 1修正 2入院 3出院 20术前 21术后 300 病案首页", name = "dxcategory", required = true)
//    @JsonProperty("dxcategory")
//    private String dxcategory;

//    @ApiModelProperty(value = "诊断医师ID", name = "doctorid", required = true)
//    @JsonProperty("doctorid")
//    private String doctorid;


//    @ApiModelProperty(value = "父ID", name = "fatherid", required = true)
//    @JsonProperty("fatherid")
//    private String fatherid;
//
//    @ApiModelProperty(value = "手术ID", name = "operationid", required = true)
//    @JsonProperty("operationid")
//    private String operationid;
//
//    @ApiModelProperty(value = "主手术ID", name = "mainoperationid", required = true)
//    @JsonProperty("mainoperationid")
//    private String mainoperationid;


    /**
     * ICD10
     */
//    @ApiModelProperty(value = "ICD10", name = "icd10", required = false)
    @JsonProperty(value = "icd10")
    private String icd10;

    /**
     * 诊断名称
     */
//    @ApiModelProperty(value = "诊断名称", name = "diagnostic_name", required = false)
    @JsonProperty(value = "diagnostic_name")
    private String diagnosticName;

    /**
     * 补充诊断
     */
//    @ApiModelProperty(value = "补充诊断", name = "supplementary_diagnosis", required = false)
    @JsonProperty(value = "supplementary_diagnosis")
    private String supplementaryDiagnosis;

    /**
     * 排序越小越靠前
     */
    @JsonIgnore
    private Integer indexNo;

}

package chagine.mdt.vo.yy;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 住院诊断
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdmissionDiagnosisV2Vo {

    @ApiModelProperty(value = "ID 保存时会用到", name = "id", required = true)
    @JsonProperty("id")
    private String id;

    @ApiModelProperty(value = "住院号", name = "in_hospital_id", required = true)
    @JsonProperty("in_hospital_id")
    private String inHospitalId;

    /**
     * 诊断时间
     */
    @ApiModelProperty(value = "诊断时间:yyyy-MM-dd HH:mm:ss", name = "diagnostic_time", required = true)
    @JsonProperty(value = "diagnostic_time")
    private String diagnosticTime;

    /**
     * ICD10
     */
    @ApiModelProperty(value = "ICD10 保存时会用到", name = "icd10", required = true)
    @JsonProperty(value = "icd10")
    private String icd10;

    /**
     * 诊断名称
     */
    @ApiModelProperty(value = "诊断名称 保存时会用到", name = "diagnostic_name", required = true)
    @JsonProperty(value = "diagnostic_name")
    private String diagnosticName;

    /**
     * 补充诊断
     */
    @ApiModelProperty(value = "补充诊断:保存时会用到", name = "supplementary_diagnosis", required = true)
    @JsonProperty(value = "supplementary_diagnosis")
    private String supplementaryDiagnosis;

    /**
     * 1-初步诊断 2-修正诊断 3-补充诊断 20-术前 21-术后
     */
    @ApiModelProperty(value = "诊断类别:1-初步诊断 2-修正诊断 3-补充诊断 20-术前 21-术后", name = "deagnostic_category_type", required = true)
    @JsonProperty(value = "deagnostic_category_type")
    private Integer deagnosticCategoryType;

    /**
     * 诊断类别（初步诊断、修正诊断、补充诊断）
     */
    @ApiModelProperty(value = "诊断类别（初步诊断、修正诊断、补充诊断、术前、术后）", name = "deagnostic_category_name", required = true)
    @JsonProperty(value = "deagnostic_category_name")
    private String deagnosticCategoryName;

    /**
     * 排序越小越靠前
     */
    @ApiModelProperty(value = "排序越小越靠前", name = "index_no", required = true)
    @JsonProperty(value = "index_no")
    private Integer indexNo;

    /**
     * his医生姓名
     */
    @ApiModelProperty(value = "his医生姓名", name = "doctor_name", required = true)
    @JsonProperty(value = "doctor_name")
    private String doctorName;

    /**
     * his医生
     */
    @ApiModelProperty(value = "his医生", name = "doctor_id", required = true)
    @JsonProperty(value = "doctor_id")
    private String doctorId;


    /**
     * 诊断确诊标记 1确认 2待查 0疑似
     */
    @ApiModelProperty(value = "诊断确诊标记 1确认 2待查 0疑似 保存时会用到", name = "dxdiagnosis", required = true)
    @JsonProperty("dxdiagnosis")
    private String dxdiagnosis;

}

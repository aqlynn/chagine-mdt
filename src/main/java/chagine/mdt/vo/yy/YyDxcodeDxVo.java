package chagine.mdt.vo.yy;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lsf
 * @since 2023-04-13
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "YyDxcodeDxVo", description = "")
public class YyDxcodeDxVo implements Serializable {

    private static final long serialVersionUID = 1L;

//    @ApiModelProperty(value = "icd编码", name = "dxcode", required = false)
//    @JsonProperty("dxcode")
//    private String dxcode;

    @ApiModelProperty(value = "诊断名称", name = "dxname", required = true)
    @JsonProperty("dxname")
    private String dxname;

    @ApiModelProperty(value = "ICD10码", name = "icd10", required = true)
    @JsonProperty("icd10")
    private String icd10;

//    @ApiModelProperty(value = "ICD9码", name = "icd9", required = true)
//    @JsonProperty("icd9")
//    private String icd9;

    @ApiModelProperty(value = "拼音码", name = "inputcode1", required = true)
    @JsonProperty("inputcode1")
    private String inputcode1;

    @ApiModelProperty(value = "五笔码", name = "inputcode2", required = true)
    @JsonProperty("inputcode2")
    private String inputcode2;

    @ApiModelProperty(value = "自定交码", name = "inputcode3", required = true)
    @JsonProperty("inputcode3")
    private String inputcode3;

//    @ApiModelProperty(value = "病案系统序号", name = "mrnumber", required = true)
//    @JsonProperty("mrnumber")
//    private String mrnumber;
//
//    @ApiModelProperty(value = "科室代码", name = "deptcode", required = true)
//    @JsonProperty("deptcode")
//    private String deptcode;
//
//    @ApiModelProperty(value = "报卡类型", name = "reportcard_type", required = true)
//    @JsonProperty("reportcard_type")
//    private String reportcardType;
//
//    @ApiModelProperty(value = "感染排序", name = "reportcard_category", required = true)
//    @JsonProperty("reportcard_category")
//    private String reportcardCategory;
//
//    @ApiModelProperty(value = "描述", name = "description", required = true)
//    @JsonProperty("description")
//    private String description;
//
//    @ApiModelProperty(value = "系统级别类型", name = "syslevel", required = true)
//    @JsonProperty("syslevel")
//    private Integer syslevel;
//
//    @ApiModelProperty(value = "疾病统计码9", name = "levelstatistics9", required = true)
//    @JsonProperty("levelstatistics9")
//    private Integer levelstatistics9;
//
//    @ApiModelProperty(value = "疾病统计码10", name = "levelstatistics10", required = true)
//    @JsonProperty("levelstatistics10")
//    private Integer levelstatistics10;
//
//    @ApiModelProperty(value = "作废标志", name = "invalidstate", required = true)
//    @JsonProperty("invalidstate")
//    private Integer invalidstate;
//
//    @ApiModelProperty(value = "报卡类别", name = "infect_sort", required = true)
//    @JsonProperty("infect_sort")
//    private Integer infectSort;

}

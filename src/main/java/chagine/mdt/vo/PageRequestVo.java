package chagine.mdt.vo;

import chagine.core.ErrorCode;
import chagine.core.util.Assert;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * @program: search
 * @description: 分页类
 * @author: wba
 * @create: 2019-10-12 11:23
 **/
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PageRequestVo implements Serializable {
    // 分页
    private static final int PAGE_NUM_MIN = 1;
    private static final int PAGE_NUM_MAX = 1000;
    private static final int PAGE_SIZE_MIN = 1;
    private static final int PAGE_SIZE_MAX = 100;
    
    /**
     * 当前页码
     */
    @JsonProperty("page_num")
    private Integer pageNum;

    /**
     * 分页大小
     */
    @JsonProperty("page_size")
    private Integer pageSize;

    public static PageRequestVo valueOf(int pageNum, int pageSize) {
        Assert.error(pageNum < PAGE_NUM_MIN || pageNum > PAGE_NUM_MAX,
                ErrorCode.ILLEGALARGUMENT_EXCEPTION, String.format("页码不能超过范围【%d-%d】", PAGE_NUM_MIN, PAGE_NUM_MAX));
        Assert.error(pageSize < PAGE_SIZE_MIN || pageNum > PAGE_SIZE_MAX,
                ErrorCode.ILLEGALARGUMENT_EXCEPTION, String.format("每页数量不能超过范围【%d-%d】", PAGE_SIZE_MIN, PAGE_SIZE_MAX));
        return new PageRequestVo(pageNum, pageSize);
    }
}

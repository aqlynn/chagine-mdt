package chagine.mdt.vo;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @program: search
 * @description: 分页类
 * @author: wba
 * @create: 2019-10-12 11:23
 **/
@Getter
@Setter
public class PageResponseVo<T> implements Serializable {
    /**
     * 分页大小
     */
    @JsonProperty("page_size")
    private Integer pageSize;
    /**
     * 当前页码
     */
    @JsonProperty("page_num")
    private Integer pageNum;
    /**
     * 页码总数
     */
    private Long pages;
    /**
     * 总行数
     */
    private Long total;
    /**
     * 列表内容
     */
    private List<T> list;

    /**
     * 是否存在上一页
     */
    @JsonProperty("has_previous")
    private Boolean hasPrevious;
    /**
     * 是否存在下一页
     */
    @JsonProperty("has_next")
    private Boolean hasNext;

    /**
     * 设置基本的翻页数据
     */
    public static <T, V> void setPageValue(PageResponseVo<V> pageVo, IPage<T> page, List<V> outList) {
        pageVo.setPageNum((int) page.getCurrent());
        pageVo.setPages(page.getPages());
        pageVo.setPageSize((int) page.getSize());
        pageVo.setTotal(page.getTotal());
        pageVo.setList(outList);
    }

    public static <V> PageResponseVo<V> valueOf(IPage<?> page, List<V> outList) {
        PageResponseVo<V> pageVo = new PageResponseVo<>();
        pageVo.setPageNum((int) page.getCurrent());
        pageVo.setPages(page.getPages());
        pageVo.setPageSize((int) page.getSize());
        pageVo.setTotal(page.getTotal());
        pageVo.setList(outList);
        return pageVo;
    }
}

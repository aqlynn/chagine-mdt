package chagine.mdt.vo.common;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 医生或护士信息
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiDoctorVo {

    /**
     * HIS医生ID
     */
    @JsonProperty(value = "doctor_id")
    private String doctorId;

    /**
     * 医生姓名
     */
    @JsonProperty(value = "doctor_name")
    private String doctorName;


    /**
     * 科室编码
     */
    @JsonProperty(value = "depa_code")
    private String depaCode;

    /**
     * 科室名称
     */
    @JsonProperty(value = "depa_name")
    private String depaName;


    /**
     * 联系电话
     */
    @JsonProperty(value = "phone")
    private String phone;


    /**
     * 人员类别 1-医生 2-护士 3-其他
     */
    @JsonProperty(value = "user_type")
    private Integer userType;

    /**
     * 拼音代码
     */
    @JsonProperty(value = "pinyin")
    private String pinyin;

    /**
     * 职称
     */
    @JsonProperty(value = "title")
    private String title;

    /**
     * 擅长
     */
    @JsonProperty(value = "good_at")
    private String goodAt;

}

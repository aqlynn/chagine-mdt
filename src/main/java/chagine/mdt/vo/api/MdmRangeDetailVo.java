package chagine.mdt.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 值域详情 oid
 * @TableName MDM_RANGE_DETAIL
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MdmRangeDetailVo {

    /**
     * 数据ID
     */
    private Integer id;

    /**
     * 值域ID
     */
    private Integer rangeId;

    /**
     * 父ID
     */
    private String parentCode;

    /**
     * 值域代码(国标)自定义建议使用code+version检索
     */
    private String code;

    /**
     * 值域名称
     */
    private String name;

    /**
     * 排列序号
     */
    private Integer sortId;

    /**
     * 拼音代码
     */
    private String pinyin;

    /**
     * 备注
     */
    private String remark;

    /**
     * 1作废 0默认
     */
    private Integer isDelete;

    /**
     * 临时
     */
    private String isnumber;

    /**
     * 子节点
     */
    private List<MdmRangeDetailVo> children;
}
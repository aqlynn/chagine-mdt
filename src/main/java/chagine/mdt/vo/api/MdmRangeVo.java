package chagine.mdt.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 值域名称
 * @TableName MDM_RANGE
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MdmRangeVo {

    /**
     * 数据ID
     */
    private Integer id;

    /**
     * GB国标/CI/WS卫生/CU自定义
     */
    private String typeCode;

    /**
     * 值域代码(国标)自定义建议使用code+version检索
     */
    private String code;

    /**
     * 值域编号(国标)
     */
    private String oid;

    /**
     * 版本说明
     */
    private String version;

    /**
     * 值域名称
     */
    private String name;

    /**
     * 拼音码
     */
    private String pinyin;

    /**
     * 排列序号
     */
    private Integer sortId;

    /**
     * 是否当年使用版本
     */
    private Integer currentVersion;

    /**
     * 0标准值域表 1查询简单值域 2拼音码 3上下级 4上下级拼音码
     */
    private Integer queryType;

    /**
     * SQL查询语句字段名约定(ld,Sortld,NameParentld,PinYin)
     */
    private String querySql;

    /**
     * 值域代码分类
     */
    private String rangeType;

    /**
     * 定义
     */
    private String definition;

    /**
     * 备注
     */
    private String remark;

    /**
     * 删除标志
     */
    private Integer isDelete;

    List<MdmRangeDetailVo> mdmRangeDetailVos;
}
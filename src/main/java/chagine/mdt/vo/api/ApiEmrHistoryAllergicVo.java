package chagine.mdt.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 产时系统-交接记录单-过敏史(医院那边的数据)
 * </p>
 *
 * @author lsf
 * @since 2023-03-13
 */
@Data
@Accessors(chain = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiEmrHistoryAllergicVo {

    /**
     * 数据ID
     */
    @JsonProperty(value = "id")
    private Long id;

    /**
     * empiID
     */
    @JsonProperty(value = "empi_id")
    private Long empiId;

    /**
     * 文档索引号:唯一标识一份临床文档
     */
    @JsonProperty(value = "doc_id")
    private Long docId;

    /**
     * 标识临床文档在业务库中是否被注销1有效0注销
     */
    @JsonProperty(value = "effective_flag")
    private Integer effectiveFlag;

    /**
     * 业务发生时间
     */
    @JsonProperty(value = "effective_time")
    private Date effectiveTime;

    /**
     * 状态 0疑似，1确定
     */
    @JsonProperty(value = "allergic_state")
    private Integer allergicState;

    /**
     * CV05_01_038 致敏成分代码
     */
    @JsonProperty(value = "allergic_code")
    private String allergicCode;

    /**
     * 致敏成分名称
     */
    @JsonProperty(value = "allergic_name")
    private String allergicName;

    /**
     * 详情
     */
    @JsonProperty(value = "allergic_detail")
    private String allergicDetail;

    /**
     * CC51_02_001 个体过敏症状的定性严重程度
     */
    @JsonProperty(value = "allergic_severity")
    private String allergicSeverity;

    /**
     * 对个体出现过敏症状的详细描述
     */
    @JsonProperty("allergic_symptoms")
    private String allergicSymptoms;

    /**
     * 具体过敏成分名称，药物填药物名称，食物可以不填
     */
    @JsonProperty("allergen_name")
    private String allergenName;

    /**
     * 创建时间
     */
    @JsonProperty("create_time")
    private Date createTime;

    /**
     * 创建人员工号
     */
    @JsonProperty("create_employee_no")
    private String createEmployeeNo;

    /**
     * 创建人员姓名
     */
    @JsonProperty("create_employee_name")
    private String createEmployeeName;

    /**
     * 更新时间
     */
    @JsonProperty("modify_time")
    private Date modifyTime;

    /**
     * 更新人员工号
     */
    @JsonProperty("modify_employee_no")
    private String modifyEmployeeNo;

    /**
     * 更新人员姓名
     */
    @JsonProperty("modify_employee_name")
    private String modifyEmployeeName;
}

package chagine.mdt.vo.api;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 住院病人
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiInpatientSimpleVo {

    /**
     * 住院号（住院唯一标识，不用展示给用户看）
     */
    private String inpatientId;

    /**
     * 住院号码
     */
    private String inpatientNo;

    /**
     * 病人ID
     */
    private String patientId;

    /**
     * 病人姓名
     */
    private String patientName;

    /**
     * 主诊医生ID
     */
    private String doctorId;

    /**
     * 主诊医生姓名
     */
    private String doctorName;

    /**
     * 病人病区代码
     */
    private String wardCode;

    /**
     * 病人病区名称
     */
    private String wardName;

    /**
     * 病人床号
     */
    private String bedNumber;

    /**
     * 诊断
     */
    private String diagnoses;
}

package chagine.mdt.common;

/**
 * 数据库源
 */
public class DbConstants {

    public static final String CHAGINEMYSQL = "chagineMysql";

    public static final String CHAGINEORACLE = "chagineOracle";
}

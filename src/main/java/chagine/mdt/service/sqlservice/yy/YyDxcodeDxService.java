package chagine.mdt.service.sqlservice.yy;

import chagine.core.ErrorCode;
import chagine.core.util.Assert;
import chagine.mdt.mapper.yy.YyDxcodeDxMapper;
import chagine.mdt.po.yy.YyDxcodeDx;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lsf
 * @since 2023-04-13
 */
@Service
public class YyDxcodeDxService extends ServiceImpl<YyDxcodeDxMapper, YyDxcodeDx> {

    public YyDxcodeDx getByIcd10(String icd10) {
        YyDxcodeDx one = getOne(
                Wrappers.<YyDxcodeDx>lambdaQuery()
                        .eq(YyDxcodeDx::getIcd10, icd10)
                        .eq(YyDxcodeDx::getInvalidstate, "0"),
                true
        );
        Assert.error(one == null, ErrorCode.BUSINESS_EXCEPTION, "ICD10码不存在");
        return one;
    }

}

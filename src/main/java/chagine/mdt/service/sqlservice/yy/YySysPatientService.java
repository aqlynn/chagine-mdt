package chagine.mdt.service.sqlservice.yy;

import chagine.core.ErrorCode;
import chagine.core.util.Assert;
import chagine.mdt.mapper.yy.YySysPatientMapper;
import chagine.mdt.po.yy.YySysPatient;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 病人信息表 服务实现类
 * </p>
 *
 * @author lsf
 * @since 2023-04-13
 */
@Service
public class YySysPatientService extends ServiceImpl<YySysPatientMapper, YySysPatient> {


}

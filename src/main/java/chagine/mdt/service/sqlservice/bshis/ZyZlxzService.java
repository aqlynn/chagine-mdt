package chagine.mdt.service.sqlservice.bshis;

import chagine.core.ErrorCode;
import chagine.core.util.Assert;
import chagine.mdt.mapper.bshis.ZyZlxzMapper;
import chagine.mdt.po.bshis.ZyZlxz;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class ZyZlxzService extends ServiceImpl<ZyZlxzMapper, ZyZlxz> {

    /**
     * 诊疗小组-单条查询
     *
     * @param xzxh     小组序号
     * @param required 必要标志
     */
    public ZyZlxz getByXzxh(String xzxh, boolean required) {
        ZyZlxz one = lambdaQuery().eq(ZyZlxz::getXzxh, xzxh).one();
        Assert.error(required && one == null, ErrorCode.BUSINESS_EXCEPTION, String.format("诊疗小组不存在, xzxh:" + xzxh));
        return one;
    }



    public List<String> getZyksdmList(Integer yqbz) {
        List<ZyZlxz> ksdmlist = list(
                Wrappers.<ZyZlxz>lambdaQuery()
                        .select(ZyZlxz::getSsks)
                        .eq(yqbz != null,ZyZlxz::getYqbz,yqbz)
                        .groupBy(ZyZlxz::getSsks));
        return ksdmlist.stream().map(ZyZlxz::getSsks).collect(Collectors.toList());
    }
}
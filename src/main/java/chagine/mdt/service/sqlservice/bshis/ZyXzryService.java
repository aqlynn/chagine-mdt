package chagine.mdt.service.sqlservice.bshis;

import chagine.core.ErrorCode;
import chagine.core.util.Assert;
import chagine.mdt.mapper.bshis.ZyXzryMapper;
import chagine.mdt.po.bshis.ZyXzry;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.List;


@Service
public class ZyXzryService extends ServiceImpl<ZyXzryMapper, ZyXzry> {


    /**
     * 当月诊疗小组
     */
    public ZyXzry getThisMonthZyXzryByEmployeeNo(String employeeNo) {
        return getMonthZyXzry(employeeNo, LocalDateTime.now());
    }


    /**
     * 下月诊疗小组
     */
    public ZyXzry getNextMonthZyXzryByEmployeeNo(String employeeNo) {
        return getMonthZyXzry(employeeNo, LocalDateTime.now().plusMonths(1));
    }


    public ZyXzry getMonthZyXzry(String employeeNo,LocalDateTime monthMate){

        Assert.error(employeeNo == null, ErrorCode.BUSINESS_EXCEPTION, "员工工号不能为空");

        LocalDateTime firstDayOfMonth = monthMate.with(TemporalAdjusters.firstDayOfMonth());
        LocalDateTime lastDayOfMonth = monthMate.with(TemporalAdjusters.lastDayOfMonth());

        List<ZyXzry> list = list(Wrappers.<ZyXzry>lambdaQuery()
                .eq(ZyXzry::getCydm, employeeNo)
                .ge(ZyXzry::getCwyf, firstDayOfMonth)
                .le(ZyXzry::getCwyf, lastDayOfMonth));

        //理论来说当月排班的诊疗小组只有一个
        if (list.isEmpty()) {
            return null;
        }

        return list.get(0);
    }



    /**
     * 获取诊疗小组人员列表
     *
     * @param monthType 1-当月 2-上月
     * @param xzxh      小组序号
     */
    public List<ZyXzry> getZyXzry(Integer monthType, String xzxh) {
        LocalDateTime date = LocalDateTime.now();
        LocalDateTime firstDay = getDay(date, monthType, true);
        LocalDateTime lastDay = getDay(date, monthType, false);

        List<ZyXzry> list = list(Wrappers.<ZyXzry>lambdaQuery()
                .eq(ZyXzry::getXzxh, xzxh)
                .ge(firstDay != null, ZyXzry::getCwyf, firstDay)
                .le(lastDay != null, ZyXzry::getCwyf, lastDay)
                .orderByDesc(ZyXzry::getZzbz));

        return list;
    }

    public LocalDateTime getDay(LocalDateTime date, Integer monthType, boolean first) {
        return monthType == 1 && first
                ? date.with(TemporalAdjusters.firstDayOfMonth())
                : monthType == 2 && first
                ? date.minusMonths(1).with(TemporalAdjusters.firstDayOfMonth())
                : monthType == 1
                ? date.with(TemporalAdjusters.lastDayOfMonth())
                : monthType == 2
                ? date.minusMonths(1).with(TemporalAdjusters.lastDayOfMonth())
                : null;
    }

    /**
     * 获取诊疗小组组长
     *
     * @param monthType 1-当月 2-上月
     */
    public ZyXzry getZyXzryZc(String xzxh, Integer monthType, boolean required) {
        LocalDateTime date = LocalDateTime.now();
        LocalDateTime firstDay = getDay(date, monthType, true);
        LocalDateTime lastDay = getDay(date, monthType, false);

        ZyXzry one = lambdaQuery()
                .eq(ZyXzry::getZzbz, 1)
                .eq(ZyXzry::getXzxh, xzxh)
                .ge(firstDay != null, ZyXzry::getCwyf, firstDay)
                .le(lastDay != null, ZyXzry::getCwyf, lastDay)
                .one();
        Assert.error(required && one == null, ErrorCode.BUSINESS_EXCEPTION, String.format("诊疗小组组长不存在, xzxh:" + xzxh));
        return one;
    }
}

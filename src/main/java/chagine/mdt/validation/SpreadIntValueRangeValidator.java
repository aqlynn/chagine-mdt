package chagine.mdt.validation;

import chagine.mdt.validation.annotation.SpreadIntValueRange;
import org.springframework.util.ObjectUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class SpreadIntValueRangeValidator implements ConstraintValidator<SpreadIntValueRange, Object> {

    private int[] values;

    @Override
    public void initialize(SpreadIntValueRange spreadIntValueRange) {
        this.values = spreadIntValueRange.values();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {

        if (ObjectUtils.isEmpty(o)) {
            return true;
        }

        for (int value : values) {
            if (Objects.equals(o, value)) {
                return true;
            }
        }

        return false;
    }
}

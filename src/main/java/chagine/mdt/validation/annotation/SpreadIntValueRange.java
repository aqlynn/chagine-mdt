package chagine.mdt.validation.annotation;

import chagine.mdt.validation.SpreadIntValueRangeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.PARAMETER;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE, PARAMETER})
@Constraint(validatedBy = SpreadIntValueRangeValidator.class)
public @interface SpreadIntValueRange {

    int[] values();

    String message() default "不在取值范围之内";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

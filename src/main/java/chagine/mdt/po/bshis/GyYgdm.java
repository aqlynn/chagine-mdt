package chagine.mdt.po.bshis;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;

import static com.baomidou.mybatisplus.annotation.IdType.ASSIGN_ID;


/**
 * 员工代码
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@TableName("BSHIS.GY_YGDM")
public class GyYgdm {


    @TableId(value = "YGDM", type = ASSIGN_ID)
    private String ygdm;

    @TableField("KSDM")
    private String ksdm;

    /**
     * 员工姓名
     */
    @TableField("YGXM")
    private String ygxm;

    @TableField("ZFPB")
    private Integer zfpb;

    /**
     *
     */
    @TableField("LXDH")
    private String lxdh;

    /**
     * 1：医生；2：护士；3：其他；
     */
    @TableField("RYLB")
    private Integer rylb;

    /**
     * 员工级别
     */
    @TableField("YGJB")
    private Integer ygjb;

    /**
     * 身份证号
     */
    @TableField("SFZH")
    private String sfzh;



}

package chagine.mdt.po.bshis;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;

/**
 * 住院-小组人员
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@TableName("BSHIS.ZY_XZRY")
public class ZyXzry {


    /**
     * 小组序号
     */
    @TableField(value = "XZXH")
    private String xzxh;

    /**
     * 成员代码
     */
    @TableField(value = "CYDM")
    private String cydm;

    /**
     * 成员姓名
     */
    @TableField(value = "CYXM")
    private String cyxm;

    /**
     * 成员级别
     */
    @TableField(value = "CYJB")
    private Integer cyjb;

    /**
     * 1 院级 2 科室级 3 诊疗小组级 4 医生级
     */
    @TableField(value = "QXJB")
    private Integer qxjb;

    /**
     * 所属科室代码
     */
    @TableField(value = "SSKS")
    private String ssks;


    /**
     * 组长标志
     */
    @TableField(value = "ZZBZ")
    private Integer zzbz;

    /**
     * 拼音代码
     */
    @TableField(value = "PYDM")
    private String pydm;


    /**
     * 财务月份
     */
    @TableField(value = "CWYF")
    private LocalDateTime cwyf;

    /**
     * 输入日期
     */
    @TableField(value = "SRRQ")
    private LocalDateTime srrq;



}

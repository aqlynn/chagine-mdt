package chagine.mdt.po.bshis;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 住院-诊疗小组
 */
@Data
@Accessors(chain = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "BSHIS.ZY_ZLXZ")
public class ZyZlxz {

    /**
     * 院区标志 1-信河 2-娄桥
     */
    @TableField(value = "YQBZ")
    private Integer yqbz;

    /**
     * 小组序号
     */
    @TableField(value = "XZXH")
    private String xzxh;

    /**
     * 小组名称
     */
    @TableField(value = "XZMC")
    private String xzmc;

    /**
     * 所属科室代码
     */
    @TableField(value = "SSKS")
    private String ssks;

    /**
     * 所属病区代码
     */
    @TableField(value = "SSBQ")
    private String ssbq;

    /**
     * 所属病区代码
     */
    @TableField(value = "SSBQ")
    private String wardCode;

    /**
     * 启用标志
     */
    @TableField(value = "YQBZ")
    private Integer qybz;

    /**
     * 作废判别
     */
    @TableField(value = "ZFPB")
    private Integer zfpb;

    /**
     * 诊疗类型 1-门诊 2-住院 3-急诊
     */
    @TableField(value = "ZLLX")
    private Integer zllx;
}

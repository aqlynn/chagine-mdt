package chagine.mdt.po.bshis;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import static com.baomidou.mybatisplus.annotation.IdType.ASSIGN_ID;

/**
 * 公用科室
 */
@Data
@Accessors(chain = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "BSHIS.GY_KSDM")
public class GyKsdm {
    /**
     * 科室代码
     */
    @TableId(value = "KSDM", type = ASSIGN_ID)
    private String ksdm;

    /**
     * 科室名称
     */
    @TableField(value = "KSMC")
    private String ksmc;

    /**
     * 上级科室（1-临床科室 3-行政科室 4-医技科室 5-护士病区）
     */
    @TableField(value = "SJKS")
    private String sjks;

    /**
     * 科室设置，如：2015.01.01
     */
    @TableField(value = "KSSZ")
    private String kssz;

    /**
     * 院区标志 1-信河 2-娄桥
     */
    @TableField(value = "YQBZ")
    private Integer yqbz;


    /**
     * 所属病区
     */
    @TableField(value = "SSBQ")
    private String ssbq;
}

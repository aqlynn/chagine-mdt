package chagine.mdt.po.yy;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 诊断表
 * </p>
 *
 * @author lsf
 * @since 2023-04-13
 */
@Data
@Accessors(chain = true)
@TableName("YOIEMR.YY_DX_DXTAB")
public class YyDxDxtab {

    /**
     * 诊断表编号---主键ID
     */
    @TableId(value = "ID", type = IdType.INPUT)
    private String id;
    /**
     * 病人序号
     */
    @TableField("PATIENTID")
    private String patientid;
    /**
     * 诊断序号
     */
    @TableField("DXINDEX")
    private String dxindex;
    /**
     * 诊断代码
     */
    @TableField("DXCODE")
    private String dxcode;
    /**
     * ICD编码
     */
    @TableField("ICD")
    private String icd;
    /**
     * 诊断名称
     */
    @TableField("DXNAME")
    private String dxname;
    /**
     * 诊断名称补充
     */
    @TableField("DXNAMESUP")
    private String dxnamesup;
    /**
     * 诊断确诊标记 1确认 2待查 0疑似
     */
    @TableField("DXDIAGNOSIS")
    private String dxdiagnosis;
    /**
     * 诊断治疗效果
     */
    @TableField("RESULTID")
    private String resultid;
    /**
     * 诊断类别 0初步 55补充 1修正 2入院 3出院 20术前 21术后 300 病案首页
     */
    @TableField("DXCATEGORY")
    private String dxcategory;
    /**
     * 诊断医师ID
     */
    @TableField("DOCTORID")
    private String doctorid;
    /**
     * 书写时间
     */
    @TableField("WRITINGTIME")
    private String writingtime;
    /**
     * 诊断时间
     */
    @TableField("DXTIME")
    private String dxtime;
    /**
     * 报卡类型
     */
    @TableField("REPORTCARD_TYPE")
    private String reportcardType;
    /**
     * 医保类型
     */
    @TableField("REPORTEDCARD_TYPE")
    private String reportedcardType;
    /**
     * 未报原因
     */
    @TableField("UNREPORTED_REASON")
    private String unreportedReason;
    /**
     * 父ID
     */
    @TableField("FATHERID")
    private String fatherid;
    /**
     * 手术ID
     */
    @TableField("OPERATIONID")
    private String operationid;
    /**
     * 主手术ID
     */
    @TableField("MAINOPERATIONID")
    private String mainoperationid;
    /**
     * 诊断状态
     */
    @TableField("DXSTATE")
    private String dxstate;
    /**
     * 诊断类型
     */
    @TableField("DXTYPE")
    private String dxtype;
    /**
     * 证候ICD
     */
    @TableField("SYNDROMESICD")
    private String syndromesicd;
    /**
     * 证候名称
     */
    @TableField("SYNDROMESNAME")
    private String syndromesname;
    /**
     * 主诊断
     */
    @TableField("MAINDX")
    private String maindx;
    /**
     * 诊断部位
     */
    @TableField("DXPOSITION")
    private String dxposition;
    /**
     * 解剖名称
     */
    @TableField("ANATOMYNAME")
    private String anatomyname;
    /**
     * 入院情况
     */
    @TableField("ADMISSION_IS")
    private String admissionIs;
    /**
     * 上级医师
     */
    @TableField("SUPERIOR_DOCTORS")
    private String superiorDoctors;
    /**
     * 修改时间(非空)
     */
    @TableField("UPDATETIME")
    private LocalDateTime updatetime;
    /**
     * DRGS
     */
    @TableField("DRGS")
    private String drgs;

}

package chagine.mdt.po.yy;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 病人信息表
 * </p>
 */
@Data
@Accessors(chain = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("YOIEMR.YY_SYS_PATIENT")
public class YySysPatient {

    /**
     * 病人序号ID
     */
    @TableId(value = "PATIENTID", type = IdType.ASSIGN_ID)
    private String patientId;
    /**
     * 在院状态：1：在院；3：出院；4:死亡；-2：未接科；-1：作废
     */
    @TableField("STATEID")
    private String stateid;
    /**
     * 入院病区代码
     */
    @TableField("ADMISSION_WARDCODE")
    private String admissionWardcode;
    /**
     * 科室代码
     */
    @TableField("DEPTCODE")
    private String deptcode;
    /**
     * 当前病区代码
     */
    @TableField("WARDCODE")
    private String wardcode;
    /**
     * 出院病区代码
     */
    @TableField("DISCHARGE_WARDCODE")
    private String dischargeWardcode;
    /**
     * 入院时间
     */
    @TableField("INADMITTIME")
    private String inadmittime;
    /**
     * 出院时间
     */
    @TableField("OUTADMITTIME")
    private String outadmittime;
    /**
     * HIS住院ID（病人每次住院一个ID）
     */
    @TableField("HISPATIENTID")
    private String hispatientid;
    /**
     * HIS病案号(住院号码)
     */
    @TableField("MRN")
    private String mrn;
    /**
     * 床号
     */
    @TableField("BEDNO")
    private String bedno;
    /**
     * 姓名
     */
    @TableField("NAME")
    private String name;
    /**
     * 出生日期 2023-11-16
     */
    @TableField("BIRTHDATE")
    private String birthdate;
    /**
     * 年龄 0、4
     */
    @TableField("AGE")
    private String age;
    /**
     * 年龄单位 天、岁
     */
    @TableField("AGE_UNIT")
    private String ageUnit;

    /**
     * 主诊医师
     */
    @TableField("CHIEF_DOCTOR")
    private String chiefDoctor;
    /**
     * 性别 1-男 2-女
     */
    @TableField("SEX")
    private String sex;
    /**
     * 身份证号
     */
    @TableField("IDCARD")
    private String idcard;
    /**
     * 手机号码
     */
    @TableField("MOBILE")
    private String mobile;
    /**
     * 主治医师ID
     */
    @TableField("ATTNDOCTOR")
    private String attndoctor;

    /**
     * 病历状态，在科-1，医生提交或护士提交0，归档1，病案室驳回-2
     */
    @TableField("CASEHISTORY_STATE")
    private String casehistoryState;

    /**
     * 门诊就诊卡号
     */
    @TableField("CLINIC_NUMBER")
    private String clinicNumber;

    /**
     * 当前院区ID
     */
    @TableField("HOSPIZATION_AREA_ID")
    private Integer hospizationAreaId;
    /**
     * 科室名称
     */
    @TableField("DEPT_NAME")
    private String deptName;
    /**
     * 病区名称
     */
    @TableField("WARD_NAME")
    private String wardName;
    /**
     * 出院病区名称
     */
    @TableField("DISCHARGE_WARD_NAME")
    private String dischargeWardName;

    /**
     * 住院序号
     */
    @TableField("ZYXH")
    private String zyxh;
    /**
     * 入院科室id
     */
    @TableField("ADMISSION_DEPTCODE")
    private String admissionDeptcode;


    /**
     * HIS住院号
     */
    @TableField("ZYH")
    private Long zyh;


    /**
     * 主诊医师姓名
     */
    @TableField(value = "CHIEF_DOCTOR_NAME",exist = false)
    private String chiefDoctorName;
}
package chagine.mdt.po.yy;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.sql.Blob;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("YOIEMR.YY_SYS_USER")
public class YySysUser {

    @TableId(value = "USERID", type = IdType.ASSIGN_ID)
    private String userid;
    @TableField("USNAME")
    private String usname;

    //用户姓名汉语拼音
    @TableField("NAME_PHONETIC")
    private String namePhonetic;
    @TableField("PASSWORD")
    private String password;

    //签名密码
    @TableField("SIGN_PASSWORD")
    private String signPassword;
    @TableField("WARDCODE")
    private String wardcode;
    @TableField("DEPTCODE")
    private String deptcode;

    @TableField("USERNO")
    private String userno;
    @TableField("STATE")
    private Integer state;

    //是否删除，启用0,  删除1,待审核-1
    @TableField("DELETEMARK")
    private Integer deletemark;


    @TableField("MOBILE")
    private String mobile;

}
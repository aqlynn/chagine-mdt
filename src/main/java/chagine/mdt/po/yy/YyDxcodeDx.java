package chagine.mdt.po.yy;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author lsf
 * @since 2023-04-13
 */
@Data
@Accessors(chain = true)
@TableName("YY_DXCODE_DX")
public class YyDxcodeDx {

    /**
     * icd编码
     */
    @TableId(value = "DXCODE", type = IdType.ASSIGN_ID)
    private String dxcode;
    /**
     * 诊断名称
     */
    @TableField("DXNAME")
    private String dxname;
    /**
     * ICD10码
     */
    @TableField("ICD10")
    private String icd10;
    /**
     * ICD9码
     */
    @TableField("ICD9")
    private String icd9;
    /**
     * 拼音码
     */
    @TableField("INPUTCODE1")
    private String inputcode1;
    /**
     * 五笔码
     */
    @TableField("INPUTCODE2")
    private String inputcode2;
    /**
     * 自定交码
     */
    @TableField("INPUTCODE3")
    private String inputcode3;
    /**
     * 病案系统序号
     */
    @TableField("MRNUMBER")
    private String mrnumber;
    /**
     * 科室代码
     */
    @TableField("DEPTCODE")
    private String deptcode;
    /**
     * 报卡类型
     */
    @TableField("REPORTCARD_TYPE")
    private String reportcardType;
    /**
     * 感染排序
     */
    @TableField("REPORTCARD_CATEGORY")
    private String reportcardCategory;
    /**
     * 描述
     */
    @TableField("DESCRIPTION")
    private String description;
    /**
     * 系统级别类型
     */
    @TableField("SYSLEVEL")
    private Integer syslevel;
    /**
     * 疾病统计码9
     */
    @TableField("LEVELSTATISTICS9")
    private Integer levelstatistics9;
    /**
     * 疾病统计码10
     */
    @TableField("LEVELSTATISTICS10")
    private Integer levelstatistics10;
    /**
     * 作废标志
     */
    @TableField("INVALIDSTATE")
    private Integer invalidstate;
    /**
     * 报卡类别
     */
    @TableField("INFECT_SORT")
    private Integer infectSort;

}

package chagine.mdt.feign;


import chagine.core.Env;
import chagine.core.ErrorCode;
import chagine.core.exception.SeeingException;
import chagine.core.restful.util.TResponse;
import chagine.core.util.Assert;
import chagine.core.util.FeignUtil;
import chagine.core.util.ServiceName;
import chagine.mdt.Constants;
import chagine.mdt.feign.vo.FTencentIMVo;
import chagine.mdt.feign.vo.FTimImVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * create by linxiaoke
 * 腾讯IM账号相关
 */
@FeignClient(ServiceName.BASIC_SERVICE)
public interface TencentIMUserClient {

    @RequestMapping(value = "/feign/timuser/get_tim_user_for_user", method = RequestMethod.GET)
    TResponse<FTencentIMVo> getTimUserForUser(@RequestParam(value = "user_id") Long userId, @RequestParam(value = "tena_id", required = false) Long tenaId);

    /**
     * 获取员工腾讯IM账号
     *
     * @param userId 用户ID
     * @param tenaId 租户ID
     */
    default FTimImVo detailGetTimUserForUser(Long userId, Long tenaId) throws SeeingException {
        TResponse<FTencentIMVo> timUserResponse = getTimUserForUser(userId, tenaId);
        if (timUserResponse == null) {
            throw new SeeingException(ErrorCode.ILLEGALARGUMENT_EXCEPTION, "根据user_ID:" + userId + "找对应腾讯IM账号出错");
        }
        FTencentIMVo tencentIMVo =  FeignUtil.getResponseData(timUserResponse);
        FTimImVo timImVo = new FTimImVo();
        if (tencentIMVo.getRst() == Constants.COMMON_YES) {
            timImVo.setIsTimUser(Constants.COMMON_YES);
            timImVo.setTimIdentifier(tencentIMVo.getIdentifier());
            timImVo.setTimUserSig(tencentIMVo.getUserSig());
            timImVo.setNickname(tencentIMVo.getNickname());
            timImVo.setFaceUrl(tencentIMVo.getFaceUrl());
        } else {
            timImVo.setIsTimUser(Constants.COMMON_NO);
        }
        return timImVo;
    }

    @GetMapping(value = "/feign/timuser/get_tim_user_by_identifier")
    TResponse<FTencentIMVo> getTimUserByIdentifier(
            @RequestParam(value = "identifier") String identifier,
            @RequestHeader(name = Env.HEAD_TENANT_KEY, required = false) Long tenantId
    );

    /**
     * 根据腾讯IM账号获取用户信息
     */
    default FTencentIMVo detailGetTimUserByIdentifier(String identifier, Long tenantId) throws SeeingException {
        TResponse<FTencentIMVo> timUserResponse = getTimUserByIdentifier(identifier, tenantId);
        Assert.error(timUserResponse == null, ErrorCode.ILLEGALARGUMENT_EXCEPTION, "根据identifier_ID:" + identifier + "找对应腾讯IM账号出错");
        FTencentIMVo fTencentIMVo =  FeignUtil.getResponseData(timUserResponse);
        return fTencentIMVo;
    }



    @RequestMapping(value = "/feign/timuser/get_tim_user_for_device", method = RequestMethod.GET)
    TResponse<FTencentIMVo> getTimUserForDevice(@RequestParam(value = "user_id") Long userId, @RequestParam(value = "device_id", required = false) String deviceId, @RequestParam(value = "tena_id", required = false) Long tenaId);

    default FTencentIMVo detailGetTimUserForDevice(Long userId, String deviceId, Long tenaId) throws SeeingException {
        TResponse<FTencentIMVo> timUserResponse = getTimUserForDevice(userId,deviceId,tenaId);
        FTencentIMVo fTencentIMVo =  FeignUtil.getResponseData(timUserResponse);
        return fTencentIMVo;
    }
}

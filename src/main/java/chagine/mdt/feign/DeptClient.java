package chagine.mdt.feign;

import chagine.core.restful.util.TResponse;
import chagine.core.util.ServiceName;
import chagine.mdt.feign.vo.DepartmentVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * create by wba on  2020/3/13
 * qq:43585061
 */
@FeignClient(ServiceName.BASIC_SERVICE)
public interface DeptClient {

    @RequestMapping(value = "/feign/depa/get_depa", method = RequestMethod.GET)
    TResponse<DepartmentVo> getDepa(@RequestParam(value = "depa_id", required = false) Long depaId,
                                    @RequestHeader("tenantId") Long tenaId);
}

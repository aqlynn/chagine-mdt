package chagine.mdt.feign.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 创建腾讯IM账号响应
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FTimImVo implements Serializable {

    @JsonProperty(value = "is_tim_user")
    private Integer isTimUser;

    @JsonProperty(value = "tim_identifier")
    private String timIdentifier;

    @JsonProperty(value = "tim_user_sig")
    private String timUserSig;

    @JsonProperty(value = "nickname")
    private String nickname;

    @JsonProperty(value = "face_url")
    private String faceUrl;

    @JsonProperty(value = "trtc_room_id")
    private String trtcRoomId;


}

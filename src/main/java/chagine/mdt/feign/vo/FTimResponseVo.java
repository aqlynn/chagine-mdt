package chagine.mdt.feign.vo;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * create by wba on  2020/12/19
 * qq:43585061
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class FTimResponseVo {

    @JsonProperty("rst")
    private int rst; //为0就是有报错，需要提示msg给用户，为1就是操作成功

    @JsonProperty("msg")
    private String msg;//错误信息（rst为0时存在）

    @JsonProperty("groupId")
    private String groupId;//创建群组时会返回这个字段（rst为1）

    @JsonProperty("MemberList")
    private List<FAddTimMemberAccount> memberList; //增加群成员时会返回这个字段说明成员是否新增成功（rst为1）


}

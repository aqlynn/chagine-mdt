package chagine.mdt.feign.vo;

import chagine.core.serialize.deserializer.DateFormatter2TimestampDeserializer;
import chagine.core.serialize.deserializer.String2LongDeserializer;
import chagine.core.serialize.serialize.Long2StringSerializer;
import chagine.core.serialize.serialize.Timestamp2DateFormatterSerializer;
import chagine.core.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PatientVo {

    @JsonProperty(value = "patient_id")
    @JsonDeserialize(using = String2LongDeserializer.class)
    @JsonSerialize(using = Long2StringSerializer.class)
    private Long patientId;

    @JsonProperty(value = "patient_name")
    private String patientName;

    @JsonProperty(value = "py")
    private String py;

    @JsonProperty(value = "wb")
    private String wb;

    @JsonProperty(value = "gender")
    private Integer gender;

    @JsonProperty(value = "birthday")
    @JsonDeserialize(using = DateFormatter2TimestampDeserializer.class)
    @JsonSerialize(using = Timestamp2DateFormatterSerializer.class)
    private Long birthday;

    @JsonProperty(value = "phone")
    private String phone;

    //不脱敏的
    @JsonProperty(value = "phone2")
    private String phone2;

    @JsonProperty(value = "tel")
    private String tel;

    @JsonProperty(value = "id_type")
    private Integer idType;

    @JsonProperty(value = "id_number")
    private String idNumber;

    @JsonProperty(value = "create_src")
    private Integer createSrc;

    @JsonProperty(value = "create_time")
    @JsonDeserialize(using = DateFormatter2TimestampDeserializer.class)
    @JsonSerialize(using = Timestamp2DateFormatterSerializer.class)
    private Long createTime;

    @JsonProperty(value = "create_user")
    private Long createUser;

    @JsonProperty(value = "is_certified")
    private Integer isCertified;

    @JsonProperty(value = "area_code")
    private String areaCode;

    // 职业
    private String profession;

    private String addr;

    @JsonProperty(value = "age")
    public String getAge() {
        Integer age = DateUtil.getAge(birthday);
        return age == null ? "未知" : age + "岁";
    }

    @JsonProperty(value = "customer_rem")
    private String customerRem;

    //关系(1:本人，0:其他)
    @JsonProperty(value = "relationship")
    private Integer relationship;

    //是否默认就诊人  1 默认  0 不默认
    @JsonProperty(value = "is_default")
    private Integer isDefault;

    /**
     * 患者在his系统状态: 1:出院状态;2:在院状态;3:门诊状态
     */
    @JsonProperty(value = "his_status")
    private Integer hisStatus;

    /**
     * 住院号
     */
    @JsonProperty(value = "inpatient_number")
    private String inpatientNumber;

    /**
     * 住院病区
     */
    @JsonProperty(value = "inpatient_ward")
    private String inpatientWard;

    /**
     * 住院床号
     */
    @JsonProperty(value = "inpatient_bed_number")
    private String inpatientBedNumber;

    /**
     * 入院时间
     */
    @JsonProperty(value = "admission_date")
    @JsonDeserialize(using = DateFormatter2TimestampDeserializer.class)
    @JsonSerialize(using = Timestamp2DateFormatterSerializer.class)
    private Long admissionDate;

    @JsonProperty(value = "recommend_user_id")
    @JsonDeserialize(using = String2LongDeserializer.class)
    @JsonSerialize(using = Long2StringSerializer.class)
    private Long recommendUserId;

    @JsonProperty(value = "recommend_user_code")
    private String recommendUserCode;

    @JsonProperty(value = "recommend_user_type")
    private Integer recommendUsertype;

    @JsonProperty(value = "pati_user_id")
    private Long patiUserId;


    @JsonProperty(value = "recommend_time")
    @JsonDeserialize(using = DateFormatter2TimestampDeserializer.class)
    @JsonSerialize(using = Timestamp2DateFormatterSerializer.class)
    private Long recommendTime;


    //积分
    @JsonProperty(value = "points")
    private Integer points;


    //完整度:0不完整,1完整
    @JsonProperty(value = "integrity")
    private Integer integrity;
}

package chagine.mdt.feign.vo;

import chagine.core.serialize.deserializer.MoneyDeserializer;
import chagine.core.serialize.deserializer.String2LongDeserializer;
import chagine.core.serialize.serialize.Long2StringSerializer;
import chagine.core.serialize.serialize.MoneySerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DoctorVO {

    @JsonProperty(value = "doctor_id")
    @JsonDeserialize(using = String2LongDeserializer.class)
    @JsonSerialize(using = Long2StringSerializer.class)
    private Long doctorId;

    @JsonProperty(value = "depa_id")
    @JsonDeserialize(using = String2LongDeserializer.class)
    @JsonSerialize(using = Long2StringSerializer.class)
    private Long depaId;

    private String name;

    @JsonProperty(value = "job_number")
    private String jobNumber;

    @JsonProperty(value = "head_img_url")
    private String headImgUrl;

    @JsonProperty(value = "qualification_img_url")
    private String qualificationImgUrl;

    @JsonProperty(value = "credentials_img_url")
    private String credentialsImgUrl;

    private String description;
    @JsonProperty(value = "depa_name")
    private String depaName;

    private String title;

    private String hospital;

    @JsonProperty(value = "good_at")
    private String goodAt;

    @JsonProperty(value = "serv_num")
    private Integer servNum;

    @JsonProperty(value = "online_status")
    private Integer onlineStatus;

    private String star;

    @JsonProperty(value = "consult_fee")
    @JsonSerialize(using = MoneySerializer.class)
    @JsonDeserialize(using = MoneyDeserializer.class)
    private Integer consultFee;

    @JsonProperty(value = "is_consult")
    private Integer isConsult;

    @JsonProperty(value = "pres_fee")
    @JsonSerialize(using = MoneySerializer.class)
    @JsonDeserialize(using = MoneyDeserializer.class)
    private Integer presFee;

    @JsonProperty(value = "is_pres")
    private Integer isPres;

    @JsonProperty(value = "district_name")
    private String districtName;

    @JsonProperty(value = "phone")
    private String phone;
}

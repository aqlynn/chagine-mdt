package chagine.mdt.feign.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FTimRequestVo {

    @JsonProperty("group_name")
    private String groupName;//创建群组时必传，UTF8 限制30个字节

    @JsonProperty("groupId")
    private String groupId;//增加群成员、删除群成员、解散群 必传

    @JsonProperty("member_list")
    private List<String> memberList; //增加群成员、删除群成员 必传

    @JsonProperty("max_member_count")
    private Integer maxMemberCount; // 最大群成员数 选填

}

package chagine.mdt.feign.vo;

import chagine.core.serialize.deserializer.String2LongDeserializer;
import chagine.core.serialize.serialize.Long2StringSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SymptomVo {

    @JsonProperty(value = "symptom_id")
    @JsonDeserialize(using = String2LongDeserializer.class)
    @JsonSerialize(using = Long2StringSerializer.class)
    private Long symptomId;

    @JsonProperty(value = "symptom_name")
    private String symptomName;

    @JsonProperty(value = "enable")
    private Integer enable;


}

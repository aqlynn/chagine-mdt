package chagine.mdt.feign.vo;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Database table trtc_video_record
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FTRTCVideoRecordVo {


    /**
     * 业务类型,1-探视,2-护理,3-急诊
     * Table column trtc_video_record.biz_type
     */
    @JsonProperty(value = "biz_type")
    private Integer bizType;

    /**
     * 时长
     * Table column trtc_video_record.duration
     */
    @JsonProperty(value = "duration")
    private Integer duration;

    /**
     * 流id
     * Table column trtc_video_record.stream_id
     */
    @JsonProperty(value = "stream_id")
    private String streamId;

    /**
     * 文件id
     * Table column trtc_video_record.file_id
     */
    @JsonProperty(value = "file_id")
    private String fileId;

    /**
     * 文件大小
     * Table column trtc_video_record.file_size
     */
    @JsonProperty(value = "file_size")
    private Long fileSize;

    /**
     * url地址
     * Table column trtc_video_record.video_url
     */
    @JsonProperty(value = "video_url")
    private String videoUrl;

//    /**
//     * 开始时间
//     * Table column trtc_video_record.start_time
//     */
//    @JsonProperty(value = "start_time")
//    @JsonDeserialize(using = TimeLongDeserializer.class)
//    @JsonSerialize(using = LongTimeSerializer.class)
//    private Long startTime;
//
//    /**
//     * 结束时间
//     * Table column trtc_video_record.end_time
//     */
//    @JsonProperty(value = "end_time")
//    @JsonDeserialize(using = TimeLongDeserializer.class)
//    @JsonSerialize(using = LongTimeSerializer.class)
//    private Long endTime;

    /**
     * 创建时间
     * Table column trtc_video_record.create_time
     */
    @JsonIgnore
    private Long createTime;

    /**
     * 租户id
     * Table column trtc_video_record.tena_id
     */
    @JsonIgnore
    private Long tenaId;


    @JsonProperty(value = "local_video_url")
    private String localVideoUrl;

    /**
     * 视频首帧数
     */
    @JsonProperty(value = "first_frame_url")
    private String firstFrameUrl;

}
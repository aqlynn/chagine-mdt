package chagine.mdt.feign.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FXingeMessageVo {


    /**
     * 消息类型，必需
     * 1：notify：通知； 2：message：透传消息/静默消息
     */
    @JsonProperty(value = "message_type")
    private Integer messageType;

    /**
     * 手表-intrap-mews-watch  床头屏-intrap-mews-bedside-screen  掌上仁医-yth
     */
    @JsonProperty(value = "biz_type")
    private Integer bizType;

    /**
     * 标题，必需
     */
    @JsonProperty(value = "title")
    private String title;

    /**
     * 内容，必需
     */
    @JsonProperty(value = "content")
    private String content;

    /**
     * 标签推送，标签推送必需
     */
    @JsonProperty(value = "tag_list")
    private List<String> tagList;

    /**
     * 账号列表群推，账号列表推送时必需
     */
    @JsonProperty(value = "account_list")
    private List<String> accountList;

    /**
     * 用户自定义的参数（需要序列化为 JSON String），可选
     */
    @JsonProperty(value = "custom_content")
    private String customContent;

}

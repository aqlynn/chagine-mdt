package chagine.mdt.feign.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FeignPatientVo {

    @JsonProperty(value = "patient_id")
    private Long patientId;

    @JsonProperty(value = "patient_name")
    private String patientName;

    @JsonProperty(value = "gender")
    private Integer gender;

    @JsonProperty(value = "birthday")
    private Long birthday;

    @JsonProperty(value = "phone")
    private String phone;

    @JsonProperty(value = "id_number")
    private String idNumber;

    @JsonProperty(value = "from_tena_id")
    private String fromTenaId;

    @JsonProperty(value = "pad_id")
    private Long padId;

    private String addr;

    @JsonProperty(value = "area_name")
    private String areaName;

    /**
     * 就诊卡号
     */
    @JsonProperty(value = "visit_card")
    private String visitCard;


    /**
     * 患者在his系统状态: 1:出院状态;2:在院状态;3:门诊状态
     */
    @JsonProperty(value = "his_status")
    private Integer hisStatus;

    /**
     * 住院号
     */
    @JsonProperty(value = "inpatient_number")
    private String inpatientNumber;

    /**
     * 住院病区
     */
    @JsonProperty(value = "inpatient_ward")
    private String inpatientWard;

    /**
     * 住院床号
     */
    @JsonProperty(value = "inpatient_bed_number")
    private String inpatientBedNumber;

    /**
     * 入院时间
     */
    @JsonProperty(value = "admission_date")
    private Long admissionDate;

    /**
     * 住院科室编号
     */
    @JsonProperty(value = "inpatient_department_code")
    private String inpatientDepartmentCode;

    /**
     * 推荐人
     */
    @JsonProperty(value = "recommend_user_id")
    private Long recommendUserId;

    /**
     * 推荐人类型
     */
    @JsonProperty(value = "recommend_user_type")
    private Integer recommendUserType;


    @JsonProperty(value = "pati_user_id")
    private Long patiUserId;

    /**
     * 是否消费 0未消费，1已消费
     */
    @JsonProperty(value = "if_sale")
    private Integer ifSale;

    @JsonProperty(value = "effective_time")
    private Long effectiveTime;

}

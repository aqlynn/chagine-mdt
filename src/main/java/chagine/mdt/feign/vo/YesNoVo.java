package chagine.mdt.feign.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * create by wba on  2020/12/19
 * qq:43585061
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class YesNoVo {
    private int rst; //0：失败；1：成功
    private String msg;//当rst为0是，msg就是错误信息
}

package chagine.mdt.feign.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;


/**
 * @author : linxiaoke
 * 加入群组用户账号
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class FAddTimMemberAccount {

    /**
     * 待添加的群成员 UserID
     */
    @JsonProperty("Member_Account")
    private String memberAccount;

    /**
     *  加人结果：0 为失败；1 为成功；2 为已经是群成员
     */
    @JsonProperty("Result")
    private Integer result;


}

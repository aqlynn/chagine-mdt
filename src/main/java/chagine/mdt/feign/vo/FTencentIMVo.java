package chagine.mdt.feign.vo;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FTencentIMVo {

//    @JsonProperty(value = "user_id")
//    @JsonDeserialize(using = String2LongJsonDeserializer.class)
//    @JsonSerialize(using = LongJsonSerializer.class)
//    private Long userId;

    @JsonProperty(value = "identifier")
    private String identifier;

    @JsonProperty(value = "user_sig")
    private String userSig;

    @JsonProperty(value = "valid_time")
    private Long validTime;

    @JsonProperty(value = "owner_type")
    private Integer ownerType;

    @JsonProperty(value = "owner_id")
    private Long ownerId;

    @JsonProperty(value = "nickname")
    private String nickname;

    @JsonProperty(value = "face_url")
    private String faceUrl;

    //0没有用户，并且注册失败
    private int rst = 1;
}

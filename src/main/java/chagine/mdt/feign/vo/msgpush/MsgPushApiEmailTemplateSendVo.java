package chagine.mdt.feign.vo.msgpush;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;
import java.util.Map;

/**
 * 发送邮件模版
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MsgPushApiEmailTemplateSendVo extends MsgPushApiBaseSendVo {

    public static final String VERSION = "0.0.0";
    public static final String ACTION = "EmailTemplateSend";

    public MsgPushApiEmailTemplateSendVo() {
        super(ACTION, VERSION);
    }

    /**
     * 模版ID
     */
    @JsonProperty("template_id")
    private String templateId;

    /**
     * 邮件主题
     */
    @JsonProperty("subject")
    private String subject;

    /**
     * 模版参数
     */
    @JsonProperty("params")
    private Map<String, Object> paramsMap;

    /**
     * 邮件收件方
     */
    @JsonProperty("to_email")
    private List<String> toEmailList;

    /**
     * 邮件类型 1-TXT 2-HTML
     */
    @JsonProperty("email_type")
    private Integer emailType;

    /**
     * 是否，自动处理短链接，如果参数里含有以url开头的参数，则自动转换
     */
    @JsonProperty("auto_short_url")
    private Boolean autoShortUrl;

}

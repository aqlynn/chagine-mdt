package chagine.mdt.feign.vo.msgpush;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class MsgPushApiCommonData {

    @JsonProperty("access_key")
    private String accessKey;

    @JsonProperty("sign")
    private String sign;

    @JsonProperty("timestamp")
    private Long timestamp;

    @JsonProperty("action")
    private String action;

    @JsonProperty("version")
    private String version;

    public MsgPushApiCommonData(String accessKey, Long timestamp, String action, String version) {
        this.accessKey = accessKey;
        this.timestamp = timestamp;
        this.action = action;
        this.version = version;
    }

}

package chagine.mdt.feign.vo.msgpush;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;

/**
 * 发送邮件模版
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MsgPushApiAppXingePushVo extends MsgPushApiBaseSendVo {

    public static final String VERSION = "0.0.0";
    public static final String ACTION = "AppXingePush";

    public MsgPushApiAppXingePushVo() {
        super(ACTION, VERSION);
    }

    /**
     * 请求的APPID
     */
    @JsonProperty("app_id")
    private String appId;

    /**
     * 推送类型 IOS、安卓
     */
    @JsonProperty("push_type")
    private String pushType;

    /**
     * 消息类型，必需
     * 1：notify：通知； 2：message：透传消息/静默消息
     */
    @JsonProperty("message_type")
    private Integer messageType;

    /**
     * 标题，必需
     */
    @JsonProperty("title")
    private String title;

    /**
     * 内容，必需
     */
    @JsonProperty("content")
    private String content;

    /**
     * 标签推送，标签推送必需
     */
    @JsonProperty("tag_list")
    private ArrayList<String> tagList;

    /**
     * 账号列表群推，账号列表推送时必需
     */
    @JsonProperty("account_list")
    private ArrayList<String> accountList;

    /**
     * 用户自定义的参数（需要序列化为 JSON String），可选
     */
    @JsonProperty("custom_content")
    private String customContent;

}

package chagine.mdt.feign.vo.msgpush;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MsgPushApiQueryMsgStatusVo extends MsgPushReqBaseVo {

    public static final String VERSION = "0.0.0";
    public static final String ACTION = "QueryMessageStatus";

    public MsgPushApiQueryMsgStatusVo() {
        super(ACTION, VERSION);
    }

    /**
     * 消息ID
     */
    @JsonProperty("message_id")
    private String messageId;


}

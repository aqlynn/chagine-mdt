package chagine.mdt.feign.vo.msgpush;

import chagine.core.serialize.deserializer.String2LongDeserializer;
import chagine.core.serialize.serialize.Long2StringSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MsgPushResQueryMessageStatusVo {

    /**
     * 消息ID
     */
    @JsonProperty("message_id")
    private String messageId;

    /**
     * 发送状态 0-未发送 1-已发送 2-发送中 3-发送失败
     */
    @JsonProperty("send_status")
    private Integer sendStatus;

    /**
     * 发送类型 1-即时发送 2-定时发送 3-计划发送
     */
    @JsonProperty("send_type")
    private Integer sendType;

    /**
     * 发送时间，时间戳：秒
     */
    @JsonProperty("send_time")
    @JsonSerialize(using = Long2StringSerializer.class)
    @JsonDeserialize(using = String2LongDeserializer.class)
    private Long sendTime;

    /**
     * 定时发送的时间
     */
    @JsonProperty("send_set_time")
    @JsonSerialize(using = Long2StringSerializer.class)
    @JsonDeserialize(using = String2LongDeserializer.class)
    private Long sendSetTime;

    /**
     * 定时计划ID
     */
    @JsonProperty("send_plan_id")
    private String sendPlanId;

    /**
     * 错误说明
     */
    @JsonProperty("error_msg")
    private String errorMsg;

}

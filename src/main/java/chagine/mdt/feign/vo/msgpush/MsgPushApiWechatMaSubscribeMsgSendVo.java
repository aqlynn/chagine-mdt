package chagine.mdt.feign.vo.msgpush;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Map;

/**
 * 发送小程序订阅消息
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MsgPushApiWechatMaSubscribeMsgSendVo extends MsgPushApiBaseSendVo {

    public static final String VERSION = "0.0.0";
    public static final String ACTION = "WechatMaSubscribeMsg";

    public MsgPushApiWechatMaSubscribeMsgSendVo() {
        super(ACTION, VERSION);
    }

    @JsonProperty("app_id")
    private String appId;

    /**
     * 小程序用户openId
     */
    @JsonProperty("touser")
    private String userOpenId;

    /**
     * 模版ID
     */
    @JsonProperty("template_id")
    private String templateId;

    /**
     * 点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
     */
    @JsonProperty("page")
    private String page;

    /**
     * 携带数据
     */
    @JsonProperty("data")
    private Map<String, MsgPushWxDataTempVo> wxDataMap;

    /**
     * 跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
     */
    @JsonProperty("miniprogram_state")
    private String miniProgramState;

}

package chagine.mdt.feign.vo.msgpush;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MsgPushResConversionLinkVo {

    /**
     * 长链接
     */
    @JsonProperty("long_url")
    private String longUrl;

    /**
     * 短链接
     */
    @JsonProperty("short_url")
    private String shortUrl;

    /**
     * 到期时间，单位秒，时间戳
     */
    @JsonProperty("validity_date")
    private Long validityDate;

    /**
     * 链接状态 0-永久链接 1-有效期链接
     */
    @JsonProperty("status")
    private Integer status;

}

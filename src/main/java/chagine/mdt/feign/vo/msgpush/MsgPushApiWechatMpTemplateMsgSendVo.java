package chagine.mdt.feign.vo.msgpush;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Map;

/**
 * 发送微信公众号订阅消息
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MsgPushApiWechatMpTemplateMsgSendVo extends MsgPushApiBaseSendVo {

    public static final String VERSION = "0.0.0";
    public static final String ACTION = "WechatMpTemplateMsg";

    public MsgPushApiWechatMpTemplateMsgSendVo() {
        super(ACTION, VERSION);
    }

    @JsonProperty("app_id")
    private String appId;

    /**
     * 小程序用户openId
     */
    @JsonProperty("touser")
    private String userOpenId;

    /**
     * 模版ID
     */
    @JsonProperty("template_id")
    private String templateId;

    /**
     * 模板跳转链接（海外帐号没有跳转能力）
     */
    @JsonProperty("url")
    private String url;

    /**
     * 跳小程序所需数据，不需跳小程序可不用传该数据
     */
    @JsonProperty("miniprogram")
    private MsgPushWxMpMiniProgramTempVo miniProgram;

    /**
     * 携带数据
     */
    @JsonProperty("data")
    private Map<String, MsgPushWxDataTempVo> wxDataMap;

}

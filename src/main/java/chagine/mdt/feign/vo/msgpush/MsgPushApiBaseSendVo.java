package chagine.mdt.feign.vo.msgpush;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class MsgPushApiBaseSendVo extends MsgPushReqBaseVo {

    public MsgPushApiBaseSendVo(String action, String version) {
        super(action, version);
    }

    /**
     * 定时推送类型 1-即时发送 2-定时发送 3-计划发送
     */
    @JsonProperty("send_time_type")
    private Integer sendTimeType;

    /**
     * 发送时间为秒级的时间戳
     */
    @JsonProperty("send_time")
    private Long sendTime;

    /**
     * 计划任务参数
     */
    @JsonProperty("send_plan_id")
    private String sendPlanId;

}

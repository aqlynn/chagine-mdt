package chagine.mdt.feign.vo.msgpush;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Map;

/**
 * 发送短信
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MsgPushApiSmsTemplateSendVo extends MsgPushApiBaseSendVo {

    public static final String VERSION = "0.0.0";
    public static final String ACTION = "SmsTemplateSend";

    public MsgPushApiSmsTemplateSendVo() {
        super(ACTION, VERSION);
    }

    /**
     * 模版ID
     */
    @JsonProperty("template_id")
    private String templateId;

    /**
     * 模版参数
     */
    @JsonProperty("params")
    private Map<String, String> paramsMap;

    /**
     * 发送手机号
     */
    @JsonProperty("phone")
    private String phone;

    /**
     * 运营商模版ID
     */
    @JsonProperty("sms_template_id")
    private String smsTemplateId;

    /**
     * 是否，自动处理短链接，如果参数里含有以url开头的参数，则自动转换
     */
    @JsonProperty("auto_short_url")
    private Boolean autoShortUrl;

}

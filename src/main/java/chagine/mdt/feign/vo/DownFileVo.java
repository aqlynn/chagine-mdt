package chagine.mdt.feign.vo;

import lombok.Data;

import java.io.InputStream;

@Data
public class DownFileVo {

    private InputStream inputStream;
    private String fileName;

}

package chagine.mdt.feign.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileVo {
    @JsonProperty(value = "file_id")
    private Long fileId;
    @JsonProperty(value = "path")
    private String path;
    @JsonProperty(value = "url")
    private String url;
}

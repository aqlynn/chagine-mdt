package chagine.mdt.feign.vo;

import chagine.core.serialize.deserializer.MoneyDeserializer;
import chagine.core.serialize.serialize.MoneySerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserVo {

    @JsonProperty(value = "user_id")
    private Long userId;

    @JsonProperty(value = "user_name")
    private String userName;
    /**
     * 部门ID
     */
    @JsonProperty(value = "depa_id")
    private Long depaId;

    @JsonProperty(value = "depa_name")
    private String depaName;

    @JsonProperty(value = "gender")
    private Integer gender;

    @JsonProperty(value = "job_number")
    private String jobNumber;

    @JsonProperty(value = "phone")
    private String phone;

    @JsonProperty(value = "password")
    private String password;

    @JsonProperty(value = "email")
    private String email;

    @JsonProperty(value = "is_admin")
    private Integer isAdmin;

    @JsonProperty(value = "head_img_id")
    private Long headImgId;

    @JsonProperty(value = "head_img_url")
    private String headImgUrl;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "good_at")
    private String goodAt;

    @JsonProperty(value = "is_doctor")
    private Integer isDoctor;

    @JsonProperty(value = "sign_img_id")
    private Long signImgId;

    @JsonProperty(value = "sign_img_url")
    private String signImgUrl;
    /*
     * 服务人数
     */
    @JsonProperty(value = "serv_num")
    private Integer servNum;
    /*
     * 星级
     */
    private Integer star;

    @JsonProperty(value = "address")
    private String address;

    @JsonProperty(value = "create_user")
    private Long createUser;

    @JsonProperty(value = "create_time")
    private Long createTime;

    /**
     * 职称
     */
    @JsonProperty(value = "title")
    private String title;

    /**
     * 医院
     */
    @JsonProperty(value = "hospital")
    private String hospital;

    @JsonProperty(value = "consult_fee")
    @JsonSerialize(using = MoneySerializer.class)
    @JsonDeserialize(using = MoneyDeserializer.class)
    private Integer consultFee;
    //是否接受咨询,默认：0，0-不接诊;1-接诊
    @JsonProperty(value = "is_consult")
    private Integer isConsult;

    //处方费
    @JsonProperty(value = "pres_fee")
    @JsonSerialize(using = MoneySerializer.class)
    @JsonDeserialize(using = MoneyDeserializer.class)
    private Integer presFee;

    //是否接受处方,默认：0，0-否;1-是
    @JsonProperty(value = "is_pres")
    private Integer isPres;

    @JsonProperty(value = "seal_data")
    private String sealData;

    @JsonProperty(value = "his_doctor_id")
    private String hisDoctorId;

}

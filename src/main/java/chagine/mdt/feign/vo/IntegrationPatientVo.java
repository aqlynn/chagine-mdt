package chagine.mdt.feign.vo;

import chagine.core.serialize.deserializer.String2LongDeserializer;
import chagine.core.serialize.serialize.Long2StringSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

/**
 * @Author Xgx
 * @Date 2022/1/20
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class IntegrationPatientVo {

    /**
     * 病人ID
     */
    @JsonProperty(value = "patient_id")
    @JsonSerialize(using = Long2StringSerializer.class)
    @JsonDeserialize(using = String2LongDeserializer.class)
    private Long patientId;

    /**
     * his患者ID
     */
    @JsonProperty(value = "his_patient_id")
    private String hisPatientId;

    /**
     * 病人姓名
     */
    @JsonProperty(value = "patient_name")
    private String patientName;

    /**
     * 性别
     */
    @JsonProperty(value = "gender")
    private Integer gender;

    /**
     * 年龄
     */
    @JsonProperty(value = "age")
    private Integer age;

    /**
     * 身份证号
     */
    @JsonProperty(value = "patient_id_number")
    private String patientIdNumber;

    /**
     * 病人性别 1-男 2-女
     */
    @JsonProperty("patient_gender")
    private Integer patientGender;

    /**
     * 出生日期
     */
    @JsonProperty("patient_birthday")
    private String patientBirthday;

    /**
     * 地址
     */
    @JsonProperty(value = "address")
    private String address;

    /**
     * 手机
     */
    @JsonProperty(value = "phone")
    private String phone;


    /**
     * 就诊卡类型 2-医保卡 4-自费卡
     */
    @JsonProperty(value = "medical_card_type")
    private Integer medicalCardType;

    /**
     * 卡号
     */
    @JsonProperty(value = "card_no")
    private String cardNo;
}

package chagine.mdt.feign.vo;

import chagine.core.serialize.deserializer.String2LongDeserializer;
import chagine.core.serialize.serialize.Long2StringSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ysc on 2017/9/25.
 */
@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DepartmentVo implements Serializable {
    /**
     * 科室ID
     */
    @JsonProperty(value = "depa_id")
    @JsonSerialize(using = Long2StringSerializer.class)
    @JsonDeserialize(using = String2LongDeserializer.class)
    private Long depaId;
    /**
     * adm_department
     * 科室名称
     */
    @JsonProperty(value = "depa_name")
    private String depaName;
    /**
     * 科室编码
     */
    @JsonProperty(value = "depa_code")
    private String depaCode;
    /**
     * 上级科室ID
     */
    @JsonProperty(value = "parent_depa_id")
    @JsonDeserialize(using = String2LongDeserializer.class)
    @JsonSerialize(using = Long2StringSerializer.class)
    private Long parentDepaId;
    /**
     * 租户号
     */
    @JsonProperty(value = "tena_id")
    private Long tenaId;

    /**
     * 科室名称拼音码
     */
    @JsonProperty(value = "py")
    private String py;
    /**
     * 科室名称五笔码
     */
    @JsonProperty(value = "wb")
    private String wb;

    /**
     * 科室位置
     */
    @JsonProperty(value = "address")
    private String address;

    /**
     * 上级部门名称
     */
    @JsonProperty(value = "parent_depa_name")
    private String parentDepaName;

    @JsonProperty(value = "logo_img_id")
    @JsonDeserialize(using = String2LongDeserializer.class)
    @JsonSerialize(using = Long2StringSerializer.class)
    private Long logoImgId;

    @JsonProperty(value = "logo_img_url")
    private String logoImgUrl;


    /**
     * 启用停用标志
     */
    private Integer enable;


    private Integer del;


    @JsonIgnore
    private Long chanUser;
    @JsonIgnore
    private Long chanTime;
    @JsonIgnore
    private Long chanRequest;

    @JsonProperty(value = "symptom_list")
    private List<SymptomVo> symptomList;

    /**
     * 是否治疗科室 0否1是
     */
    @JsonProperty(value = "is_treat")
    private Integer isTreat;

    /**
     * 科室备注
     */
    @JsonProperty(value = "memo")
    private String memo;

    /**
     * 探视限制时间
     */
    @JsonProperty(value = "visit_duration_limit")
    private Integer visitDurationLimit;

    /**
     * 院区名称
     */
    @JsonProperty(value = "district_name")
    private String districtName;


    /**
     * 院区id
     */
    @JsonProperty(value = "district_id")
    @JsonDeserialize(using = String2LongDeserializer.class)
    @JsonSerialize(using = Long2StringSerializer.class)
    private Long districtId;


    @JsonProperty(value = "user_list")
    private List<UserVo> userList;

}

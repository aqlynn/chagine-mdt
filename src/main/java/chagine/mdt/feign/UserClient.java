package chagine.mdt.feign;

import chagine.core.Env;
import chagine.core.ErrorCode;
import chagine.core.exception.SeeingRuntimeException;
import chagine.core.restful.util.TResponse;
import chagine.core.util.Assert;
import chagine.core.util.FeignUtil;
import chagine.core.util.ServiceName;
import chagine.mdt.feign.vo.DoctorVO;
import chagine.mdt.feign.vo.UserVo;
import chagine.mdt.feign.vo.YesNoVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * create by wba on  2020/3/13
 * qq:43585061
 */
@FeignClient(ServiceName.BASIC_SERVICE)
public interface UserClient {

    @RequestMapping(value = "/feign/user/get_user", method = RequestMethod.GET)
    TResponse<UserVo> getUser(@RequestParam(value = "user_id") Long userId, @RequestParam(value = "tena_id", required = false) Long tenaId);

    default UserVo detailGetUser(Long userId, Long tenaId) {
        try {
            TResponse<UserVo> tResponse = getUser(userId, tenaId);
            Assert.error(!tResponse.isSuccess(), ErrorCode.BUSINESS_EXCEPTION, "无效的用户ID：" + userId);
            UserVo vo = tResponse.getData();
            Assert.error(vo == null, ErrorCode.BUSINESS_EXCEPTION, "无效数据,用户ID：" + userId);
            return vo;
        } catch (Exception e) {
            throw new SeeingRuntimeException(ErrorCode.BUSINESS_EXCEPTION, e.getMessage());
        }
    }

    @RequestMapping(value = "/feign/user/get_user_list_by_function_code", method = RequestMethod.GET)
    TResponse<List<UserVo>> getUserListByFunctionCode(@RequestParam(value = "tena_id", required = false) Long tenaId,
                                                      @RequestParam(value = "function_code", required = false) String functionCode);

    default List<UserVo> detailUserListByFunctionCode(Long tenaId, String functionCode) {
        try {
            TResponse<List<UserVo>> tResponse = getUserListByFunctionCode(tenaId, functionCode);
            Assert.error(!tResponse.isSuccess(), ErrorCode.BUSINESS_EXCEPTION, tResponse.getMessage());
            return tResponse.getData();
        } catch (Exception e) {
            throw new SeeingRuntimeException(ErrorCode.BUSINESS_EXCEPTION, e.getMessage());
        }
    }

    @GetMapping("/doctor/get_doctor_list")
    TResponse<List<DoctorVO>> getDoctorList(@RequestParam(value = "key", required = false) String key,
                                            @RequestParam(value = "depa_id", required = false) Long depaId,
                                            @RequestParam(value = "buz_type", required = false) Integer buzType,
                                            @RequestHeader(value = Env.HEAD_TENANT_KEY) Long tenantId);

    default List<DoctorVO> detailDoctorListNotFound(Long tenantId) {
        try {
            TResponse<List<DoctorVO>> tResponse = getDoctorList(null, null, null, tenantId);
            Assert.error(!tResponse.isSuccess(), ErrorCode.BUSINESS_EXCEPTION, tResponse.getMessage());
            return tResponse.getData();
        } catch (Exception e) {
            throw new SeeingRuntimeException(ErrorCode.BUSINESS_EXCEPTION, e.getMessage());
        }
    }

    @GetMapping(value = "/feign/function/has_function")
    TResponse<YesNoVo> getFunction(@RequestParam(value = "user_id", required = false) Long userId,
                                   @RequestParam(value = "func_code", required = false) String funcCode);

    default boolean detailHasFunction(Long userId, String funcCode) {
        try {
            TResponse<YesNoVo> tResponse = getFunction(userId, funcCode);
            Assert.error(!tResponse.isSuccess(), ErrorCode.BUSINESS_EXCEPTION, tResponse.getMessage());
            YesNoVo data = tResponse.getData();
            Assert.error(data == null, ErrorCode.BUSINESS_EXCEPTION, "返回数据为null");
            return data.getRst() == 1;
        } catch (Exception e) {
            throw new SeeingRuntimeException(ErrorCode.BUSINESS_EXCEPTION, e.getMessage());
        }
    }



    @RequestMapping(value = "/feign/user/get_user_by_his_doctor_id", method = RequestMethod.GET)
    TResponse<UserVo> getUserByHisDoctorId(@RequestParam(value = "his_doctor_id") String hisDoctorId, @RequestParam(value = "tena_id", required = false) Long tenaId);

    default UserVo detailUserByHisDoctorId(String hisDoctorId, Long tenaId) {
        try {
            TResponse<UserVo> response = getUserByHisDoctorId(hisDoctorId, tenaId);
            UserVo user =  FeignUtil.getResponseData(response);
            return user;
        } catch (Exception e) {
            return null;
        }


    }
}

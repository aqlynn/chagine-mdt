package chagine.mdt.feign;

import chagine.core.Env;
import chagine.core.restful.util.TResponse;
import chagine.core.util.ServiceName;
import chagine.mdt.feign.vo.FeignPatientVo;
import chagine.mdt.feign.vo.PatientVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * create by wba on  2020/3/13
 * qq:43585061
 */
@FeignClient(ServiceName.BASIC_SERVICE)
public interface PatientClient {

    @RequestMapping(value = "/feign/patient/get_patient", method = RequestMethod.GET)
    TResponse<FeignPatientVo> getPatient(@RequestParam(value = "patient_id") Long patientId, @RequestParam(value = "tena_id", required = false) Long tenaId);

    @GetMapping("/patient/select_patient_list")
    TResponse<List<PatientVo>> selectPatientList(@RequestParam(value = "key", required = false) String key,
                                                 @RequestHeader(value = Env.HEAD_TENANT_KEY) Long tenantId,
                                                 @RequestHeader(value = Env.HEAD_USERID_KEY) Long userId);

}

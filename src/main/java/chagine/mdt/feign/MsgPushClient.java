package chagine.mdt.feign;

import chagine.core.restful.util.TResponse;
import chagine.core.util.ServiceName;
import chagine.mdt.feign.vo.msgpush.MsgPushApiRequest;
import chagine.mdt.feign.vo.msgpush.MsgPushReqBaseVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 消息中心接口
 */
@FeignClient(ServiceName.MSG_PUSH_SERVICE)
public interface MsgPushClient {

    /**
     * 系统内调用，不需要进行数据签名
     */
    @PostMapping(value = "/pass_open_api/push_message", consumes = MediaType.APPLICATION_JSON_VALUE)
    <T extends MsgPushReqBaseVo> TResponse<Object> pushMessage(@RequestBody MsgPushApiRequest<T> requestVo);

}

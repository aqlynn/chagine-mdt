package chagine.mdt.controller;

import chagine.core.annotation.AccessLimit;
import chagine.core.annotation.ApiVersion;
import chagine.core.annotation.CommonResult;
import chagine.mdt.test.TestTable;
import chagine.mdt.test.TestTableMapper;
import chagine.mdt.vo.test.TestVo;
import com.alibaba.fastjson.JSONPath;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@Validated
@CommonResult // 结果处理注解
@RestController
@RequestMapping("/test")
public class TestController {


    @AccessLimit(maxRequestCount = 10, timeout = 1, timeUnit = TimeUnit.MINUTES) // 请求频率注解
    @ApiVersion(value = "0.0.0") // 版本注解
    @PostMapping("/test")
    public TestVo testV0_0_0(@RequestBody TestVo inputVo) {
        System.out.println("inputVo = " + inputVo);
        return inputVo;
    }


    @ApiVersion(value = "1.0.0")
    @PostMapping("/test")
    public TestVo testV1_0_0(@RequestBody TestVo inputVo) {
        inputVo.setNumber(inputVo.getNumber() + 100);
        System.out.println("inputVo = " + inputVo);
        return inputVo;
    }



    @Resource
    private TestTableMapper testTableMapper;

    @GetMapping("/test_insert")
    public Object testInsert() {
        TestTable testTable = new TestTable();
        testTable.setTestDate(LocalDateTime.now());
        testTable.setParams("1");
        testTableMapper.insert(testTable);
        return "success";
    }

    @GetMapping("/test_update")
    public Object testUpdate() {
        LambdaUpdateWrapper<TestTable> updateWrapper = Wrappers.lambdaUpdate();
        updateWrapper.eq(TestTable::getParams, "1");
        TestTable testTable = new TestTable();
        testTable.setTestDate(LocalDateTime.now());
        testTable.setChanUser("1111");
        testTableMapper.update(testTable, updateWrapper);

        // TODO 某些字段不设置为自动填充

        return "success";
    }




    @GetMapping("/time_out")
    public DeferredResult<String> testDeferredResult() {
        DeferredResult<String> deferredResult = new DeferredResult<>(10000L, "no result");
        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(5);
                deferredResult.setResult("10000");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        return deferredResult;
    }


    public static void main(String[] args) {
        Long Bed = Long.valueOf((String) JSONPath.read("{\"Bed\":\"0\",\"PR\":\"\",\"Temp\":\"\",\"NBPs\":\"\",\"NBPm\":\"\",\"Resp\":\"\",\"SpO2\":\"\",\"EquipId\":\"A1AC74E4\",\"NBPTime\":\"\",\"NBPd\":\"\",\"NBP\":\"\",\"deptCode\":\"219\"}", "$.Bed"));
        System.out.println(Bed);
    }
}

package chagine.mdt.controller;

import chagine.core.annotation.CommonResult;
import chagine.core.current.CurrentEnv;
import chagine.mdt.po.yy.YySysPatient;
import chagine.mdt.po.yy.YySysUser;
import chagine.mdt.service.sqlservice.yy.YySysPatientService;
import chagine.mdt.service.sqlservice.yy.YySysUserService;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;



/**
 * 住院MDT
 */
@Validated
@CommonResult
@RestController
@RequestMapping("/inpatient_mdt")
public class InpatientMdtController {

    @Resource
    private YySysPatientService yySysPatientService;

    @Resource
    private YySysUserService yySysUserService;

    /**
     * 获取住院患者
     */
    @GetMapping("/get_patient_info")
    public YySysPatient getPatientInfo(@RequestParam(value = "patientid", required = false) String patientid) {

        YySysPatient yySysPatient = yySysPatientService.getById(patientid);
        if(yySysPatient != null  && yySysPatient.getChiefDoctor() != null){
            YySysUser yySysUser = yySysUserService.getOne(
                    Wrappers.<YySysUser>lambdaQuery()
                            .select(YySysUser::getUsname)
                            .eq(YySysUser::getUserid, yySysPatient.getChiefDoctor())
            );
            yySysPatient.setChiefDoctorName(yySysUser.getUsname());
        }

        return yySysPatient;
    }


    /**
     * 获取医生信息
     */
    @GetMapping("/get_doctor_info")
    public YySysUser getPatientInfo() {
        String employeeNo = CurrentEnv.employeeNo();
        YySysUser yySysUser = yySysUserService.getById(employeeNo);
        return yySysUser;
    }


}

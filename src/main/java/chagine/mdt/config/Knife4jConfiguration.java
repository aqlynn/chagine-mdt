package chagine.mdt.config;

import io.swagger.annotations.Api;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfiguration {

    @Bean(value = "defaultApi1")
    public Docket defaultApi1() {
        return getDocket("A全部接口", "/**");
    }


    private Docket getDocket(String groupName, String path) {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("接口文档")
                        .description("# swagger-bootstrap-ui-demo RESTful APIs")
                        .termsOfServiceUrl("")
                        .contact(new Contact("", "", ""))
                        .version("1.0")
                        .build())
                //分组名称
                .groupName(groupName)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(PathSelectors.ant(path))
                .build()
                .pathMapping("/")
//                .globalOperationParameters(getParameters())
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());
    }

//    private List<Parameter> getParameters() {
//        return Arrays.asList(
//                new ParameterBuilder().parameterType("header").required(false)
//                        .name("token").description("令牌").modelRef(new ModelRef("string"))
//                        .build(),
//                new ParameterBuilder().parameterType("header").required(true)
//                        .name("tenantId").description("租户").modelRef(new ModelRef("string"))
//                        .build(),
//                new ParameterBuilder().parameterType("header").required(true)
//                        .name("userid").description("用户").modelRef(new ModelRef("string"))
//                        .build()
//        );
//    }

    private List<SecurityContext> securityContexts() {
        List<SecurityContext> securityContextList = new ArrayList<>();
        List<SecurityReference> securityReferenceList = new ArrayList<>();
        securityReferenceList.add(new SecurityReference("token", scopes()));
        securityContextList.add(SecurityContext
                .builder()
                .securityReferences(securityReferenceList)
                .forPaths(PathSelectors.any())
                .build()
        );
        return securityContextList;
    }

    private AuthorizationScope[] scopes() {
        return new AuthorizationScope[]{new AuthorizationScope("global", "accessAnything")};
    }

    private List<ApiKey> securitySchemes() {
        List<ApiKey> apiKeyList = new ArrayList<>();
//        apiKeyList.add(new ApiKey("token", "token", "header"));
        apiKeyList.add(new ApiKey("租户", "tenantid", "header"));
        apiKeyList.add(new ApiKey("用户", "userid", "header"));
        return apiKeyList;
    }
}

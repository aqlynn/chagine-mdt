package chagine.mdt.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 定时器配置
 */
@Data
@Configuration
@ConfigurationProperties(ParamsScheduleProperties.PREFIX)
public class ParamsScheduleProperties {

    public static final String PREFIX = "custom.schedule";


    private boolean enabledTest = false;

}

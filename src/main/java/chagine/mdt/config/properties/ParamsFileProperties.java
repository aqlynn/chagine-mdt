package chagine.mdt.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(ParamsFileProperties.PREFIX)
public class ParamsFileProperties {

    public static final String PREFIX = "custom.file";

    /**
     * 外网Host
     */
    public String extranetHost;

    /**
     * 内网Host
     */
    public String intranetHost;

    /**
     * host环境
     */
    public String env;

    public enum EnvType {
        EXTRANET("extranet", "外网"),
        INTRANE("intrane", "内网"),
        ;

        public final String name;
        public final String detail;
        public final static EnvType[] VALUES = values();

        EnvType(String name, String detail) {
            this.name = name;
            this.detail = detail;
        }

        public boolean equals(String env) {
            return env.equals(name);
        }

        public static EnvType findEnv(String env) {
            for (EnvType v : VALUES) {
                if (v.name.equals(env)) {
                    return v;
                }
            }
            return null;
        }
    }
}

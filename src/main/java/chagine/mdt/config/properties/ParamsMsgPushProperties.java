package chagine.mdt.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(ParamsMsgPushProperties.PREFIX)
public class ParamsMsgPushProperties {

    public static final String PREFIX = "custom.msg-push";

    /**
     * 账号Key
     */
    private String accessKey;

    /**
     * 对应的私钥
     */
    private String secretKey;

}

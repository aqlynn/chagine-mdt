package chagine.mdt.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(ParamsAdapterProperties.PREFIX)
public class ParamsAdapterProperties {

    public static final String PREFIX = "custom.adapter";

    /**
     * 一体化Adapter的host
     */
    private String apiIntegrationAdapterHost;

    /**
     * Ar的Adapter的host
     */
    private String apiArAdapterHost;





}

package chagine.mdt.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(ParamsCaSignProperties.PREFIX)
public class ParamsCaSignProperties {

    public static final String PREFIX = "custom.ca.sign";

    /**
     * 一体化Adapter的host
     */
    private String doctorSignStatusRedisMessagePushKey;
    /**
     * 是否开启，检查医生端CA签名的状态
     */
    private Boolean enabledCheckCaSignDoctorStatus = false;

    /**
     * 是否开启，患者CA签名测试环境
     */
    private Boolean enabledCaSignPatientStatus = false;

    /**
     * 患者CA签名，生产环境URL
     */
    private String productCaSignUrl;

}

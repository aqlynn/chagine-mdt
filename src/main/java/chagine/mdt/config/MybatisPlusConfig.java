package chagine.mdt.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisPlusConfig {

    /**
     * 配置-分页插件
     * 配置-乐观锁插件
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();

//        // 动态表名，仅适合mybatis-plus的CRUD的SQL，此配置必须在翻页插件前
//        DynamicTableNameInnerInterceptor dynamicTableNameInnerInterceptor = new DynamicTableNameInnerInterceptor();
//        dynamicTableNameInnerInterceptor.setTableNameHandler((sql, tableName) -> {
//            if (EwsOriginService.TABLE_NAME.equals(tableName)) {
//
//                // 获取参数方法
//                Integer year = RequestDataHelper.getRequestData("year");
//                return tableName + "_" + year;
//            } else if (FailMsgService.TABLE_BASENAME.equals(tableName)) {
//                // 获取参数方法
//                Integer year = RequestDataHelper.getRequestData("year");
//                return tableName + "_" + year;
//            }
//            return tableName;
//
//        });
//        interceptor.addInnerInterceptor(dynamicTableNameInnerInterceptor);

        // 翻页插件
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        paginationInnerInterceptor.setMaxLimit(-1L);
        interceptor.addInnerInterceptor(paginationInnerInterceptor);

        // 乐观锁插件
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());

        return interceptor;
    }

}

package chagine.mdt.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class DataIdUtil {

    public static <T extends Number> String getIdsByList(List<T> ids) {
        StringBuilder sb = new StringBuilder();
        if (ids == null || ids.isEmpty()) {
            return sb.toString();
        }
        for (int i = 0; i < ids.size(); i++) {
            T t = ids.get(i);
            if (t != null) {
                sb.append(t);
                if (i != ids.size() - 1) {
                    sb.append(",");
                }
            }
        }
        return sb.toString();
    }

    public static List<Integer> getListIntByIds(String ids) {
        if (StringUtils.isBlank(ids)) {
            return new ArrayList<>();
        }
        List<Integer> list = new ArrayList<>();
        String[] split = ids.split(",");
        for (String s : split) {
            if (StringUtils.isNotBlank(s)) {
                list.add(Integer.valueOf(s));
            }
        }
        return list;
    }

    public static List<Long> getListLongByIds(String ids) {
        if (StringUtils.isBlank(ids)) {
            return new ArrayList<>();
        }
        List<Long> list = new ArrayList<>();
        String[] split = ids.split(",");
        for (String s : split) {
            if (StringUtils.isNotBlank(s)) {
                list.add(Long.valueOf(s));
            }
        }
        return list;
    }

}

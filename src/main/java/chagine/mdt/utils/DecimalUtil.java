package chagine.mdt.utils;

import java.math.BigDecimal;

public class DecimalUtil {
    private final BigDecimal value;

    private DecimalUtil(BigDecimal value) {
        this.value = value;
    }

    private static BigDecimal objectOfDecimal(Object val) {
        BigDecimal bigDecimal = null;
        if (val instanceof Number) {
            bigDecimal = BigDecimal.valueOf(((Number) val).longValue());
        }
        if (val instanceof Long) {
            bigDecimal = BigDecimal.valueOf((Long) val);

        }
        if (val instanceof Integer) {
            bigDecimal = BigDecimal.valueOf((Integer) val);

        }
        if (val instanceof String) {
            bigDecimal = BigDecimal.valueOf(Double.parseDouble(val.toString()));

        }
        if (val instanceof Double) {
            bigDecimal = BigDecimal.valueOf((Double)val);

        }

        return bigDecimal;
    }

    public static DecimalUtil of(Object value) {
        return new DecimalUtil(objectOfDecimal(value));
    }

    // 小于
    public boolean lt(Object val) {
        return value.compareTo(objectOfDecimal(val)) == -1;
    }

    // 等于
    public boolean eq(Object val) {
        return value.compareTo(objectOfDecimal(val)) == 0;
    }

    // 大于
    public boolean gt(Object val) {
        return value.compareTo(objectOfDecimal(val)) == 1;
    }

    // 大于等于
    public boolean gte(Object val) {
        return value.compareTo(objectOfDecimal(val)) > -1;
    }

    // 小于等于
    public boolean lte(Object val) {
        return value.compareTo(objectOfDecimal(val)) < 1;
    }


}

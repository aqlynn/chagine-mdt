package chagine.mdt.utils;

public class ResFileUtil {

    /**
     * 获取Res服务Url的Host
     */
    public static String getResHost(String url) {
        int resIndex = url.indexOf("/res", 8);
        return url.substring(0, resIndex);
    }

    /**
     * 获取Res服务Url的Path
     */
    public static String getResPath(String url) {
        int resIndex = url.indexOf("/res", 8);
        return url.substring(resIndex + 5);
    }

    /**
     * 替换路径的host
     *
     * @param url         原url
     * @param replaceHost 替换的host
     */
    public static String replaceUrlHost(String url, String replaceHost) {
        String resPath = getResPath(url);
        if (replaceHost.endsWith("/")) {
            return replaceHost + "res/" + resPath;
        } else {
            return replaceHost + "/res/" + resPath;
        }
    }

    /**
     * 生成新的url
     *
     * @param host    域名
     * @param resPath 文件路径
     */
    public static String createNewUrl(String host, String resPath) {
        if (host.endsWith("/")) {
            return host + "res/" + resPath;
        } else {
            return host + "/res/" + resPath;
        }
    }

}

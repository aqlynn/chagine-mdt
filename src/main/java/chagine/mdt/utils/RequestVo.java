package chagine.mdt.utils;

import lombok.Data;

/**
 * create by wba on  2020/3/23
 * qq:43585061
 */
@Data
public class RequestVo {
    //响应码 200 成功
    private int status;
    // 成功则成功信息，失败则失败信息

    private String message;

}

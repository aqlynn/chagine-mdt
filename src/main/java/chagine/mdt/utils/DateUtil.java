package chagine.mdt.utils;

import chagine.core.ErrorCode;
import chagine.core.exception.SeeingRuntimeException;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @program: search
 * @description: Date操作类
 * @author: WangBin
 * @create: 2018-08-01 11:13
 **/
public class DateUtil {


    /**
     * 获取当前时间String类型
     */
    public static String getNowString(String pattern) {
        if (StringUtil.isEmpty(pattern)) {
            throw new SeeingRuntimeException(ErrorCode.RUNNING_EXCEPTION, "日期转换异常,参数不能为空");
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.format(new Date());
        } catch (Exception e) {
            throw new SeeingRuntimeException(ErrorCode.RUNNING_EXCEPTION, "日期转换异常");
        }
    }


    /**
     * Date 转 LocalDateTime
     */
    public static LocalDateTime dateToLocalDateTime(Date date) {
        try {
            LocalDateTime localTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            return localTime;
        } catch (Exception ex) {
            throw new SeeingRuntimeException(ErrorCode.RUNNING_EXCEPTION, "日期转换异常");
        }
    }


    /**
     * String 转 LocalDateTime
     */
    public static LocalDateTime stringToLocalDateTime(String dateTime, String pattern) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            Date date = sdf.parse(dateTime);
            LocalDateTime localTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            return localTime;
        } catch (Exception ex) {
            throw new SeeingRuntimeException(ErrorCode.RUNNING_EXCEPTION, "日期转换异常");
        }
    }

    /**
     * LocalDateTime 转 Date
     */
    public static Date localDateTimeToDate(LocalDateTime localTime) {
        if (localTime == null) {
            throw new SeeingRuntimeException(ErrorCode.RUNNING_EXCEPTION, "日期转换异常,参数不能为空");
        }
        try {
            Date date = Date.from(localTime.atZone(ZoneId.systemDefault()).toInstant());
            return date;
        } catch (Exception ex) {
            throw new SeeingRuntimeException(ErrorCode.RUNNING_EXCEPTION, "日期转换异常");
        }
    }


    /**
     * LocalDateTime 转 String
     */
    public static String localDateTimeToString(LocalDateTime localTime, String pattern) throws SeeingRuntimeException {
        try {
            Date date = Date.from(localTime.atZone(ZoneId.systemDefault()).toInstant());
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            String str = sdf.format(date);
            return str;
        } catch (Exception ex) {
            throw new SeeingRuntimeException(ErrorCode.RUNNING_EXCEPTION, "日期转换异常");
        }
    }


    /**
     * String 转 Date
     */
    public static Date stringToDateTime(String dateTime, String pattern) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            Date date = sdf.parse(dateTime);
            return date;
        } catch (Exception ex) {
            throw new SeeingRuntimeException(ErrorCode.RUNNING_EXCEPTION, "日期转换异常");
        }
    }


    /**
     * Date 转 String
     */
    public static String dateToString(Date dateTime, String pattern) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            String date = sdf.format(dateTime);
            return date;
        } catch (Exception ex) {
            throw new SeeingRuntimeException(ErrorCode.RUNNING_EXCEPTION, "日期转换异常");
        }
    }


    public static String stringToString(String dateTime, String pattern, String pattern1) {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            Date date = sdf.parse(dateTime);
            System.out.println(" date==" + date);

            SimpleDateFormat sdf1 = new SimpleDateFormat(pattern1);
            String str = sdf1.format(date);
            System.out.println(" str==" + str);
            return str;
        } catch (Exception ex) {
            throw new SeeingRuntimeException(ErrorCode.RUNNING_EXCEPTION, "日期转换异常");
        }

    }


}

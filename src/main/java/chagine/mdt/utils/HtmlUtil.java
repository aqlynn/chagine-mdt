package chagine.mdt.utils;

import cn.hutool.core.map.MapUtil;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HtmlUtil {
    public static final String DOCTOR_SIGN_PIC_URL = "SIGN_PIC_URL";
    public static final String DOCTOR_SIGN_TIME = "SIGN_TIME";
    public static final String PATIENT_SIGN_PIC_URL = "PATIENT_SIGN_PIC_URL";
    public static final String PATIENT_SIGN_ID_NUMBER = "PATIENT_SIGN_ID_NUMBER";
    public static final String PATIENT_SIGN_TIME = "PATIENT_SIGN_TIME";
    public static final String FAMILY_SIGN_PIC_URL = "FAMILY_SIGN_PIC_URL";
    public static final String FAMILY_SIGN_ID_NUMBER = "FAMILY_SIGN_ID_NUMBER";
    public static final String FAMILY_SIGN_TIME = "FAMILY_SIGN_TIME";
    private static final String TEXT_HTML = "<div style=\"float:left;height:20px;margin-top:15px;\">#{title}：</div>" +
            "<div style=\"float:left;position:relative;text-align:center;height:20px;width:#{width}px;border-bottom:1px solid black;margin-top:15px;\" >#{value}</div>" +
            "<div style=\"clear: both;\"></div>";

    private static final String IMG_HTML = "<div style=\"float:left;height:20px;margin-top:15px;\">#{title}：</div>" +
            "<div style=\"margin-top:15px;height:20px;float:left;position:relative;text-align:center;width:110px;border-bottom:1px solid black;\" >" +
            "    <img style=\"position:absolute;height:32px;top:-13px;left:15px;\" src='#{value}' alt=''/></div>" +
            "<div style=\"clear: both;\"></div>";

    private static final Map<String, ElementValue> ELEMENT_VALUE_TYPE_MAP = MapUtil.<String, ElementValue>builder()
            // 医生签名
            .put("p$static@qianmin--g$QM.YS.GROUP.01--e$QM.YS.01", new ElementValue(Type.IMG, DOCTOR_SIGN_PIC_URL, 110))
            // 医生知情同意签字日期
            .put("p$static@qianmin--g$QM.YS.GROUP.01--e$DE06.00.156.00", new ElementValue(Type.TEXT, DOCTOR_SIGN_TIME, 90))
            // 患者签名
            .put("p$static@qianmin--g$QM.HZ.GROUP.01--e$QM.HZ.01", new ElementValue(Type.IMG, PATIENT_SIGN_PIC_URL, 110))
            // 患者身份证件号码
            .put("p$static@qianmin--g$QM.HZ.GROUP.01--e$DE02.01.030.00", new ElementValue(Type.TEXT, PATIENT_SIGN_ID_NUMBER, 180))
            // 患者知情同意签字日期
            .put("p$static@qianmin--g$QM.HZ.GROUP.01--e$DE06.00.156.00", new ElementValue(Type.TEXT, PATIENT_SIGN_TIME, 90))
            // 家属签名
            .put("p$static@qianmin--g$QM.JS.GROUP.01--e$QM.JS.01", new ElementValue(Type.IMG, FAMILY_SIGN_PIC_URL, 110))
            // 家属身份证件号码
            .put("p$static@qianmin--g$QM.JS.GROUP.01--e$DE02.01.030.00", new ElementValue(Type.TEXT, FAMILY_SIGN_ID_NUMBER, 180))
            // 家属知情同意签字日期
            .put("p$static@qianmin--g$QM.JS.GROUP.01--e$DE06.00.156.00", new ElementValue(Type.TEXT, FAMILY_SIGN_TIME, 90))
            .build();

    enum Type {
        TEXT, IMG
    }

    @NoArgsConstructor
    @AllArgsConstructor
    static class ElementValue {
        Type type;

        String column;

        Integer width;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    static class ElementAndTitle {
        Element dataElement;

        String title;
    }

    public static void main(String[] args) {
        String xmlStr = getStr2();

        long t1 = System.currentTimeMillis();
//        String html = newHtml(xmlStr);
//        System.out.println("最终html:" + html);
//        System.out.println("时间:" + (System.currentTimeMillis() - t1));
//        System.out.println("最终newDocument:" + StringEscapeUtils.unescapeXml(newDocument.outerHtml()));

//        List<Element> dataElements = document.select("data-element > data-name [data-datatype]");
//        for (Element dataElement : dataElements) {
//            String title = dataElement.ownText();
//            System.out.println("标题:" + title);
//            Element parent = dataElement.parent().parent().parent();
//            String attr = parent.attr("data-compose_code");
//            String[] es = attr.split("--");
//            String e = es[es.length - 1];
//            String[] e1s = e.split("\\$");
//            String e1 = e1s[e1s.length - 1];
//            System.out.println("数据元:" + e1);
//            System.out.println("parent:" + parent);
//            System.out.println("dataElement:" + dataElement);
//            Element newElement = dataElement.tagName("div");
//            System.out.println("newElement:" + newElement);
//        }

//        Document document = Jsoup.parse(xmlStr);
//        Element element = document.selectFirst("data-element > data-name [data-datatype]");
//        Element dataElement = element.parent().parent().parent();
////        a(dataElement);
//        clearOtherTag(dataElement);

//        dataElement.empty();

        String s = newHtml(xmlStr);
//
        System.out.println("alldataElement:" + s);
    }

//    public static void a(Element element) {
//        System.out.println("element:" + element);
//        Element element1 = element.nextElementSibling();
//        if (element1 != null) {
//            a(element1);
//        }
//    }

    public static String newHtml(String html) {
        Document document = Jsoup.parse(html);

        String dataElementHtml = dataElementHtml(document);

        return StringUtils.replacePattern(html, "<data-element(([\\s\\S])*)<\\/data-element>", dataElementHtml);


//        Document newDocument = newDocument(document);
//
//        return newDocument.body().unwrap().outerHtml();
    }

    public static String replaceHtml(String html, Map<String, Object> valueMap) {
        for (String k : valueMap.keySet()) {
            Object v = valueMap.get(k);
            if (v != null) {
                html = html.replace("#{" + k + "}", v.toString());
            }
        }
        return html;
    }

    public static String newHtmlAndReplace(String html, Map<String, Object> valueMap) {

        return replaceHtml(newHtml(html), valueMap);
    }

    private static String dataElementHtml(Document document) {
        if (document == null) {
            return null;
        }
        Map<String, String> columnMap = new HashMap<>();
        StringBuilder newHtml = new StringBuilder();
        // 选中
        List<Element> elements = document.select("data-element > data-name [data-datatype]");
        for (Element element : elements) {
            if (element == null) {
                continue;
            }
            String title = element.ownText();
            // 标题
            System.out.println("标题:" + title);
            // 数据元
            Element dataElement = element.parent().parent().parent();
            String attr = dataElement.attr("data-compose_code");
            columnMap.put(attr, title);

            ElementValue elementValue = ELEMENT_VALUE_TYPE_MAP.get(attr);
            String divHtml = "";
            switch (elementValue.type) {
                case IMG:
                    divHtml = IMG_HTML.replace("#{title}", title).replace("#{value}", "#{" + elementValue.column + "}");
                    break;
                case TEXT:
                    divHtml = TEXT_HTML.replace("#{width}", elementValue.width.toString()).replace("#{title}", title).replace("#{value}", "#{" + elementValue.column + "}");
                    break;
                default:
                    break;
            }
            newHtml.append(divHtml);
        }
        System.out.println("newHtml:" + newHtml);
        return newHtml.toString();
    }


//    private static Document newDocument(Document document) {
//        if (document == null) {
//            return null;
//        }
//        Map<String, String> columnMap = new HashMap<>();
//        // 选中
//        List<Element> elements = document.select("data-element > data-name [data-datatype]");
//        for (Element element : elements) {
//            if (element == null) {
//                continue;
//            }
//            String title = element.ownText();
//            // 标题
//            System.out.println("标题:" + title);
//            // 数据元
//            Element dataElement = element.parent().parent().parent();
//            String attr = dataElement.attr("data-compose_code");
//            columnMap.put(attr, title);
//
//            ElementValue elementValue = ELEMENT_VALUE_TYPE_MAP.get(attr);
//            String divHtml = "";
//            switch (elementValue.type) {
//                case IMG:
//                    divHtml = IMG_HTML.replace("${title}", title).replace("${value}", "${" + elementValue.column + "}");
//                    break;
//                case TEXT:
//                    divHtml = TEXT_HTML.replace("${width}", elementValue.width.toString()).replace("${title}", title).replace("${value}", "${" + elementValue.column + "}");
//                    break;
//                default:
//                    break;
//            }
//        }
//        System.out.println("divHtml:" + divHtml);
//        // 替换数据元值
//        dataElement.html(divHtml);
//        dataElement = (Element) dataElement.unwrap();
//
//        System.out.println("dataElement:" + dataElement);
//        Document newDocument = dataElement.parent().ownerDocument();
//        System.out.println("newDocument:" + newDocument);
//        return newDocument(newDocument);
//    }

//    private static Document newDocument(Document document) {
//        if (document == null) {
//            return null;
//        }
//        // 选中
//        Element element = document.selectFirst("data-element > data-name [data-datatype]");
//        if (element == null) {
//            return document;
//        }
//        String title = element.ownText();
//        // 标题
//        System.out.println("标题:" + title);
//        // 数据元
//        Element dataElement = element.parent().parent().parent();
//        String attr = dataElement.attr("data-compose_code");
//        ElementValue elementValue = ELEMENT_VALUE_TYPE_MAP.get(attr);
//        String divHtml = "";
//        switch (elementValue.type) {
//            case IMG:
//                divHtml = IMG_HTML.replace("${title}", title).replace("${value}", "${" + elementValue.column + "}");
//                break;
//            case TEXT:
//                divHtml = TEXT_HTML.replace("${width}", elementValue.width.toString()).replace("${title}",
//                        title).replace("${value}", "${" + elementValue.column + "}");
//                break;
//            default:
//                break;
//        }
//        System.out.println("divHtml:" + divHtml);
//        // 替换数据元值
//        dataElement.html(divHtml);
//        dataElement = (Element) dataElement.unwrap();
//            Element elementSibling = dataElement.nextElementSibling();
//            if (elementSibling != null) {
//                if (StringUtils.equalsIgnoreCase("data-element", elementSibling.tagName())) {
//
//                }
//            }
//
//            System.out.println("dataElement:" + dataElement);
//        Document newDocument = dataElement.parent().ownerDocument();
//        System.out.println("newDocument:" + newDocument);
//        return newDocument(newDocument);
//    }

    // 清除<br>
//    private static void clearOtherTag(Element dataElement) {
//        if (dataElement == null) {
//            return;
//        }
//        if (StringUtils.equalsIgnoreCase("br", dataElement.tagName())) {
//            dataElement.tagName("span");
//            clearOtherTag(dataElement.nextElementSibling());
//            return;
//        }
//        // 兄弟元素
//        Element elementSibling = dataElement.nextElementSibling();
//        if (elementSibling == null) {
//            return;
//        }
//
//        clearOtherTag(elementSibling);
//    }

//    private static Document newDocument(Document document) {
//        if (document == null) {
//            return null;
//        }
//        // 选中
//        Element element = document.selectFirst("data-element > data-name [data-datatype]");
//        if (element == null) {
//            return document;
//        }
//        String title = element.ownText();
//        // 标题
//        System.out.println("标题:" + title);
//        // 数据元
//        Element dataElement = element.parent().parent().parent();
//        String attr = dataElement.attr("data-compose_code");
////        String[] es = attr.split("--");
////        String e = es[es.length - 1];
////        String[] e1s = e.split("\\$");
////        String code = e1s[e1s.length - 1];
////        System.out.println("数据元:" + code);
//        ElementValue elementValue = ELEMENT_VALUE_TYPE_MAP.get(attr);
//        String divHtml = "";
//        switch (elementValue.type) {
//            case IMG:
//                divHtml = IMG_HTML.replace("${title}", title).replace("${value}", "${" + elementValue.column + "}");
//                break;
//            case TEXT:
//                divHtml = TEXT_HTML.replace("${title}", title).replace("${value}", "${" + elementValue.column + "}");
//                break;
//            default:
//                break;
//        }
//        System.out.println("divHtml:" + divHtml);
//        // 替换数据元值
//        dataElement.html(divHtml);
//        dataElement = (Element) dataElement.unwrap();
//
//        System.out.println("dataElement:" + dataElement);
//        Document newDocument = dataElement.parent().ownerDocument();
//        System.out.println("newDocument:" + newDocument);
//        return newDocument(newDocument);
//    }

    private static String getStr1() {
        return "<data-element data-type=\"data-element\" data-compose_code=\"p$static@qianmin--g$QM.YS.GROUP.01--e$QM.YS.01\"\n" +
                "    style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "    <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\">\n" +
                "        <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "            {{ <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "        </p>\n" +
                "\n" +
                "        <data-name>\n" +
                "            <color-rect data-bordercolor=\"rgb(228, 120, 44)\"\n" +
                "                style=\"border: 1px solid rgb(228, 120, 44); color: rgb(228, 120, 44); display: inline-flex; align-items: center;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                <span data-datatype=\"core_data\" style=\"color:rgb(228, 120, 44);\">医生签名</span>\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </color-rect>\n" +
                "        </data-name>\n" +
                "        <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "        :\n" +
                "        <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "        <data-value>\n" +
                "            <color-rect data-bordercolor=\"rgb(0, 143, 145)\"\n" +
                "                style=\"border: 1px solid rgb(0, 143, 145); color: rgb(0, 143, 145); display: inline-flex; align-items: center;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                <text-disable contenteditable=\"false\"\n" +
                "                    style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                    <span style=\"color:rgb(0, 143, 145)\">QM.YS.01</span>\n" +
                "                </text-disable>\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </color-rect>\n" +
                "        </data-value>\n" +
                "        <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "\n" +
                "\n" +
                "        <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>}}\n" +
                "        </p>\n" +
                "    </merge-tag>\n" +
                "</data-element>\n" +
                "<data-element data-type=\"data-element\" data-compose_code=\"p$static@qianmin--g$QM.YS.GROUP.01--e$DE06.00.156.00\"\n" +
                "    style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "    <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\">\n" +
                "        <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "            {{ <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "        </p>\n" +
                "\n" +
                "        <data-name>\n" +
                "            <color-rect data-bordercolor=\"rgb(228, 120, 44)\"\n" +
                "                style=\"border: 1px solid rgb(228, 120, 44); color: rgb(228, 120, 44); display: inline-flex; align-items: center;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                <span data-datatype=\"core_data\" style=\"color:rgb(228, 120, 44);\">知情同意签字日期</span>\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </color-rect>\n" +
                "        </data-name>\n" +
                "        <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "        :\n" +
                "        <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "        <data-value>\n" +
                "            <color-rect data-bordercolor=\"rgb(0, 143, 145)\"\n" +
                "                style=\"border: 1px solid rgb(0, 143, 145); color: rgb(0, 143, 145); display: inline-flex; align-items: center;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                <text-disable contenteditable=\"false\"\n" +
                "                    style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                    <span style=\"color:rgb(0, 143, 145)\">DE06.00.156.00</span>\n" +
                "                </text-disable>\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </color-rect>\n" +
                "        </data-value>\n" +
                "        <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "\n" +
                "\n" +
                "        <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>}}\n" +
                "        </p>\n" +
                "    </merge-tag>\n" +
                "</data-element>\n" +
                "<data-element data-type=\"data-element\" data-compose_code=\"p$static@qianmin--g$QM.HZ.GROUP.01--e$QM.HZ.01\"\n" +
                "    style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "    <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\"></merge-tag>\n" +
                "</data-element>\n" +
                "<div><br>&nbsp;<data-element data-type=\"data-element\" data-compose_code=\"p$static@qianmin--g$QM.HZ.GROUP.01--e$QM.HZ.01\"\n" +
                "        style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "        <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\">\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                {{ <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </p>\n" +
                "\n" +
                "            <data-name>\n" +
                "                <color-rect data-bordercolor=\"rgb(228, 120, 44)\"\n" +
                "                    style=\"border: 1px solid rgb(228, 120, 44); color: rgb(228, 120, 44); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <span data-datatype=\"core_data\" style=\"color:rgb(228, 120, 44);\">患者签名</span>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-name>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            :\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            <data-value>\n" +
                "                <color-rect data-bordercolor=\"rgb(0, 143, 145)\"\n" +
                "                    style=\"border: 1px solid rgb(0, 143, 145); color: rgb(0, 143, 145); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span style=\"color:rgb(0, 143, 145)\">QM.HZ.01</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-value>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "\n" +
                "\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>}}\n" +
                "            </p>\n" +
                "        </merge-tag>\n" +
                "    </data-element>&nbsp; &nbsp;<data-element data-type=\"data-element\"\n" +
                "        data-compose_code=\"p$static@qianmin--g$QM.HZ.GROUP.01--e$DE02.01.030.00\"\n" +
                "        style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "        <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\">\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                {{ <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </p>\n" +
                "\n" +
                "            <data-name>\n" +
                "                <color-rect data-bordercolor=\"rgb(228, 120, 44)\"\n" +
                "                    style=\"border: 1px solid rgb(228, 120, 44); color: rgb(228, 120, 44); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <span data-datatype=\"core_data\" style=\"color:rgb(228, 120, 44);\">身份证件号码</span>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-name>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            :\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            <data-value>\n" +
                "                <color-rect data-bordercolor=\"rgb(0, 143, 145)\"\n" +
                "                    style=\"border: 1px solid rgb(0, 143, 145); color: rgb(0, 143, 145); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span style=\"color:rgb(0, 143, 145)\">DE02.01.030.00</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-value>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "\n" +
                "\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>}}\n" +
                "            </p>\n" +
                "        </merge-tag>\n" +
                "    </data-element>&nbsp; &nbsp;<data-element data-type=\"data-element\"\n" +
                "        data-compose_code=\"p$static@qianmin--g$QM.HZ.GROUP.01--e$DE06.00.156.00\"\n" +
                "        style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "        <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\">\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                {{ <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </p>\n" +
                "\n" +
                "            <data-name>\n" +
                "                <color-rect data-bordercolor=\"rgb(228, 120, 44)\"\n" +
                "                    style=\"border: 1px solid rgb(228, 120, 44); color: rgb(228, 120, 44); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <span data-datatype=\"core_data\" style=\"color:rgb(228, 120, 44);\">知情同意签字日期</span>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-name>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            :\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            <data-value>\n" +
                "                <color-rect data-bordercolor=\"rgb(0, 143, 145)\"\n" +
                "                    style=\"border: 1px solid rgb(0, 143, 145); color: rgb(0, 143, 145); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span style=\"color:rgb(0, 143, 145)\">DE06.00.156.00</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-value>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "\n" +
                "\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>}}\n" +
                "            </p>\n" +
                "        </merge-tag>\n" +
                "    </data-element>&nbsp;&nbsp;<br></div>";
    }

    private static String getStr2() {
        return "<div><p class=\"MsoNormal\" style=\"margin-bottom: 6.0pt; mso-para-margin-bottom: .5gd; text-align: center; text-indent: 0cm; mso-char-indent-count: 0; line-height: 125%;\" align=\"center\" data-mce-style=\"margin-bottom: 6.0pt; mso-para-margin-bottom: .5gd; text-align: center; text-indent: 0cm; mso-char-indent-count: 0; line-height: 125%;\"><span style=\"font-family: 仿宋; font-size: 18pt;\" data-mce-style=\"font-family: 仿宋; font-size: 18pt;\"><strong style=\"mso-bidi-font-weight: normal;\" data-mce-style=\"mso-bidi-font-weight: normal;\"><span lang=\"EN-US\" style=\"line-height: 125%; color: black;\" data-mce-style=\"line-height: 125%; color: black;\">CMA</span></strong><strong style=\"mso-bidi-font-weight: normal;\" data-mce-style=\"mso-bidi-font-weight: normal;\"><span style=\"line-height: 125%; color: black;\" data-mce-style=\"line-height: 125%; color: black;\">遗传病分子检测知情同意书</span></strong></span></p><p class=\"MsoNormal\" style=\"text-indent: 21.0pt; mso-char-indent-count: 0; line-height: normal;\" data-mce-style=\"text-indent: 21.0pt; mso-char-indent-count: 0; line-height: normal;\"><span style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span lang=\"EN-US\" style=\"color: black;\" data-mce-style=\"color: black;\">CMA</span><span style=\"color: black;\" data-mce-style=\"color: black;\">是先进的基因芯片检测技术，以外周血、脐血、羊水、绒毛、流产组织中提取的</span><span lang=\"EN-US\" style=\"color: black;\" data-mce-style=\"color: black;\">DNA</span><span style=\"color: black;\" data-mce-style=\"color: black;\">作为检测样本，不仅能检出染色体的非整倍体、大片段的缺失和重复，而且可以发现核型分析等常规技术手段难以检出的染色体微缺失和重复，以及</span><span lang=\"EN-US\" style=\"color: black;\" data-mce-style=\"color: black;\">-CGH</span><span style=\"color: black;\" data-mce-style=\"color: black;\">芯片无法检测到的三倍体、单亲二倍体（</span><span lang=\"EN-US\" style=\"color: black;\" data-mce-style=\"color: black;\">UPD</span><span style=\"color: black;\" data-mce-style=\"color: black;\">）及杂合性缺失（</span><span lang=\"EN-US\" style=\"color: black;\" data-mce-style=\"color: black;\">LOH</span><span style=\"color: black;\" data-mce-style=\"color: black;\">）。应用</span><span lang=\"EN-US\" style=\"color: black;\" data-mce-style=\"color: black;\">CMA</span><span style=\"color: black;\" data-mce-style=\"color: black;\">技术进行产前、产后遗传病诊断是提高染色体疾病患儿检出率、查明病因并指导家庭二次生育的有效措施。</span></span></p><p class=\"MsoNormal\" style=\"text-indent: 21.1pt; line-height: 125%;\" data-mce-style=\"text-indent: 21.1pt; line-height: 125%;\"><span style=\"font-family: 仿宋; font-size: 14pt;\" data-mce-style=\"font-family: 仿宋; font-size: 14pt;\"><strong style=\"mso-bidi-font-weight: normal;\" data-mce-style=\"mso-bidi-font-weight: normal;\"><span style=\"line-height: 125%; color: black;\" data-mce-style=\"line-height: 125%; color: black;\">局限性和风险</span></strong></span><span style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"line-height: 125%; color: black;\" data-mce-style=\"line-height: 125%; color: black;\"><br></span></span></p><ol><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"color: black;\" data-mce-style=\"color: black;\">该检测不适用于染色体平衡易位、倒位、插入、嵌合体及基因点突变的检测；对于染色体微小缺失与重复，检测结果以</span><span lang=\"EN-US\" style=\"color: black;\" data-mce-style=\"color: black;\">500kb </span><span style=\"color: black;\" data-mce-style=\"color: black;\">以上片段重复及</span><span lang=\"EN-US\" style=\"color: black;\" data-mce-style=\"color: black;\">200kb </span><span style=\"color: black;\" data-mce-style=\"color: black;\">以上片段缺失作为截断值，超出此检测范围的片段不包含在结果报告内，结果报告仅供参考。</span></span></li><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-size: 12pt; font-family: 仿宋; color: black;\" data-mce-style=\"font-size: 12pt; font-family: 仿宋; color: black;\">在极少数情况下，受检者近期接受过异体输血、移植手术、干细胞治疗等，本检测结果可能受此类因素影响。</span></li><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-size: 12pt; font-family: 仿宋; color: black;\" data-mce-style=\"font-size: 12pt; font-family: 仿宋; color: black;\">该检测是为了确诊某种疾病或评估预测某种疾病的风险，阳性检测结果仍需其他方法辅助验证。</span></li><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-size: 12pt; font-family: 仿宋; color: black;\" data-mce-style=\"font-size: 12pt; font-family: 仿宋; color: black;\">该检测结果仅为进一步的临床检测、医疗保健和疾病预防提供参考。如果检测结果为阳性，请遵医嘱；如果某个特定位点的检测结果是阴性，并不完全排除患这种疾病的可能。因为一个疾病的发生可能受多个基因的影响，未检测到的其他基因也可能与该病的发生有关。目前国际上对于很多复杂疾病的研究也尚未明了或存在争议，本检测结果也将受此因素的限制而只能对明确的病因给予诊断及解释。</span></li><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-size: 12pt; font-family: 仿宋; color: black;\" data-mce-style=\"font-size: 12pt; font-family: 仿宋; color: black;\">虽然该分析通常能提供准确的信息，但是可能存在极少数包括（但不局限于）样本误认、样本污染以及有关亲缘关系的不准确信息等因素的影响而可能导致误诊的风险。</span></li><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span lang=\"EN-US\" style=\"mso-bidi-font-size: 10.5pt; line-height: 125%; mso-fareast-font-family: 'Times New Roman'; color: black;\" data-mce-style=\"mso-bidi-font-size: 10.5pt; line-height: 125%; mso-fareast-font-family: 'Times New Roman'; color: black;\"><span style=\"mso-list: Ignore;\" data-mce-style=\"mso-list: Ignore;\"><span style=\"font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; line-height: normal;\" data-mce-style=\"font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; line-height: normal;\"><span style=\"color: black;\" data-mce-style=\"color: black;\">鉴于当前医学检测技术水平的限制和个体差异等不同原因，即使在检测人员已经履行了工作职责和严格按照技术规范流程进行操作的前提下，仍有可能出现假阳性或假阴性的结果。</span></span></span></span>&nbsp;</span></li><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"color: black;\" data-mce-style=\"color: black;\">即使严格按照技术规范流程进行操作，在极少数情况下也可能因样本中</span><span lang=\"EN-US\" style=\"color: black;\" data-mce-style=\"color: black;\">DNA </span><span style=\"color: black;\" data-mce-style=\"color: black;\">含量过低等原因导致无法进行检测.</span></span></li></ol><p class=\"MsoNormal\" style=\"text-indent: 21.1pt; line-height: 125%;\" data-mce-style=\"text-indent: 21.1pt; line-height: 125%;\"><span style=\"font-family: 仿宋; font-size: 14pt;\" data-mce-style=\"font-family: 仿宋; font-size: 14pt;\"><strong style=\"mso-bidi-font-weight: normal;\" data-mce-style=\"mso-bidi-font-weight: normal;\"><span style=\"line-height: 125%; color: black;\" data-mce-style=\"line-height: 125%; color: black;\">知情同意：<br></span></strong></span></p><ol><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-size: 12pt; font-family: 仿宋; color: black;\" data-mce-style=\"font-size: 12pt; font-family: 仿宋; color: black;\">我已充分了解该检测的性质、目的、必要性和风险；</span></li><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-size: 12pt; font-family: 仿宋; color: black;\" data-mce-style=\"font-size: 12pt; font-family: 仿宋; color: black;\">我已了解该检测项目的适用人群；</span></li><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-size: 12pt; font-family: 仿宋; color: black;\" data-mce-style=\"font-size: 12pt; font-family: 仿宋; color: black;\">我并未得到该检测结果百分之百准确的许诺，对于由结果带来的心理及生理负担我不要求检测机构承担；</span></li><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-size: 12pt; font-family: 仿宋; color: black;\" data-mce-style=\"font-size: 12pt; font-family: 仿宋; color: black;\">我知晓该检测结果仅作参考，不能作为最终临床诊断依据；</span></li><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-size: 12pt; font-family: 仿宋; color: black;\" data-mce-style=\"font-size: 12pt; font-family: 仿宋; color: black;\">我知晓该检测结果仅作参考，不能作为最终临床诊断依据；</span></li><li style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><span style=\"font-family: 仿宋; color: black; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; color: black; font-size: 12pt;\"><span style=\"color: black;\" data-mce-style=\"color: black;\">我同意在不提供样本来源信息的前提下，使用我（或我的孩子）相关临床数据进行科研、发表论文或质量控制。<br></span></span><span lang=\"EN-US\" style=\"font-family: 'CourierNewPSMT',serif; mso-fareast-font-family: CourierNewPSMT; mso-bidi-font-family: CourierNewPSMT;\" data-mce-style=\"font-family: 'CourierNewPSMT',serif; mso-fareast-font-family: CourierNewPSMT; mso-bidi-font-family: CourierNewPSMT;\"><span style=\"mso-list: Ignore;\" data-mce-style=\"mso-list: Ignore;\"><span style=\"font: 7.0pt 'Times New Roman';\" data-mce-style=\"font: 7.0pt 'Times New Roman';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span>&nbsp; &nbsp; &nbsp;</li></ol><p class=\"MsoNormal\" style=\"margin-bottom: 12.0pt; mso-para-margin-bottom: 1.0gd; text-indent: 21.1pt;\" data-mce-style=\"margin-bottom: 12.0pt; mso-para-margin-bottom: 1.0gd; text-indent: 21.1pt;\"><span style=\"font-family: 仿宋; font-size: 12pt;\" data-mce-style=\"font-family: 仿宋; font-size: 12pt;\"><strong style=\"mso-bidi-font-weight: normal;\" data-mce-style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black;\" data-mce-style=\"color: black;\">本人对以上各条款均已经了解清楚，要求检查、同意随访，愿意承担因该检查带来的相应风险，提供资料属实。</span></strong></span></p>&nbsp;<data-element data-type=\"data-element\" data-compose_code=\"p$static@qianmin--g$QM.HZ.GROUP.01--e$QM.HZ.01\" style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "                            <data-name>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span data-datatype=\"core_data\" style=\"color:black;\">孕妇签名\uFEFF</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-name>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            :\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            <data-value>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span style=\"position:relative;display:inline-flex;text-decoration: underline;color:black;\">&nbsp;&nbsp;&nbsp;<div style=\"margin: 0px;padding:0px; position: absolute;bottom:-5px;left:px;height:30px;background-image:url(假数据);\n" +
                "                                background-repeat:no-repeat;background-size:100% 100%;z-index:-1\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>\n" +
                "                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" +
                "                                &nbsp;&nbsp;&nbsp;</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-value>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            \n" +
                "                        </data-element>&nbsp; &nbsp;<data-element data-type=\"data-element\" data-compose_code=\"p$static@qianmin--g$QM.HZ.GROUP.01--e$DE02.01.030.00\" style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "                            <data-name>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span data-datatype=\"core_data\" style=\"color:black;\">身份证号码</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-name>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            :\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            <data-value>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span style=\"position:relative;display:inline-flex;text-decoration: underline;color:black;\">&nbsp;&nbsp;&nbsp;330723120050314****&nbsp;&nbsp;&nbsp;</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-value>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            \n" +
                "                        </data-element>&nbsp; &nbsp;<data-element data-type=\"data-element\" data-compose_code=\"p$static@qianmin--g$QM.HZ.GROUP.01--e$DE06.00.156.00\" style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "                            <data-name>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span data-datatype=\"core_data\" style=\"color:black;\">日期</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-name>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            :\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            <data-value>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span style=\"position:relative;display:inline-flex;text-decoration: underline;color:black;\">&nbsp;&nbsp;&nbsp;2022-12-23&nbsp;&nbsp;&nbsp;</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-value>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            \n" +
                "                        </data-element>&nbsp;&nbsp;<br>&nbsp;<data-element data-type=\"data-element\" data-compose_code=\"p$static@qianmin--g$QM..GROUP.01--e$QM.JS.01\" style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "                            <data-name>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span data-datatype=\"core_data\" style=\"color:black;\">配偶签名</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-name>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            :\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            <data-value>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span style=\"position:relative;display:inline-flex;text-decoration: underline;color:black;\">&nbsp;&nbsp;&nbsp;<div style=\"margin: 0px;padding:0px; position: absolute;bottom:-5px;left:px;height:30px;background-image:url(假数据);\n" +
                "                                background-repeat:no-repeat;background-size:100% 100%;z-index:-1\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>\n" +
                "                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" +
                "                                &nbsp;&nbsp;&nbsp;</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-value>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            \n" +
                "                        </data-element>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<br>&nbsp;<data-element data-type=\"data-element\" data-compose_code=\"p$static@qianmin--g$QM.YS.GROUP.01--e$QM.YS.01\" style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "                            <data-name>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span data-datatype=\"core_data\" style=\"color:black;\">医生签名</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-name>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            :\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            <data-value>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span style=\"position:relative;display:inline-flex;text-decoration: underline;color:black;\">&nbsp;&nbsp;&nbsp;<div style=\"margin: 0px;padding:0px; position: absolute;bottom:-5px;left:px;height:30px;background-image:url(假数据);\n" +
                "                                background-repeat:no-repeat;background-size:100% 100%;z-index:-1\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>\n" +
                "                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" +
                "                                &nbsp;&nbsp;&nbsp;</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-value>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            \n" +
                "                        </data-element>&nbsp; &nbsp;<data-element data-type=\"data-element\" data-compose_code=\"p$static@qianmin--g$QM.YS.GROUP.01--e$DE06.00.156.00\" style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "                            <data-name>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span data-datatype=\"core_data\" style=\"color:black;\">日期</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-name>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            :\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            <data-value>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                            <text-disable contenteditable=\"false\" style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                            <span style=\"position:relative;display:inline-flex;text-decoration: underline;color:black;\">&nbsp;&nbsp;&nbsp;2022-12-23&nbsp;&nbsp;&nbsp;</span>\n" +
                "                            </text-disable>\n" +
                "                            <space-word data-length=\"0\" style=\"display: inline; margin: 0px;\"></space-word>\n" +
                "                        </data-value>\n" +
                "                            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                            \n" +
                "                        </data-element>&nbsp;<br><br><br><br data-mce-bogus=\"1\"></div>";
    }

    private static String getStr3() {
        return "<div>&nbsp;<data-element data-type=\"data-element\" data-compose_code=\"p$static@qianmin--g$QM.HZ.GROUP.01--e$QM.HZ.01\"\n" +
                "        style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "        <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\">\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                {{ <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </p>\n" +
                "\n" +
                "            <data-name>\n" +
                "                <color-rect data-bordercolor=\"rgb(228, 120, 44)\"\n" +
                "                    style=\"border: 1px solid rgb(228, 120, 44); color: rgb(228, 120, 44); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span data-datatype=\"core_data\" style=\"color:rgb(228, 120, 44);\">#QM.HZ.01.name#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-name>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            :\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            <data-value>\n" +
                "                <color-rect data-bordercolor=\"rgb(0, 143, 145)\"\n" +
                "                    style=\"border: 1px solid rgb(0, 143, 145); color: rgb(0, 143, 145); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span style=\"color:rgb(0, 143, 145);\">#QM.HZ.01.value#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-value>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "\n" +
                "\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>}}\n" +
                "            </p>\n" +
                "        </merge-tag>\n" +
                "    </data-element>&nbsp; &nbsp;<data-element data-type=\"data-element\"\n" +
                "        data-compose_code=\"p$static@qianmin--g$QM.HZ.GROUP.01--e$DE02.01.030.00\"\n" +
                "        style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "        <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\">\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                {{ <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </p>\n" +
                "\n" +
                "            <data-name>\n" +
                "                <color-rect data-bordercolor=\"rgb(228, 120, 44)\"\n" +
                "                    style=\"border: 1px solid rgb(228, 120, 44); color: rgb(228, 120, 44); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span data-datatype=\"core_data\" style=\"color:rgb(228, 120, 44);\">#DE02.01.030.00.name#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-name>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            :\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            <data-value>\n" +
                "                <color-rect data-bordercolor=\"rgb(0, 143, 145)\"\n" +
                "                    style=\"border: 1px solid rgb(0, 143, 145); color: rgb(0, 143, 145); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span style=\"color:rgb(0, 143, 145);\">#DE02.01.030.00.value#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-value>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "\n" +
                "\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>}}\n" +
                "            </p>\n" +
                "        </merge-tag>\n" +
                "    </data-element>&nbsp; &nbsp;<data-element data-type=\"data-element\"\n" +
                "        data-compose_code=\"p$static@qianmin--g$QM.HZ.GROUP.01--e$DE06.00.156.00\"\n" +
                "        style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "        <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\">\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                {{ <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </p>\n" +
                "\n" +
                "            <data-name>\n" +
                "                <color-rect data-bordercolor=\"rgb(228, 120, 44)\"\n" +
                "                    style=\"border: 1px solid rgb(228, 120, 44); color: rgb(228, 120, 44); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span data-datatype=\"core_data\" style=\"color:rgb(228, 120, 44);\">#DE06.00.156.00.name#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-name>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            :\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            <data-value>\n" +
                "                <color-rect data-bordercolor=\"rgb(0, 143, 145)\"\n" +
                "                    style=\"border: 1px solid rgb(0, 143, 145); color: rgb(0, 143, 145); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span style=\"color:rgb(0, 143, 145);\">#DE06.00.156.00.value#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-value>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "\n" +
                "\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>}}\n" +
                "            </p>\n" +
                "        </merge-tag>\n" +
                "    </data-element>&nbsp;&nbsp;<br>&nbsp;<data-element data-type=\"data-element\"\n" +
                "        data-compose_code=\"p$static@qianmin--g$QM..GROUP.01--e$QM.JS.01\"\n" +
                "        style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "        <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\">\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                {{ <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </p>\n" +
                "\n" +
                "            <data-name>\n" +
                "                <color-rect data-bordercolor=\"rgb(228, 120, 44)\"\n" +
                "                    style=\"border: 1px solid rgb(228, 120, 44); color: rgb(228, 120, 44); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span data-datatype=\"core_data\" style=\"color:rgb(228, 120, 44);\">#QM.JS.01.name#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-name>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            :\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            <data-value>\n" +
                "                <color-rect data-bordercolor=\"rgb(0, 143, 145)\"\n" +
                "                    style=\"border: 1px solid rgb(0, 143, 145); color: rgb(0, 143, 145); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span style=\"color:rgb(0, 143, 145);\">#QM.JS.01.value#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-value>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "\n" +
                "\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>}}\n" +
                "            </p>\n" +
                "        </merge-tag>\n" +
                "    </data-element>&nbsp;&nbsp;<br>&nbsp;<data-element data-type=\"data-element\"\n" +
                "        data-compose_code=\"p$static@qianmin--g$QM.YS.GROUP.01--e$QM.YS.01\"\n" +
                "        style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "        <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\">\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                {{ <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </p>\n" +
                "\n" +
                "            <data-name>\n" +
                "                <color-rect data-bordercolor=\"rgb(228, 120, 44)\"\n" +
                "                    style=\"border: 1px solid rgb(228, 120, 44); color: rgb(228, 120, 44); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span data-datatype=\"core_data\" style=\"color:rgb(228, 120, 44);\">#QM.YS.01.name#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-name>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            :\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            <data-value>\n" +
                "                <color-rect data-bordercolor=\"rgb(0, 143, 145)\"\n" +
                "                    style=\"border: 1px solid rgb(0, 143, 145); color: rgb(0, 143, 145); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span style=\"color:rgb(0, 143, 145);\">#QM.YS.01.value#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-value>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "\n" +
                "\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>}}\n" +
                "            </p>\n" +
                "        </merge-tag>\n" +
                "    </data-element>&nbsp; &nbsp;<data-element data-type=\"data-element\"\n" +
                "        data-compose_code=\"p$static@qianmin--g$QM.YS.GROUP.01--e$DE06.00.156.00\"\n" +
                "        style=\"display: inline-flex; margin: 0px; align-items: center; justify-content: center; padding: 0px; box-sizing: border-box;\">\n" +
                "        <merge-tag style=\"display: inline-flex; align-items: center; overflow-wrap: break-word; height: 100%;\">\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                {{ <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            </p>\n" +
                "\n" +
                "            <data-name>\n" +
                "                <color-rect data-bordercolor=\"rgb(228, 120, 44)\"\n" +
                "                    style=\"border: 1px solid rgb(228, 120, 44); color: rgb(228, 120, 44); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span data-datatype=\"core_data\" style=\"color:rgb(228, 120, 44);\">#DE06.00.156.00.name#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-name>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            :\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "            <data-value>\n" +
                "                <color-rect data-bordercolor=\"rgb(0, 143, 145)\"\n" +
                "                    style=\"border: 1px solid rgb(0, 143, 145); color: rgb(0, 143, 145); display: inline-flex; align-items: center;\">\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                    <text-disable contenteditable=\"false\"\n" +
                "                        style=\"display: inline; font-size: 14px; overflow-wrap: break-word; user-select: none;\">\n" +
                "                        <span style=\"color:rgb(0, 143, 145);\">#DE06.00.156.00.value#</span>\n" +
                "                    </text-disable>\n" +
                "                    <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "                </color-rect>\n" +
                "            </data-value>\n" +
                "            <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>\n" +
                "\n" +
                "\n" +
                "            <p data-type=\"merge-tag-arrow\" style=\"margin:0px;font-size: 20px;display: inline;color:#4eb7a2;\">\n" +
                "                <space-word data-length=\"1\" style=\"display: inline; margin: 0px;\">&nbsp;</space-word>}}\n" +
                "            </p>\n" +
                "        </merge-tag>\n" +
                "    </data-element>&nbsp;&nbsp;<br data-mce-bogus=\"1\"></div>";
    }

//    private static ElementAndTitle selectFirst(String xmlStr, String config, String type) {
//        Document document = Jsoup.parse(xmlStr);
//        if ("高级设计状态".equals(type)) {
//            Element element = document.selectFirst("data-element > merge-tag > data-name > color-rect [data-datatype]");
//            if (element == null) {
//                return null;
//            }
//            String title = element.ownText();
//            // 数据元
//            Element dataElement = element.parent().parent().parent().parent().parent();
//            return new ElementAndTitle(dataElement, title);
//        }
//        if ("预览状态".equals(type)) {
//            Element element = document.selectFirst("data-element > data-name [data-datatype]");
//            if (element == null) {
//                return null;
//            }
//            String title = element.ownText();
//            // 数据元
//            Element dataElement = element.parent().parent().parent();
//            return new ElementAndTitle(dataElement, title);
//        }
//        if ("设计状态".equals(type)) {
//            return;
//        }
//
//    }
}

package chagine.mdt.utils;

import chagine.core.ErrorCode;
import chagine.core.exception.SeeingException;
import chagine.core.util.PyOrWbUtil;
import org.springframework.util.StringUtils;

import java.util.regex.Pattern;

/**
 * @program: search
 * @description: string操作类
 * @author: WangBin
 * @create: 2018-08-01 11:13
 **/
public class StringUtil {

    /**
     * 空格字符串
     */
    public static final String REGEX_ID_CARD = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)";
    public static final String REGEX_NUM_CARD = "^[0-9]*$";


    public static boolean isEmpty(String value) {
        return isEmpty((CharSequence) value);
    }

    public static boolean isEmpty(CharSequence value) {
        return value == null || value.length() == 0;
    }

    /**
     * 检查对象为空或者超过长度限制
     *
     * @param obj
     * @param length
     * @param title
     * @throws SeeingException
     */
    public static void isEmptyOrTooLong(Object obj, Integer length, String title) throws SeeingException {
        if (obj == null || obj.toString().replace(" ", "").length() == 0) {
            throw new SeeingException(ErrorCode.ILLEGALARGUMENT_EXCEPTION, title + "不能为空");
        }
        if (length != null && obj.toString().length() > length) {
            throw new SeeingException(ErrorCode.ILLEGALARGUMENT_EXCEPTION, "[" + title + "]-不能超过[" + length + "]个字符");
        }
    }

    /**
     * String转py
     *
     * @param
     * @return
     */
    public static String string2Py(String data) {
        String afterChangeValue = PyOrWbUtil.cn2FirstSpell(data);
        if (StringUtils.hasText(afterChangeValue)) {
            return afterChangeValue;
        } else {
            return data;
        }
    }

    /**
     * String转wb
     *
     * @param
     * @return
     */
    public static String string2Wb(String data) {
        String afterChangeValue = PyOrWbUtil.cn2WBCode(data);
        if (StringUtils.hasText(afterChangeValue)) {
            return afterChangeValue;
        } else {
            return data;
        }
    }



    /**
     * 是否有值
     *
     * @param cha
     * @return
     */
    public static Boolean hasText(CharSequence cha) {
        return StringUtils.hasText(cha);
    }


    /**
     * 校验身份证
     *
     * @param idCard
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isIDCard(String idCard) {
        return Pattern.matches(REGEX_ID_CARD,idCard);
    }


    /**
     * 校验纯数字
     *
     * @param number
     * @return 校验通过返回true，否则返回false
     */
    public static boolean isNumber(String number) {
        return Pattern.matches(REGEX_NUM_CARD,number);
    }

}

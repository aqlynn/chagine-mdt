package chagine.mdt.schedule;

import chagine.mdt.config.properties.ParamsScheduleProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

@Slf4j
@Component
public class TestSchedule {



    @Resource
    private ParamsScheduleProperties paramsScheduleProperties;




    @Scheduled(cron = "0 25 0 * * ?")
    public void test() {
        log.info("test");
    }
}

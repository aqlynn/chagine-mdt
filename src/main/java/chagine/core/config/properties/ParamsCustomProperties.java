package chagine.core.config.properties;

import chagine.mdt.config.properties.*;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = ParamsCustomProperties.PREFIX)
public class ParamsCustomProperties {

    public static final String PREFIX = "custom";

    /**
     * 雪花ID配置
     */
    @NestedConfigurationProperty
    private ParamsSnowFlakeProperties snowFlake;

    /**
     * 适配器URL
     */
    @NestedConfigurationProperty
    private ParamsAdapterProperties adapter;

    /**
     * 消息中心配置
     */
    @NestedConfigurationProperty
    private ParamsMsgPushProperties msgPush;

    /**
     * 文件处理配置
     */
    @NestedConfigurationProperty
    private ParamsFileProperties file;



}

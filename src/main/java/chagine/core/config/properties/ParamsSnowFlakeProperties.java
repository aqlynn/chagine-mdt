package chagine.core.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(ParamsSnowFlakeProperties.PREFIX)
public class ParamsSnowFlakeProperties {

    public static final String PREFIX = "custom.snow-flake";

    /**
     * 机器码
     */
    private Integer machineId = 0;

}

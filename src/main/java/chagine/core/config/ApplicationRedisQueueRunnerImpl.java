package chagine.core.config;

import chagine.core.DefaultConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 项目完全启动后执行代码
 */
@Slf4j
@Order
@Component
public class ApplicationRedisQueueRunnerImpl implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {
        initSystemRun();
    }

    private void initSystemRun() {
        DefaultConstants.SYSTEM_IS_RUN = true;
    }
}
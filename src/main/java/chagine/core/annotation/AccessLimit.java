package chagine.core.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 请求限制
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AccessLimit {

    /**
     * 限制类型
     */
    LimitType type() default LimitType.NO_SETTING;

    /**
     * 参数重复判断
     */
    ArgsRepeat argsRepeat() default ArgsRepeat.CLOSE;

    /**
     * 单位时间内最大请求次数
     */
    long maxRequestCount() default 1;

    /**
     * 最大请求次数，时间间隔
     * 例如 第1次，请求与第2次请求可请求的次数
     */
    long timeout() default 1;

    /**
     * 最大请求次数，时间单位
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * 限流KEY
     */
    String key() default "";

    /**
     * 限制类型
     */
    enum LimitType {
        /**
         * 未设置，按照单IP限制进行
         */
        NO_SETTING,
        /**
         * 用户
         */
        USER,
        /**
         * 租户限制
         */
        TENANT,
        /**
         * 根据设备ID限制
         */
        DEVICE,
        /**
         * 自定义限制key
         */
        KEY
        ;
    }

    /**
     * 入参限制
     */
    enum ArgsRepeat{
        /**
         * 开启
         */
        OPEN,
        /**
         * 关闭
         */
        CLOSE;
    }

}
package chagine.core.annotation;

import java.lang.annotation.*;

/**
 * 计算接口运行时间
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CalculateTime {

    String value() default "";

    long timeout() default 1000;
}
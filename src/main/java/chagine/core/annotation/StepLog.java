package chagine.core.annotation;

import java.lang.annotation.*;

/**
 * 开启step_log
 *
 * @author zevin
 * @date 2023/3/15 17:00
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface StepLog {
}
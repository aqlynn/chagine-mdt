package chagine.core.util;

/**
 * Created by 王彬安 on 2019/8/24.
 */
public class ServiceName {
    public final static String AUTH_SERVICE = "auth";
    public final static String BASIC_SERVICE = "basic";
    public final static String RESOURCE_SERVICE = "resource";
    public final static String CHARGE_SERVICE = "charge";
    public final static String EMALL_SERVICE = "emall";
    public final static String PHARMACY_SERVICE = "pharmacy";
    public final static String QUEST_CENTER_SERVICE = "questcenter";
    //public final static String ERP_SERVICE = "erp";
    public final static String MSG_PUSH_SERVICE = "msgpush";
}

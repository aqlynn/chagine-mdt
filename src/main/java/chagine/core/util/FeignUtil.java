package chagine.core.util;

import chagine.core.ErrorCode;
import chagine.core.exception.SeeingRuntimeException;
import chagine.core.restful.util.TResponse;
import feign.FeignException;

public class FeignUtil {

    public static <T> T getResponseData(TResponse<T> response) {
        try {
            Assert.error(!response.isSuccess(), ErrorCode.BUSINESS_EXCEPTION, response.getMessage());
            return response.getData();
        } catch (FeignException e) {
            throw new SeeingRuntimeException(ErrorCode.HTTP_ERROR_EXCEPTION, "feign异常:" + e.getMessage());
        }
    }

}

package chagine.core.util;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ListUtil {

    /**
     * 判断去重的Key
     *
     * @param keyExtractor 需要去重的属性
     */
    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    /**
     * 根据字符串，用逗号分割，生成集合
     */
    public static List<Long> listFromString(String str) {
        if (StringUtils.isBlank(str)) {
            return new ArrayList<>();
        }
        String[] split = str.split(",");
        List<Long> outList = new ArrayList<>();
        for (String s : split) {
            if (StringUtils.isNotBlank(s)) {
                outList.add(Long.valueOf(s));
            }
        }
        return outList;
    }

    public static List<String> listStrFromString(String str) {
        if (StringUtils.isBlank(str)) {
            return new ArrayList<>();
        }
        String[] split = str.split(",");
        List<String> outList = new ArrayList<>();
        for (String s : split) {
            if (StringUtils.isNotBlank(s)) {
                outList.add(s.trim());
            }
        }
        return outList;
    }

    /**
     * 根据集合获取科室负责人列表
     */
    public static String stringFromList(List<Long> ids) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ids.size(); i++) {
            Long id = ids.get(i);
            if (id != null) {
                sb.append(id);
            }
            if (i != ids.size() - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    /**
     * 根据集合获取科室负责人列表
     */
    public static String stringFromListStr(List<String> ids) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ids.size(); i++) {
            String id = ids.get(i);
            if (id != null) {
                sb.append(id);
            }
            if (i != ids.size() - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    /**
     * 更新集合
     *
     * @param oldIdsStr 旧字符串
     * @param newIds    新数组
     */
    public static String updateIdsStr(String oldIdsStr, List<Long> newIds) {
        List<Long> oldIds = listFromString(oldIdsStr);
        List<Long> ids = new ArrayList<>(oldIds);
        ids.addAll(newIds);
        ids.removeAll(Collections.singleton(null));
        ids = ids.stream().distinct().collect(Collectors.toList());
        return stringFromList(ids);
    }

    /**
     * 更新集合
     *
     * @param oldIdsStr 旧字符串
     * @param newIds    新数组
     */
    public static String updateIdsStrList(String oldIdsStr, List<String> newIds) {
        List<String> oldIds = listStrFromString(oldIdsStr);
        List<String> ids = new ArrayList<>(oldIds);
        ids.addAll(newIds);
        ids.removeAll(Collections.singleton(null));
        ids = ids.stream().distinct().collect(Collectors.toList());
        return stringFromListStr(ids);
    }

    /**
     * 更新集合
     *
     * @param oldIdsStr 旧字符串
     * @param newId     新ID
     */
    public static String updateIdsStr(String oldIdsStr, Long newId) {
        List<Long> ids = new ArrayList<>();
        ids.add(newId);
        return updateIdsStr(oldIdsStr, ids);
    }

    /**
     * 更新集合
     *
     * @param oldIdsStr 旧字符串
     * @param newId     新ID
     */
    public static String updateIdsStrList(String oldIdsStr, String newId) {
        List<String> ids = new ArrayList<>();
        ids.add(newId);
        return updateIdsStrList(oldIdsStr, ids);
    }

    /**
     * 删除集合
     *
     * @param oldIdsStr 旧字符串
     * @param newId     待删除ID
     */
    public static String removeIdsStr(String oldIdsStr, Long newId) {
        List<Long> ids = new ArrayList<>();
        ids.add(newId);
        return removeIdsStr(oldIdsStr, ids);
    }

    /**
     * 删除集合
     *
     * @param oldIdsStr 旧字符串
     * @param newId     待删除ID
     */
    public static String removeIdsStrList(String oldIdsStr, String newId) {
        List<String> ids = new ArrayList<>();
        ids.add(newId);
        return removeIdsStrList(oldIdsStr, ids);
    }

    /**
     * 删除集合
     *
     * @param oldIdsStr 旧字符串
     * @param newIds    待删除集合
     */
    public static String removeIdsStr(String oldIdsStr, List<Long> newIds) {
        List<Long> oldIds = listFromString(oldIdsStr);
        newIds.removeAll(Collections.singleton(null));
        oldIds.removeIf(newIds::contains);
        oldIds = oldIds.stream().distinct().collect(Collectors.toList());
        return stringFromList(oldIds);
    }

    /**
     * 删除集合
     *
     * @param oldIdsStr 旧字符串
     * @param newIds    待删除集合
     */
    public static String removeIdsStrList(String oldIdsStr, List<String> newIds) {
        List<String> oldIds = listStrFromString(oldIdsStr);
        newIds.removeAll(Collections.singleton(null));
        oldIds.removeIf(newIds::contains);
        oldIds = oldIds.stream().distinct().collect(Collectors.toList());
        return stringFromListStr(oldIds);
    }


}

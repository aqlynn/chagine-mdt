package chagine.core.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * create by wba on  2019/12/28
 * qq:43585061
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Page<T> {
    public final static int DEFAULT_PAGE_SIZE = 10;

    @JsonProperty(value = "page_num")
    private Integer pageNum;

    @JsonProperty(value = "page_size")
    private Integer pageSize = DEFAULT_PAGE_SIZE;

    //总记录条数
    @JsonProperty(value = "total")
    private Integer total;

    //总页数
    @JsonProperty(value = "pages")
    private Integer pages;

    @JsonProperty(value = "list")
    private List<T> list;

    /**
     * 设置为空的页集
     */
    public void setBlankPage() {
        this.total = 0;
        this.pageNum = 1;
        this.pages = 0;
        list = new ArrayList<>();
    }

    /**
     * 设置总记录数和页号，计算出总页数，并调整页号
     */
    public void setPageTotalNum(int total, int pageNum) {
        if (total <= 0) {
            this.total = 0;
            this.pageNum = 0;
            this.pages = 0;
        } else {
            this.total = total;
            int pages0 = total / pageSize;
//余数
            int mod = total % pageSize;
            if (mod > 0) {
                this.pages = pages0 + 1;
            } else {
                this.pages = pages0;
            }
            if (pageNum < 0) {
                this.pageNum = 1;
            } else {
                this.pageNum = pageNum;
            }

        }
    }

    /**
     *
     */
    public int pageBegin() {
        return (pageNum - 1) * pageSize;
    }


}

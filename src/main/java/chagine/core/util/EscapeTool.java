package chagine.core.util;

import org.springframework.util.StringUtils;

/**
 * create by wba on  2021/3/6
 * qq:43585061
 * 脱敏
 */
public class EscapeTool {

    //public final static String REG_PHONE_ESCAPE = "1\\d{10}";//定义手机号规则  1开头，后面数字出现10次

    public static String mobileEscape(String mobile) {
        if (StringUtils.isEmpty(mobile) || (mobile.length() != 11)) {
            return mobile;
        }
        return mobile.replaceAll("(\\d{3})\\d{6}(\\d{2})", "$1****$2");
    }

    //身份证前三后四脱敏
    public static String idEscape(String id) {
        if (StringUtils.isEmpty(id) || (id.length() < 8)) {
            return id;
        }
        return id.replaceAll("(?<=\\w{3})\\w(?=\\w{4})", "*");
    }

    //姓名脱敏
    public static String nameEscape(String name) {
        if (StringUtils.isEmpty(name) || (name.length() <= 1)) {
            return name;
        }
        if (name.length() == 1) {
            return "*";
        } else if (name.length() == 2) {
            return "*" + name.substring(1);
            //return name.replaceAll("^(.).+$", "$1*");
        } else if (name.length() == 3) {
            return name.replaceAll("^(.).+(.)$", "**$2");
            //return name.replaceAll("^(.).+(.)$", "$1*$2");
        } else if (name.length() == 4) {
            return name.replaceAll("^(.).+(.)$", "***$2");
        } else {
            return name.replaceAll("^(.{2}).+(.)", "****$2");
        }

    }


}

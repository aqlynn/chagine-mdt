package chagine.core.util;

import org.apache.commons.lang3.StringUtils;

public class NumberUtil {

    public static Integer stringToInteger(String input) {
        if (StringUtils.isBlank(input)) {
            return null;
        }
        return Integer.valueOf(input.trim());
    }

}

package chagine.core.id;

import java.util.List;

/**
 * Created by 蔡邦超 on 2017/10/9.
 * com.guantong.seeing.id.ID
 * 全平台全局唯一，toString 后长度较长
 */
public interface IDService {

    /**
     * 获得一个新的ID
     *
     * @param idType id类型
     * @return 新的ID
     */
    long getNextId(int idType);

    /**
     * 批量获取ID
     *
     * @param idType id类型
     * @param count  批量获取的数量
     * @return 新的ID
     */
    List<Long> getBatchIds(int idType, int count);

}

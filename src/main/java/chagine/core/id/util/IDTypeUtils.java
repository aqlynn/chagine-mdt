package chagine.core.id.util;

/**
 * com.seeing.id.util.IDTypeUtils
 * Created by 王彬安（wba）on 2017/10/14.
 */
public class IDTypeUtils {

    /**
     * 每一部分占用的位数
     */
    public final static long SEQUENCE_BIT = 12; //序列号占用的位数
    public final static long MACHINE_BIT = 5;  //机器标识占用的位数
    public final static long DATACENTER_BIT = 5;//数据中心占用的位数

    /**
     * 每一部分的最大值
     */
    public final static long MAX_DATACENTER_NUM = ~(-1L << DATACENTER_BIT);
    public final static long MAX_MACHINE_NUM = ~(-1L << MACHINE_BIT);
    public final static long MAX_SEQUENCE = ~(-1L << SEQUENCE_BIT);

    /**
     * 解析ID类型
     *
     * @param id id值
     * @return
     */
    public static long parseIdType(long id) {
        return (id >> SEQUENCE_BIT >> MACHINE_BIT) & MAX_DATACENTER_NUM;
    }

    public static long parseMachineId(long id) {
        return (id >> SEQUENCE_BIT) & MAX_MACHINE_NUM;
    }
}

package chagine.core.id.util;

import chagine.core.id.IDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * com.seeing.id.util.IDServiceUtils
 * 客户端级的缓存工作类
 * Created by 王彬安（wba）on 2017/12/14.
 */
@Component
public class IDServiceUtils {

    private IDService idService;

    @Autowired
    public IDServiceUtils(IDService idService) {
        this.idService = idService;
    }

    /**
     * 获得一个新的ID
     *
     * @param idType id类型
     * @return 新的ID
     */
    public long getNextId(int idType) {

        return idService.getNextId(idType);
    }

}

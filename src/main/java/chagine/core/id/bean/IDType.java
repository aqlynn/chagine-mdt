package chagine.core.id.bean;

/**
 * com.guantong.seeing.ider.id.IDType
 * Created by 王彬安（wba）on 2017/8/8.
 */
public class IDType {

    private IDType() {
    }

    /**
     * 普通的唯一ID
     */
    public static final int UniqueID = 1;

    /**
     * 租户ID
     */
    public static final int TenantID = 2;

    /**
     * 医护用户ID
     */
    public static final int MedicalUserID = 3;

    /**
     * 患者用户ID
     */
    public static final int PatientUserID = 4;

    /**
     * 医生的员工ID
     */
    public static final int MedicalEmployeeID = 5;

    /**
     * 文件资源记录ID
     */
    public static final int FileResourceID = 8;

    /**
     * 医护账号ID
     */
    public static final int MedicalAccountID = 9;

    /**
     * 患者账号ID
     */
    public static final int PatientAccountID = 10;

    /*
     * 开发账号ID
     * */
    public static final int DevelopID = 11;

    /*
     * 租户管理员ID
     * */
    public static final int TenantAdminID = 12;

    /*
     * 科室部门ID
     * */
    public static final int DepartementID = 13;

    /**
     * 租户订单ID
     */
    public static final int TradeID = 14;

    /**
     * 定时任务的请求操作ID
     */
    public static final int ScheduleRequestID = 30;

    /**
     * 请求操作ID
     */
    public static final int RequestID = 31;

}

package chagine.core.id.ider;


import chagine.core.id.util.IDTypeUtils;

import java.security.SecureRandom;

/**
 * com.guantong.seeing.ider.id.SnowFlake
 * Created by 王彬安（wba）on 2017/8/8.
 */
public class SnowFlake {

    /**
     * 起始的时间戳 2017-08-08 8:8:8Z888
     */
    private final static long START_STAMP = 1502150888888L;

    /**
     * 每一部分占用的位数
     */
    private final static long SEQUENCE_BIT = IDTypeUtils.SEQUENCE_BIT; //序列号占用的位数
    private final static long MACHINE_BIT = IDTypeUtils.MACHINE_BIT;  //机器标识占用的位数
    private final static long DATACENTER_BIT = IDTypeUtils.DATACENTER_BIT;//数据中心占用的位数

    /**
     * 每一部分的最大值
     */
    private final static long MAX_DATACENTER_NUM = IDTypeUtils.MAX_DATACENTER_NUM;
    private final static long MAX_MACHINE_NUM = IDTypeUtils.MAX_MACHINE_NUM;
    private final static long MAX_SEQUENCE = IDTypeUtils.MAX_SEQUENCE;

    /**
     * 每一部分向左的位移
     */
    private final static long MACHINE_LEFT = SEQUENCE_BIT;
    private final static long DATACENTER_LEFT = SEQUENCE_BIT + MACHINE_BIT;
    private final static long TIMESTAMP_LEFT = DATACENTER_LEFT + DATACENTER_BIT;

    private long datacenterId;  //数据中心
    private long machineId;    //机器标识
    private long sequence = new SecureRandom().nextLong() & (MAX_SEQUENCE >> 2); //序列号
    //    private long sequence = 0L; //序列号
    private long lastStamp = -1L;//上一次时间戳

    public SnowFlake(long dataCenterId, long machineId) {
        if (dataCenterId > MAX_DATACENTER_NUM || dataCenterId < 0) {
            throw new IllegalArgumentException("dataCenterId can't be greater than " + MAX_DATACENTER_NUM + " or less than 0");
        }
        if (machineId > MAX_MACHINE_NUM || machineId < 0) {
            throw new IllegalArgumentException("machineId can't be greater than " + MAX_MACHINE_NUM + " or less than 0");
        }
        this.datacenterId = dataCenterId;
        this.machineId = machineId;
    }

    public static long parseDataCenterId(long id) {
        return (id >> SEQUENCE_BIT >> MACHINE_BIT) & MAX_DATACENTER_NUM;
    }

    public static long parseMachineId(long id) {
        return (id >> SEQUENCE_BIT) & MAX_MACHINE_NUM;
    }

    /**
     * 产生下一个ID
     *
     * @return
     */
    public synchronized long nextId() {
        long currStamp = getNewstmp();
        if (currStamp < lastStamp) {
            throw new RuntimeException("Clock moved backwards.  Refusing to generate id");
        }

        if (currStamp == lastStamp) {
            //相同毫秒内，序列号自增
            sequence = (sequence + 1) & MAX_SEQUENCE;
            //同一毫秒的序列数已经达到最大
            if (sequence == 0L) {
                currStamp = getNextMill();
            }
        } else {
            // 如果和上次生成时间不同,重置sequence，就是下一毫秒开始，sequence计数重新从0开始累加,
            // 为了保证尾数随机性更大一些,最后一位设置一个随机数
            sequence = new SecureRandom().nextLong() & (MAX_SEQUENCE >> 2);
//            sequence = 0L;
        }

        lastStamp = currStamp;

        return (currStamp - START_STAMP) << TIMESTAMP_LEFT //时间戳部分
                | datacenterId << DATACENTER_LEFT      //数据中心部分
                | machineId << MACHINE_LEFT            //机器标识部分
                | sequence;                            //序列号部分
    }

    private long getNextMill() {
        long mill = getNewstmp();
        while (mill <= lastStamp) {
            mill = getNewstmp();
        }
        return mill;
    }

    private long getNewstmp() {
        return System.currentTimeMillis();
    }

    public int getDatacenterId() {
        return (int) datacenterId;
    }

    public int getMachineId() {
        return (int) machineId;
    }
}

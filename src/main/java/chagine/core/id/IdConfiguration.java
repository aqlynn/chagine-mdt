package chagine.core.id;

import chagine.core.config.properties.ParamsSnowFlakeProperties;
import chagine.core.id.ider.impl.IDImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * Created by 王彬安 on 2019/8/28.
 */
@Configuration
@Slf4j
public class IdConfiguration {

    @Resource
    private ParamsSnowFlakeProperties paramsSnowFlakeProperties;

    @Bean
    public IDService getIdService() {
        log.info("=============getIdService==============");
        return new IDImpl(paramsSnowFlakeProperties.getMachineId());
    }
}

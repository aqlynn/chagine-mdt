package chagine.core.id;

import chagine.core.id.bean.IDType;
import chagine.core.id.util.IDServiceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by 蔡邦超 on 2017/10/16.
 * About :
 */
@Component
public class Order {

    private final IDService idService;

    private final IDServiceUtils idServiceUtils;

    @Autowired
    public Order(IDService idService, IDServiceUtils idServiceUtils) {
        this.idService = idService;
        this.idServiceUtils = idServiceUtils;
    }

    /**
     * 订单ID
     */
    public long getTradeId() {
        return idServiceUtils.getNextId(IDType.TradeID);
    }

    /**
     * 租户ID
     */
    public long getTenantId() {
        return idServiceUtils.getNextId(IDType.TenantID);
    }

    /**
     * 医护用户ID
     */
    public long getMedicalUserId() {
        return idServiceUtils.getNextId(IDType.MedicalUserID);
    }

    /**
     * 请求操作ID
     */
    public long getRequestId() {
        return idServiceUtils.getNextId(IDType.RequestID);
    }

    /**
     * 全平台不重复ID
     */
    public long getUniqueId() {
        return idServiceUtils.getNextId(IDType.UniqueID);
    }

    /**
     * 文件资源记录ID
     */
    public long getFileResourceId() {
        return idServiceUtils.getNextId(IDType.FileResourceID);
    }

    /**
     * 医护账号ID
     */
    public long getMedicalAccountId() {
        return idServiceUtils.getNextId(IDType.MedicalAccountID);
    }

    /**
     * 患者用户ID
     */
    public long getPatientUserId() {
        return idServiceUtils.getNextId(IDType.PatientUserID);
    }

    /**
     * 患者账号ID
     */
    public long getPatientAccountId() {
        return idServiceUtils.getNextId(IDType.PatientAccountID);
    }

    /**
     * 开发账号ID
     */
    public long getDevelopId() {
        return idServiceUtils.getNextId(IDType.DevelopID);
    }

    /**
     * 租户管理员ID
     */
    public long getTenantAdminId() {
        return idServiceUtils.getNextId(IDType.TenantAdminID);
    }

    /**
     * 科室部门ID
     */
    public long getDepartementId() {
        return idServiceUtils.getNextId(IDType.DepartementID);
    }

    /**
     * 批量获取租户ID
     *
     * @param count 批量获取的数量
     */
    public List<Long> getTenantIdList(int count) {
        return idService.getBatchIds(IDType.TenantID, count);
    }

    /**
     * 批量获取医护用户ID
     *
     * @param count 批量获取的数量
     */
    public List<Long> getMedicalUserIdList(int count) {
        return idService.getBatchIds(IDType.MedicalUserID, count);
    }


    /**
     * 批量获取请求操作ID
     *
     * @param count 批量获取的数量
     */
    public List<Long> getRequestIdList(int count) {
        return idService.getBatchIds(IDType.RequestID, count);
    }

    /**
     * 批量获取全平台不重复ID
     *
     * @param count 批量获取的数量
     */
    public List<Long> getUniqueIdList(int count) {
        return idService.getBatchIds(IDType.UniqueID, count);
    }

    /**
     * 批量获取文件资源记录ID
     *
     * @param count 批量获取的数量
     */
    public List<Long> getFileResourceIdList(int count) {
        return idService.getBatchIds(IDType.FileResourceID, count);
    }


}
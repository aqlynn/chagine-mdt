package chagine.core.serialize.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Date;

/**
 * Date对象序列化为时间戳
 */
public class Date2TimestampSerializer extends JsonSerializer<Date> {

    @Override
    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException {
        if (date != null) {
            jsonGenerator.writeNumber(date.getTime() / 1000);
        }
    }

}

//package chagine.core.serialize.serialize;
//
//import com.fasterxml.jackson.core.JsonGenerator;
//import com.fasterxml.jackson.databind.JsonSerializer;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.SerializerProvider;
//
//import java.io.IOException;
//
///**
// * json序列化反序列化的时候将BigDecimal类型转成String类型
// * Created by lyd on 2017/7/27.
// */
//public class LongJsonSerializer extends JsonSerializer<Long> {
//    @Override
//    public void serialize(Long in, JsonGenerator generator, SerializerProvider provider)
//            throws IOException {
//        ObjectMapper om = new ObjectMapper();
//        String temp = in.toString();
//        om.writeValue(generator, temp);
//    }
//}

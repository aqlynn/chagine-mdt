//package chagine.core.serialize.serialize;
//
//import chagine.core.exception.SeeingException;
//import chagine.core.util.DateUtil;
//import com.fasterxml.jackson.core.JsonGenerator;
//import com.fasterxml.jackson.databind.JsonSerializer;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.SerializerProvider;
//import lombok.extern.slf4j.Slf4j;
//
//import java.io.IOException;
//
///**
// * @program: optometry
// * @description:
// * @author: redscarf
// * @create: 2019-01-11 20:05
// **/
//
//@Slf4j
//public class LongTimeSerializer extends JsonSerializer<Long> {
//
//    @Override
//    public void serialize(Long in, JsonGenerator generator, SerializerProvider provider) throws IOException {
//        ObjectMapper om = new ObjectMapper();
//        if (in != null) {
//            String temp = null;
//            try {
//                temp = DateUtil.getTimeStr(in, 1000);
//            } catch (SeeingException e) {
//                log.warn(e.getMessage());
//            }
//            om.writeValue(generator, temp);
//        } else {
//            om.writeValue(generator, "");
//        }
//
//    }
//}

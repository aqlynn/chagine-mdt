package chagine.core.serialize.serialize;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Date;

/**
 * Date对象序列化为时间戳
 */
public class Date2DateFormatterSerializer extends JsonSerializer<Date> {

    @Override
    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException {
        if (date != null) {
            jsonGenerator.writeString(DateUtil.format(date, chagine.core.util.DateUtil.DATE_FORMATTER));
        }
    }
}

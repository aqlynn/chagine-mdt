package chagine.core.serialize.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * json序列化反序列化的时候将BigDecimal类型转成String类型
 * Created by lyd on 2017/7/27.
 */
public class Long2StringSerializer extends JsonSerializer<Long> {
    @Override
    public void serialize(Long in, JsonGenerator jsonGenerator, SerializerProvider provider)
            throws IOException {
        if (in != null) {
            jsonGenerator.writeString(in.toString());
        }
    }
}

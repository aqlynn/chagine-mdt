package chagine.core.serialize.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by lyd on 2017/11/8.
 * 序列化的时候用于金额换算，从分换算成元
 * 数据库中取出Integer 转换成 double 显示
 */
public class MoneySerializer extends JsonSerializer<Integer> {
    @Override
    public void serialize(Integer in, JsonGenerator jsonGenerator, SerializerProvider provider)
            throws IOException {
        if (in != null) {
            Double temp = BigDecimal.valueOf(in.doubleValue()).divide(new BigDecimal(100)).doubleValue();
            DecimalFormat format = new DecimalFormat("##0.00");
            String result = format.format(temp);
            jsonGenerator.writeString(result);
        }
    }
}

package chagine.core.serialize.serialize;

import chagine.core.util.DateUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * 将时间戳序列化日期时间格式 HH:mm
 */

@Slf4j
public class LocalDateTime2ShortTime2FormatterSerializer extends JsonSerializer<LocalDateTime> {

    @Override
    public void serialize(LocalDateTime timestamp, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
        if (timestamp != null) {
            jsonGenerator.writeString(DateUtil.getSimpleShortTimeFormatter(timestamp));
        }
    }
}

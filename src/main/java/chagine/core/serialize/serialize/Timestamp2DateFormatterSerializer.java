package chagine.core.serialize.serialize;

import chagine.core.util.DateUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 将时间戳序列化日期格式 yyyy-MM-dd
 */
@Slf4j
public class Timestamp2DateFormatterSerializer extends JsonSerializer<Long> {

    @Override
    public void serialize(Long timestamp, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
        if (timestamp != null) {
            jsonGenerator.writeString(DateUtil.getDateFormatter(timestamp));
        }
    }
}

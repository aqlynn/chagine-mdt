package chagine.core.serialize.deserializer;

import chagine.core.util.DateUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.IOException;
import java.time.LocalDateTime;

/**
 * 反序列化时，将格式日期转换为时间戳
 */
@Slf4j
public class LongString2LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    @Override
    public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (!NumberUtils.isNumber(p.getText())) {
            return null;
        }
        return DateUtil.getLocalDateTimeByTimestamp(Long.parseLong(p.getText()));
    }

}

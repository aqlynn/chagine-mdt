package chagine.core.serialize.deserializer;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Date;

/**
 * Date对象序列化为时间戳
 */
public class DateFormatter2DateSerializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (StringUtils.isBlank(p.getText())) {
            return null;
        }
        return DateUtil.parse(p.getText(), chagine.core.util.DateUtil.DATE_FORMATTER).toJdkDate();
    }
}

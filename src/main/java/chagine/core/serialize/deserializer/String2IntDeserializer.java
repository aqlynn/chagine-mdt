package chagine.core.serialize.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.IOException;


/**
 * 反序列化时将String类型转成Integer类型
 */
@Slf4j
public class String2IntDeserializer extends JsonDeserializer<Integer> {

    @Override
    public Integer deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (!NumberUtils.isNumber(p.getText())) {
            return null;
        }
        try {
            return Integer.parseInt(p.getText());
        } catch (Exception ignore) {
            return null;
        }
    }

}

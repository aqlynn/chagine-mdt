package chagine.core.serialize.deserializer;

import chagine.core.ErrorCode;
import chagine.core.util.Assert;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.IOException;

/**
 * @author lyd
 * @date 2017/9/2
 */
@Slf4j
public class String2LongDeserializer extends JsonDeserializer<Long> {

    @Override
    public Long deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (!NumberUtils.isNumber(p.getText())) {
            return null;
        }
        return Long.parseLong(p.getText());
    }
}

//package chagine.core.serialize.deserializer;
//
//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.DeserializationContext;
//import com.fasterxml.jackson.databind.JsonDeserializer;
//import org.springframework.util.StringUtils;
//
//import java.io.IOException;
//
///**
// * Created by lyd on 2017/9/2.
// */
//public class StringJsonDeserializer extends JsonDeserializer<Long> {
//    @Override
//    public Long deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
//        if (StringUtils.isEmpty(p.getText()))
//            return null;
//        return Long.valueOf(p.getText());
//    }
//}

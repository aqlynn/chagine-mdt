package chagine.core.serialize.deserializer;

import chagine.core.util.DateUtil;
import cn.hutool.core.util.NumberUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * 反序列化时，将格式日期转换为时间戳
 */
@Slf4j
public class DateFormatter2TimestampDeserializer extends JsonDeserializer<Long> {

    @Override
    public Long deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (StringUtils.isBlank(p.getText())) {
            return null;
        }
        if (NumberUtil.isLong(p.getText())) {
            return Long.parseLong(p.getText());
        }
        return DateUtil.getTimestampByDateOrDateTime(p.getText());
    }

}

package chagine.core.serialize.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.apache.commons.lang.math.NumberUtils;

import java.io.IOException;
import java.util.Date;

/**
 * 时间戳反序列化为Date对象
 */
public class Timestamp2DateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String value = p.getText();
        Date date = null;
        if (NumberUtils.isDigits(value)) {
            long timestamp = Long.parseLong(value);
            date = new Date(timestamp * 1000);
        }
        return date;
    }

}

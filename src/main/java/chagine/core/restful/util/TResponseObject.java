package chagine.core.restful.util;

import chagine.core.ErrorCode;
import chagine.core.exception.ExceptionInfix;
import chagine.core.exception.SeeingException;
import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * 封装啦，反序列化不可以使用它
 * Created by wba on 2016/12/30.
 */
public class TResponseObject extends TResponse<Object> {

    /**
     * 自定义服务号前缀
     */
    @JsonIgnore
    private static String prefixcode = "";

    public static void setPrefixcode(String prefixcode) {
        TResponseObject.prefixcode = prefixcode;
    }

    public static TResponseObject success(Object data) {
        if (data instanceof TResponse) {
            return (TResponseObject) data;
        } else {
            TResponseObject restResponseObject = new TResponseObject();
            restResponseObject.setStatus(ErrorCode.Success);
            restResponseObject.setData(data);
            return restResponseObject;
        }
    }

    //    @Deprecated
    public static TResponseObject Success(Object data) {
        return success(data);
    }

    public static TResponseObject error(String code, String message) {
        TResponseObject restResponseObject = new TResponseObject();
        if (code.length() <= 2) {
            restResponseObject.setStatus(TResponseObject.prefixcode + ExceptionInfix.FILL_CODE + code);
        } else if (code.length() <= 4) {
            restResponseObject.setStatus(TResponseObject.prefixcode + code);
        } else {
            restResponseObject.setStatus(code);
        }
        restResponseObject.setMessage(message);
        return restResponseObject;
    }


    public static TResponseObject Error(String code, String message) {
        return error(code, message);
    }

    public static TResponseObject error(SeeingException ex) {
        TResponseObject restResponseObject = new TResponseObject();
        if (ex.getCode().length() <= 2) {
            restResponseObject.setStatus(TResponseObject.prefixcode + ExceptionInfix.FILL_CODE + ex.getCode());
        } else if (ex.getCode().length() <= 4) {
            restResponseObject.setStatus(TResponseObject.prefixcode + ex.getCode());
        } else {
            restResponseObject.setStatus(ex.getCode());
        }
        restResponseObject.setMessage(ex.getMessage());
        return restResponseObject;
    }


    public static TResponseObject Error(SeeingException ex) {
        return error(ex);
    }

}

package chagine.core.restful.util;

import chagine.core.ErrorCode;
import chagine.core.exception.ExceptionInfix;
import chagine.core.exception.SeeingException;
import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * 封装啦，反序列化不可以使用它
 * Created by wba on 2016/12/30.
 */
public class TResponseT<T> extends TResponse<T> {

    /**
     * 自定义服务号前缀
     */
    @JsonIgnore
    private static String prefixcode = "";

    public static void setPrefixcode(String prefixcode) {
        TResponseT.prefixcode = prefixcode;
    }

    public static <T> TResponseT<T> success(T data) {

        TResponseT<T> restResponseObject = new TResponseT<>();
        restResponseObject.setStatus(ErrorCode.Success);
        restResponseObject.setData(data);
        return restResponseObject;
    }

    //    @Deprecated
    public static <T> TResponseT<T> Success(T data) {
        return success(data);
    }

    public static <T> TResponseT<T> error(String code, String message) {
        TResponseT<T> restResponseObject = new TResponseT<>();
        if (code.length() <= 2) {
            restResponseObject.setStatus(TResponseT.prefixcode + ExceptionInfix.FILL_CODE + code);
        } else if (code.length() <= 4) {
            restResponseObject.setStatus(TResponseT.prefixcode + code);
        } else {
            restResponseObject.setStatus(code);
        }
        restResponseObject.setMessage(message);
        return restResponseObject;
    }


    public static <T> TResponseT<T> Error(String code, String message) {
        return error(code, message);
    }

    public static <T> TResponseT<T> error(SeeingException ex) {
        TResponseT<T> restResponseObject = new TResponseT<>();
        if (ex.getCode().length() <= 2) {
            restResponseObject.setStatus(TResponseT.prefixcode + ExceptionInfix.FILL_CODE + ex.getCode());
        } else if (ex.getCode().length() <= 4) {
            restResponseObject.setStatus(TResponseT.prefixcode + ex.getCode());
        } else {
            restResponseObject.setStatus(ex.getCode());
        }
        restResponseObject.setMessage(ex.getMessage());
        return restResponseObject;
    }


    public static <T> TResponseT<T> Error(SeeingException ex) {
        return error(ex);
    }

}

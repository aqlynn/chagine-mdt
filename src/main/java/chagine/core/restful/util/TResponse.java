package chagine.core.restful.util;


/**
 * 用于基本信息的序列化与反序列化
 * Created by wba on 2016/12/30.
 */
public class TResponse<DataType> extends HResponse<DataType> {

    public TResponse() {
        super();
    }

    @SuppressWarnings("unchecked")
    public TResponse(String status, DataType data, String message) {
        super(status, data, message);
    }

}

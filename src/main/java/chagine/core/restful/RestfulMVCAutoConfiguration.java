package chagine.core.restful;

import chagine.core.http.HttpAutoConfiguration;
import chagine.core.restful.mvc.RestfulMvcFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by 蔡邦超 on 2017/10/25.
 * About :
 */
@Slf4j
@Configuration
public class RestfulMVCAutoConfiguration implements BeanPostProcessor {

    public static final int RESTFUL_MVC_FILTER_ORDER = HttpAutoConfiguration.WEB_REQUEST_FILTER_ORDER + 10;

    @Bean
    public FilterRegistrationBean<RestfulMvcFilter> restfulMVCFilter() {
        log.info("===================FilterRegistrationBean======================");
        FilterRegistrationBean<RestfulMvcFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(new RestfulMvcFilter());
        filterRegistrationBean.setOrder(RESTFUL_MVC_FILTER_ORDER);
        return filterRegistrationBean;
    }

//    @Override
//    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
//        return bean;
//    }
//
//    @Override
//    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
//        Class<?> beanClass = bean.getClass();
//        RestController annotation = beanClass.getAnnotation(RestController.class);
//        if (annotation == null) {
//            return bean;
//        }
//
//        boolean haveDebounce = false;
//        Method[] methods = beanClass.getDeclaredMethods();
//
//        return bean;
//    }
}
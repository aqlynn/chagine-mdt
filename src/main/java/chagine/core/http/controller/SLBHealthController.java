package chagine.core.http.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * com.seeing.core.rpc.controller.SLBHealthController
 * Created by 王彬安（wba）on 2018/5/11.
 * 用来应付slb的健康检查
 */
@RestController
public class SLBHealthController {

    @GetMapping("/slb/health")
    public String health() {
        return "ok";
    }
}

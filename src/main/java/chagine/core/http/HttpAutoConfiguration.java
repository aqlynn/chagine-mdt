package chagine.core.http;

import chagine.core.http.filter.WebRequestFilter;
import chagine.core.http.service.ReturnNullMessageConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * com.seeing.core.rpc.RpcHttpAutoConfiguration
 * Created by 王彬安（wba）on 2018/5/11.
 */
@Configuration
@Slf4j
public class HttpAutoConfiguration {

    // restful-mvc 中还有过滤器，那个过滤器比这个晚
    public static final int WEB_REQUEST_FILTER_ORDER = 100;

    @Bean
    public FilterRegistrationBean<WebRequestFilter> currentInfoFilter() {
        FilterRegistrationBean<WebRequestFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(new WebRequestFilter());
        filterRegistrationBean.setOrder(WEB_REQUEST_FILTER_ORDER);
        return filterRegistrationBean;
    }


    @Bean
    public ReturnNullMessageConverter returnNullMessageConverter(ObjectMapper objectMapper) {
        log.info("==================returnNullMessageConverter============================");
        return new ReturnNullMessageConverter(objectMapper);
    }
}

package chagine.core.http.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * 杭州乾瑾科技
 *
 * @author 王彬安
 * @create 2018/8/8 12:40
 */
public class ReturnNullMessageConverter extends MappingJackson2HttpMessageConverter {

    public ReturnNullMessageConverter(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return super.supports(clazz);
    }

    @Override
    public boolean canWrite(Type type, Class<?> clazz, MediaType mediaType) {
        boolean canWrite = super.canWrite(type, clazz, mediaType);
        if (!canWrite && clazz == Object.class &&
//                (mediaType == null || mediaType == MediaType.APPLICATION_JSON) &&
                type instanceof Class && (Class) type == Object.class) {
            canWrite = true;
        }
        return canWrite;
    }

    @Override
    protected void writeInternal(Object o, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        super.writeInternal(o, outputMessage);
    }
}

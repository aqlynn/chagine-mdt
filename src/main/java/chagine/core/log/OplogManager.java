package chagine.core.log;

import java.util.HashMap;
import java.util.Map;

/**
 * create by wba on  2020/5/16
 * qq:43585061
 */
public class OplogManager {
    private static String serverCode;

    //需要监控的事件列表
    private static Map<String, OplogEvent> oplogEventMap = new HashMap<>();


    public static void setServerCode(String code) {
        serverCode = code;
    }

    public static String getServerCode() {
        return serverCode;
    }

    /**
     * 注册事件
     */
    public static void addOplogEvent(String uri, String opName, String method) {
        OplogEvent event = new OplogEvent();
        event.setUri(uri);
        event.setMethod(method);
        event.setOpName(opName);
        oplogEventMap.put(uri, event);
    }


    public static OplogEvent getOplogEvent(String uri) {
        return oplogEventMap.get(uri);
    }

}

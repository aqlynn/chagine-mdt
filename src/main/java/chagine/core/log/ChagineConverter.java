package chagine.core.log;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import chagine.core.current.CurrentRequestID;
import chagine.core.current.CurrentTenant;
import chagine.core.current.CurrentUser;

/**
 * SeeingConverter
 * Created by 王彬安（wba）on 2019/8/7.
 */
public class ChagineConverter extends ClassicConverter {

    /**
     * 第一个当前请求的ID，第二个租户ID,第三个用户ID
     *
     * @param event
     * @return
     */
    @Override
    public String convert(ILoggingEvent event) {

        //CurrentTenant.currentTenant() +
        return CurrentTenant.currentTenant() + "," + CurrentUser.currentUser() + "," + CurrentRequestID.currentRequestID();
    }
}

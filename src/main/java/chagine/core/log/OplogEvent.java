package chagine.core.log;

import lombok.Data;

/**
 * create by wba on  2020/5/16
 * qq:43585061
 * 操作日志事件
 */
@Data
public class OplogEvent {
    //请求的url
    private String uri;
    //方法
    private String method;
    //操作名称
    private String opName;
}

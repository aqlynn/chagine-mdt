
package chagine.core.log;

import chagine.redis.OplogRedisTemplate;
import chagine.redis.cache.OplogCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * create by wba on  2020/5/16
 * qq:43585061
 */
@Component
@Slf4j
public class OplogWritter {
    @Autowired
    private OplogRedisTemplate oplogRedisTemplate;

    public void write(OplogCache oplog) {
        try {

            OplogEvent event = OplogManager.getOplogEvent(oplog.getQueryUri());
            if (event != null) {
                log.info("oplog.getQueryUri()============= need log:" + oplog.getQueryUri() + "");
                oplog.setOpName(event.getOpName());
                oplog.setMethod(event.getMethod());
                oplog.setServerCode(OplogManager.getServerCode());
                oplogRedisTemplate.addOplog(oplog);
            } else {
                log.info("oplog.getQueryUri()=============dont need log:" + oplog.getQueryUri() + " drop");
            }

        } catch (Exception e) {
            log.info("===========OplogWritter=====" + e.getMessage() + " url:" + oplog.getQueryUri());
            e.printStackTrace();

        }

    }
}

package chagine.core.exception;

/**
 * com.seeing.core.exception.ExceptionInfix
 * Created by 王彬安（wba）on 2018/1/9.
 *
 * @author wba
 */
public class ExceptionInfix {

    private ExceptionInfix() {
    }

    /**
     * 补位 四位补到六位
     */
    public static final String FILL_CODE = "00";
}

package chagine.core.exception;

/**
 * com.seeing.core.exception.ExceptionPrefix
 * Created by 王彬安（wba）on 2018/1/9.
 */
public class ExceptionPrefix {

    private ExceptionPrefix() {
    }

    /**
     * 基础服务
     */
    public static final String CORE_SERVICE = "50";

    /**
     * 通知服务
     */
    public static final String NOTIFY_SERVICE = "51";

    /**
     * 文件资源服务
     */
    public static final String FILE_RESOURCE_SERVICE = "52";

    /**
     * 回调服务
     */
    public static final String CALLBACK_SERVICE = "53";

    /**
     * 支付服务
     */
    public static final String GTPAY_SERVICE = "54";

    /**
     * 公共服务
     */
    public static final String COMM_SERVICE = "11";

    public static final String WEB_SERVICE = "21";//WEBRESTful

    public static final String MOBILER_SERVICE = "31";//APIRESTful

    public static final String API_SERVICE = "40";//MOBILERESTful

}

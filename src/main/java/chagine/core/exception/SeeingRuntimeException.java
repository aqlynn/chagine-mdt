package chagine.core.exception;


/**
 * com.seeing.core.exception.SeeingRuntimeException
 * Created by 王彬安（wba）on 2018/6/14.
 */
public class SeeingRuntimeException extends RuntimeException {

    private SeeingException seeingException;

    /**
     * 自定义异常
     *
     * @param code
     * @param message 异常信息
     */
    public SeeingRuntimeException(String code, String message) {
        super(message);
        seeingException = new SeeingException(code, message);
    }

    public SeeingRuntimeException(SeeingException e) {
        super(e.getMessage());
        seeingException = e;
        setStackTrace(e.getStackTrace());
    }

    public String getCode() {
        return seeingException.getCode();
    }

    public void setCode(String code) {
        seeingException.setCode(code);
    }

    public SeeingException getSeeingException() {
        seeingException.setStackTrace(this.getStackTrace());
        return seeingException;
    }
}

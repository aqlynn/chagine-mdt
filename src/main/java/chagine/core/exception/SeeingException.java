package chagine.core.exception;

/**
 * com.guantong.seeing.core.exception.SeeingException
 * Created by 王彬安（wba）on 2017/8/8.
 */
public class SeeingException extends Exception {

    private static final StackTraceElement[] UNASSIGNED_STACK = new StackTraceElement[0];

    /**
     *
     */
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 自定义异常
     *
     * @param code
     * @param message 异常信息
     */
    public SeeingException(String code, String message) {
        super(message);
        this.code = code;
        setStackTrace(UNASSIGNED_STACK);
    }
}

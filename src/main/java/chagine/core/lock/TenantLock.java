package chagine.core.lock;

import chagine.core.current.CurrentTenant;
import chagine.core.lock.redis.RedisReentrantLock;
import chagine.core.lock.redis.util.SeeingLockRedisUtilFactory;

/**
 * @author chengsongping
 * @create 2018-03-21 11:33
 * 租户级锁，作用到当前租户上，锁的互质由 租户号 加 lockkey 来控制
 **/
public class TenantLock extends RedisReentrantLock {

    @Deprecated
    public TenantLock(String lockKey) {
        this(lockKey, RedisReentrantLock.DEFAULT_LOCK_EXPIRES);
    }

    /**
     * 创建一个租户级的锁
     *
     * @param lockKey    锁用的KEY
     * @param lockExpire 锁超时时间，单位millis
     * @deprecated 推荐 SeeingLockFactory
     */
    @Deprecated
    public TenantLock(String lockKey, long lockExpire) {
        this(lockKey, -1, lockExpire);
    }

    /**
     * 创建一个租户级的锁,支持Redis数据库的自定义
     *
     * @param lockKey    锁用的KEY
     * @param lockExpire 锁超时时间，单位millis
     * @param dbIndex    redis数据库定义
     * @deprecated 推荐 SeeingLockFactory
     */
    @Deprecated
    public TenantLock(String lockKey, long lockExpire, int dbIndex) {
        this(lockKey, dbIndex, lockExpire);
    }

    @SuppressWarnings("deprecation")
    private TenantLock(String lockKey, int dbIndex, long lockExpire) {
        super(CurrentTenant.currentTenant() + "_" + lockKey, lockExpire, SeeingLockRedisUtilFactory.getInstance().getSeeingLockRedisUtil(dbIndex));
    }

}

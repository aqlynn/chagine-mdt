package chagine.core.lock;

import chagine.core.lock.redis.RedisReentrantLock;
import chagine.core.lock.redis.util.SeeingLockRedisUtilFactory;

/**
 * @author chengsongping
 * @create 2018-03-21 10:39
 * 平台级的锁，作用在所有租户上，是最底线的锁，完全是由lockKey来控制
 **/
public class SystemLock extends RedisReentrantLock {

    @Deprecated
    public SystemLock(String lockKey) {
        this(lockKey, RedisReentrantLock.DEFAULT_LOCK_EXPIRES);
    }

    /**
     * 创建一个平台级的锁
     *
     * @param lockKey    锁用的KEY
     * @param lockExpire 锁超时时间，单位millis
     * @deprecated 推荐 SeeingLockFactory
     */
    @Deprecated
    public SystemLock(String lockKey, long lockExpire) {
        this(lockKey, lockExpire, -1);
    }

    /**
     * 创建一个平台级的锁，支持reids 数据库的自定义
     *
     * @param lockKey    锁用的KEY
     * @param lockExpire 锁超时时间，单位millis
     * @param dbIndex    reids 数据库
     * @deprecated 推荐 SeeingLockFactory
     */
    @Deprecated
    public SystemLock(String lockKey, long lockExpire, int dbIndex) {
        super(lockKey, lockExpire, SeeingLockRedisUtilFactory.getInstance().getSeeingLockRedisUtil(dbIndex));
    }
}

package chagine.core.lock.util;

import chagine.core.util.NetworkUtil;
import lombok.extern.slf4j.Slf4j;
//import sun.management.VMManagement;


/**
 * @author zzw
 */
@Slf4j
public class PlatformUtils {

    private PlatformUtils() {
    }

    /**
     * 获取localhost MAC地址
     *
     * @return
     * @throws Exception
     */
    public static String MACAddress() {
        try {
            return NetworkUtil.getMAC();
        } catch (Exception e) {
            log.error(e.getMessage());
            return "00";
        }
    }

    /**
     * 获取当前JVM 的进程ID
     *
     * @return
     */
    public static int JVMPid() {
//        try {
//            RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();
//            Field jvm = runtime.getClass().getDeclaredField("jvm");
//            jvm.setAccessible(true);
//            VMManagement mgmt = (VMManagement) jvm.get(runtime);
//            Method pidMethod = mgmt.getClass().getDeclaredMethod("getProcessId");
//            pidMethod.setAccessible(true);
//            int pid = (Integer) pidMethod.invoke(mgmt);
//            return pid;
//        } catch (Exception e) {
//            log.error(e.getMessage());
//            return -1;
//        }
        return -1;
    }


//    public static void main(String[] args) throws Exception {
//        System.out.println(MACAddress());
//    }
}

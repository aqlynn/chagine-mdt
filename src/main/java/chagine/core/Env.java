package chagine.core;

/**
 * Env
 * Created by 王彬安 on 2017/8/6.
 */
public class Env {

    /**
     * 令牌信息在请求头中的KEY
     */
    public static final String HEAD_TOKEN_KEY = "token";

    /**
     * 用户ID在请求头中的KEY
     */
    public static final String HEAD_USERID_KEY = "userid";

    /**
     * 请求头中请求ID的KEY
     */
    public static final String HEAD_REQUEST_ID_KEY = "requestid";

    /**
     * 请求头中请求时间的KEY
     */
    public static final String HEAD_REQUEST_TIME_KEY = "requesttime";

    /**
     * 客户端操作系统，包含操作系统与版本号
     */
    public static final String HEAD_OS_KEY = "os";

    /**
     * 当前设备名称，如iOS的手机设备名称
     */
    public static final String HEAD_DEVICE_KEY = "device";

    /**
     * 当前医生工号
     */
    public static final String HEAD_EMPLOYEE_NO_KEY = "employeeno";


    /**
     * 用于统计用户使用的浏览器的，由网关获取与填写
     */
    public static final String HEAD_USERAGENT_KEY = "basic-agent";

    /**
     * 用户获取ip
     */
    public static final String CLIENT_IP = "clientip";

    /**
     * 请求header，为了获取二级域名
     */
    public static final String HEAD_HOST_KEY = "host";

    /**
     * 租户
     */
    public static final String HEAD_TENANT_KEY = "tenantid";

    //GROUP_ID 和 患者ID
    public static final String HEAD_GROUPID_KEY = "groupid";
    public static final String HEAD_PATIENTID_KEY = "patientid";

    /**
     * 用户连击时返回的HTTP状态
     */
    public static final int DOUBLE_HIT_STATUS = 666;

}

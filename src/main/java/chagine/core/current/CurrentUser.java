package chagine.core.current;

/**
 * CurrentUser
 * 用于保存当前用户信息
 * Created by 王彬安（wba）on 2017/8/6.
 */
public class CurrentUser {

    public final static Long EMPTY_FLAG = -109L;

    private static final ThreadLocal<Long> userId = new ThreadLocal<>();

    public static void setCurrentUser(Long userId) {
        CurrentUser.userId.set(userId);
    }

    public static Long currentUser() {
        return userId.get();
    }
}

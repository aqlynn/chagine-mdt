package chagine.core.current;

/**
 * 用于保存当前租户ID的
 * Created by wba on 2017/1/6.
 */
public class CurrentTenant {

    public final static Long EMPTY_FLAG = -110L;

    private static final ThreadLocal<Long> tenant = new ThreadLocal<>();

    public static void setCurrentTenant(Long tenant) {
        CurrentTenant.tenant.set(tenant);
    }

    public static Long currentTenant() {
        return tenant.get();
    }
}

package chagine.core.current;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * 杭州乾瑾科技
 * <p>
 * Created by 王彬安 on 2019/5/11.
 */
@AllArgsConstructor
@Getter
@Setter
public class RequestContent {
    public RequestContent() {

    }

    /**
     * 请求的用户
     */
    private Long user;

    /**
     * 当前请求的租户
     */
    private Long tenant;

    /**
     * 请求的时间,单位毫秒
     */
    private Long time;

    /**
     * 请求操作的ID
     */
    private Long id;

    /**
     * 当前请求的主租户
     */
    private Long mainTenant;

    /**
     * 集团ID
     */
    private Long groupId;


    /**
     * 患者id
     */
    private Long patientId;


}

package chagine.core.current;

/**
 * create by wba on  2020/3/1
 * qq:43585061
 */
public class CurrentClientIp {
    private static final ThreadLocal<String> CLIENT_IP = new ThreadLocal<>();

    public static void setCurrentClientIp(String ip) {
        CurrentClientIp.CLIENT_IP.set(ip);
    }

    public static String currentClientIp() {
        return CurrentClientIp.CLIENT_IP.get();
    }
}

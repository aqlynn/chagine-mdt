package chagine.core.current;

/**
 * CurrentRequestID
 * Created by 王彬安（wba）on 2017/8/7.
 *
 * @author wba
 */
public class CurrentRequestID {

    public final static Long EMPTY_FLAG = -108L;

    private static final ThreadLocal<Long> requestId = new ThreadLocal<>();

    public static void setCurrentRequestID(Long requestId) {
        CurrentRequestID.requestId.set(requestId);
    }

    public static Long currentRequestID() {
        return requestId.get();
    }
}

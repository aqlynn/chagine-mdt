package chagine.core.current;

/**
 * 杭州乾瑾科技
 * <p>
 * 当前操作的统一接口
 *
 * @author 王彬安
 * @create 2018/12/30 12:27
 */
public class CurrentEnv {

    private CurrentEnv() {
    }

    private static final ThreadLocal<Long> mainTenaId = new ThreadLocal<>();

    private static final ThreadLocal<Long> groupId = new ThreadLocal<>();

    private static final ThreadLocal<Long> patientId = new ThreadLocal<>();

    public static void setUser(Long userId) {
        CurrentUser.setCurrentUser(userId);
    }

    public static Long user() {
        return CurrentUser.currentUser();
    }

    public static void setTenant(Long tenant) {
        CurrentTenant.setCurrentTenant(tenant);
    }

    public static Long currentTenant() {
        return CurrentTenant.currentTenant();
    }

    public static Long tenant() {
        return currentTenant();
    }

    public static void setRequestID(Long requestId) {
        CurrentRequestID.setCurrentRequestID(requestId);
    }

    public static Long requestID() {
        return CurrentRequestID.currentRequestID();
    }

    public static Long id() {
        return requestID();
    }

    /**
     * 请求的服务器时间，单位毫秒
     *
     * @param requestId
     */
    public static void setRequestTime(Long requestId) {
        CurrentRequestTime.setCurrentRequestTime(requestId);
    }

    public static Long requestTime() {
        return CurrentRequestTime.currentRequestTime();
    }

    public static Long time() {
        return requestTime();
    }


    public static void setMainTenaId(Long mainTenaId) {
        CurrentEnv.mainTenaId.set(mainTenaId);
    }

    public static Long mainTenaId() {
        return mainTenaId.get();
    }

    public static void setGroupId(Long groupId) {
        CurrentEnv.groupId.set(groupId);
    }

    public static Long groupId() {
        return groupId.get();
    }

    public static void setPatientId(Long patientId) {
        CurrentEnv.patientId.set(patientId);
    }

    public static Long patientId() {
        return patientId.get();
    }

    /**
     * 获得当前请求内容的数据结构
     *
     * @return 当前请求内容的数据结构
     */
    public static RequestContent getContent() {
        return new RequestContent(user(), tenant(), time(), id(), mainTenaId(), groupId(), patientId());
    }

    public static void setEmployeeNo(String employeeNo) {
        CurrentEmployeeNo.setCurrentEmployeeNo(employeeNo);
    }

    public static String employeeNo() {
        return CurrentEmployeeNo.currentEmployeeNo();
    }

    /**
     * 快速清空缓存
     */
    public static void clear() {
        setGroupId(null);
        setMainTenaId(null);
        setRequestID(null);
        setRequestTime(null);
        setTenant(null);
        setUser(null);
        setEmployeeNo(null);

    }
}

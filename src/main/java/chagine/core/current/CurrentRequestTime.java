package chagine.core.current;

/**
 * 杭州乾瑾科技
 *
 * @author 王彬安
 * @create 2018/12/30 12:26
 */
public class CurrentRequestTime {

    public final static Long EMPTY_FLAG = 0L;

    /**
     * 请求时间，单位毫秒
     */
    private static final ThreadLocal<Long> requestTime = new ThreadLocal<>();

    public static void setCurrentRequestTime(Long requestTime) {
        CurrentRequestTime.requestTime.set(requestTime);
    }

    public static Long currentRequestTime() {
        return requestTime.get();
    }

}

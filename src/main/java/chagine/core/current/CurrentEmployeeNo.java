package chagine.core.current;

/**
 * CurrentUser
 * 用于保存当前用户医生工号
 * Created by 王彬安（wba）on 2017/8/6.
 */
public class CurrentEmployeeNo {

    public final static Long EMPTY_FLAG = -121L;

    private static final ThreadLocal<String> employeeNo = new ThreadLocal<>();

    public static void setCurrentEmployeeNo(String userId) {
        CurrentEmployeeNo.employeeNo.set(userId);
    }

    public static String currentEmployeeNo() {
        return employeeNo.get();
    }
}

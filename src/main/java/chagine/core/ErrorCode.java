package chagine.core;

/**
 * ErrorCode
 * Created by 王彬安（wba）on 2017/8/4.
 */
public class ErrorCode {

    private ErrorCode() {
    }

    /**
     * 成功标志
     */
    public static final String Success = "000";

    /**
     * 租户号未配置
     */
    public static final String TenantMissed = "501";

    /**
     * 错误后两位--错误码
     */
    //参数验证异常,例如，不能为空的参数，传了空值。对WEBRESTful来说，对前端的验证出现的错误属于该类异常
    public static final String ILLEGALARGUMENT_EXCEPTION = "01";

    //业务异常,在业务操作时，根据传入的参数，与数据库比对后发现不符合业务规则，属于业务异常。比如，保存医嘱时，传入的就诊id无效或就诊id不属于对应病人id,属于业务异常。异常信息由message提供
    public static final String BUSINESS_EXCEPTION = "02";

    //发送邮件时，目标地址不存在
    public static final String EMAIL_ADRESS_EXCEPTION = "03";

    /**
     * http请求时发生的错误 目前包括404（找不到地址或get/post请求方式错误） /405（请求体参数类型等错误）
     */
    public static final String HTTP_ERROR_EXCEPTION = "04";

    //token已失效 401（认证失败）
    //token已失效
    public static final String TOKEN_UNABLE = "05";

    /**
     * 租户ID错误
     */
    public static final String TENANTID_ERROR_EXCEPTION = "06";

    /**
     * 正常租户登录失败
     */
    public static final String LOGIN_FAILURE = "07";

    /**
     * URL权限不足
     */
    public static final String URL_PERMISSION_DENIED = "08";

    /**
     * 登陆超出最大人数限制
     */
    public static final String OUTOFMAXUSER_EXCEPTION = "21";


    public static final String RUNNING_EXCEPTION = "50";

    //未知错误
    public static final String UNKNOWN_EXCEPTION = "99";

    /**
     * 业务异常，前要求前端进行页面刷新
     */
    public static final String BUSINESS_REFRESH_EXCEPTION = "07";

    /**
     * 在此期间，业务数据有发送更新，（如update时执行数据量与预期不符）
     */
    public static final String DATA_CHANGED = "10";

    //业务异常
    public static final int EXTERNAL_BUSINESS_EXCEPTION = 1;

    /**
     * 请求次数限制
     */
    public static final String QPS_EXCEEDS_LIMIT = "98";
}

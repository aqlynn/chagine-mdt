package chagine.redis.cache;

import lombok.Data;

/**
 * create by wba on  2020/5/16
 * qq:43585061
 */
@Data
public class OplogCache {

    private String clientIp;
    private String responseData;
    private String requestBody;
    private String queryString;
    private String queryUri;
    private Long requestId;
    private Long tenantId;
    private Long userId;
    private Long requestTime;
    private String method;
    //微服务服务代码
    private String serverCode;
    //操作名称
    private String opName;

}

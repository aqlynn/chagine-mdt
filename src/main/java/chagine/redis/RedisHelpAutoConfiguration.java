package chagine.redis;

import io.lettuce.core.resource.DefaultClientResources;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.data.redis.LettuceClientConfigurationBuilderCustomizer;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.boot.autoconfigure.data.redis.MyRedisConnectionConfiguration;

import java.net.UnknownHostException;
import java.util.List;

/**
 * com.seeing.redis.RedisHelpAutoConfiguration
 * Created by 王彬安（wba）on 2017/10/6.
 */
@Configuration
//@AutoConfigureAfter({CoreAutoConfiguration.class, RedisAutoConfiguration.class})
@AutoConfigureAfter({RedisAutoConfiguration.class})
@EnableConfigurationProperties(RedisProperties.class)
public class RedisHelpAutoConfiguration extends RedisAutoConfiguration {


//    public RedisHelpAutoConfiguration(RedisProperties properties) throws SeeingException {
//        String oldPass = properties.getPassword();
//        if (StringUtils.hasLength(oldPass)) {
//            throw new SeeingException(ErrorCode.BUSINESS_EXCEPTION, "redis password disallow write to configure file.");
//        }
//    }

    @EnableConfigurationProperties(RedisProperties.class)
    public static class RedisHelpConnectionConfiguration extends MyRedisConnectionConfiguration {

        public RedisHelpConnectionConfiguration(RedisProperties properties,
                                                ObjectProvider<RedisSentinelConfiguration> sentinelConfigurationProvider,
                                                ObjectProvider<RedisClusterConfiguration> clusterConfigurationProvider,
                                                ObjectProvider<List<LettuceClientConfigurationBuilderCustomizer>> builderCustomizers) {
            super(properties, sentinelConfigurationProvider, clusterConfigurationProvider, builderCustomizers);
//            this.properties = properties;
        }

        public LettuceConnectionFactory redisConnectionFactory() {
            try {
                DefaultClientResources resources = DefaultClientResources.create();
                return super.redisConnectionFactory(resources);
            } catch (UnknownHostException e) {
                throw new BeanCreationException("create redis connection factory failure", e);
            }
        }

//        private JedisConnectionFactory createJedisConnectionFactory() {
//            JedisPoolConfig poolConfig = (this.properties.getPool() != null
//                    ? jedisPoolConfig() : new JedisPoolConfig());
//
//            if (getSentinelConfig() != null) {
//                return new JedisConnectionFactory(getSentinelConfig(), poolConfig);
//            }
//            if (getClusterConfiguration() != null) {
//                return new JedisConnectionFactory(getClusterConfiguration(), poolConfig);
//            }
//            return new JedisConnectionFactory(poolConfig);
//        }
//
//        private JedisPoolConfig jedisPoolConfig() {
//            JedisPoolConfig config = new JedisPoolConfig();
//            RedisProperties.Pool props = this.properties.getPool();
//            config.setMaxTotal(props.getMaxActive());
//            config.setMaxIdle(props.getMaxIdle());
//            config.setMinIdle(props.getMinIdle());
//            config.setMaxWaitMillis(props.getMaxWait());
//            return config;
//        }
    }

//    @Bean
//    public RedisHelpConnectionConfiguration redisHelpConnectionConfiguration(RedisProperties properties,
//                                                                             ObjectProvider<RedisSentinelConfiguration> sentinelConfiguration,
//                                                                             ObjectProvider<RedisClusterConfiguration> clusterConfiguration) {
//        try {
//            String pass = null;
//            if (kms != null) {
//                pass = kms.getSecretData(SecretDataType.REDIS_PASSWORD);
//            }
//            if (StringUtils.isEmpty(pass)) {
//                throw new BeanCreationException("Get redis password failure.");
//            }
//
//            properties.setPassword(pass);
////            properties.getShardInfo().setPassword(pass);
////            jedisConnectionFactory.destroy();
////            jedisConnectionFactory.afterPropertiesSet();
//        } catch (Exception e) {
//            throw new BeanCreationException(e.getMessage());
//        }

//        return new RedisHelpConnectionConfiguration(properties, sentinelConfiguration, clusterConfiguration);
//    }

//    @Bean
//    public JedisConnectionFactory redisConnectionFactory(RedisHelpConnectionConfiguration redisConnectionConfiguration) throws UnknownHostException {
//        return redisConnectionConfiguration.redisConnectionFactory();
//    }
}

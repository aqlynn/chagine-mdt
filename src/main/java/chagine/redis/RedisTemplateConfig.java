package chagine.redis;



import chagine.redis.cache.OplogCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * Created by 王彬安 on 2019/8/23.
 */
@Configuration
public class RedisTemplateConfig {
    public final static String KEY_SEPARATOR = ":";

    private final static int BASIC_DATABASE = 7;
    private final static int DATA_ELEMENT_DATABASE = 15;

    @Bean(name = "OplogRedisTemplate")
    public RedisTemplate<String, OplogCache> oplogRedisTemplate(RedisHelpAutoConfiguration.RedisHelpConnectionConfiguration redisHelpConnectionConfiguration) {
        LettuceConnectionFactory factory = redisHelpConnectionConfiguration.redisConnectionFactory();
        factory.afterPropertiesSet();
        factory.setDatabase(BASIC_DATABASE);
        RedisTemplate<String, OplogCache> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        template.setKeySerializer(new StringRedisSerializer());

        template.setValueSerializer(new Jackson2JsonRedisSerializer<>(OplogCache.class));
        template.afterPropertiesSet();
        return template;
    }



}

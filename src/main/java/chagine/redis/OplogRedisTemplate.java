package chagine.redis;

import chagine.redis.cache.OplogCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * create by wba on  2020/3/4
 * qq:43585061
 * 验证码
 */
@Component
public class OplogRedisTemplate {
    private String REDIS_OPLOG_KEY = "OPLOG_LIST";

    @Autowired
    @Qualifier("OplogRedisTemplate")
    private RedisTemplate<String, OplogCache> redisTemplate;


//    // 读取
//    public Map getPayCacheMap() {
//        return redisTemplate.opsForHash().entries(REDIS_PAY_KEY);
//    }
//
//    // 删除
//    public void removePayCache(String tradeId) {
//        redisTemplate.opsForHash().delete(REDIS_PAY_KEY, tradeId);
//    }

    // 查询
    //得到队头
    public OplogCache getOplogCache() {
        return (OplogCache) redisTemplate.opsForList().leftPop(REDIS_OPLOG_KEY);
    }

    public void addOplog(OplogCache cache) {
        redisTemplate.opsForList().rightPush(REDIS_OPLOG_KEY, cache);
    }
}

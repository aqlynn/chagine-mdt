/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.aop.interceptor;

import chagine.core.config.AsyncMonitor;
import chagine.core.config.AsyncTaskConfig;
import chagine.core.current.CurrentEnv;
import chagine.core.util.SpringUtil;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.interceptor.AsyncExecutionAspectSupport;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.core.BridgeMethodResolver;
import org.springframework.core.Ordered;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.lang.Nullable;
import org.springframework.util.ClassUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;

/**
 * AOP Alliance {@code MethodInterceptor} that processes method invocations
 * asynchronously, using a given {@link AsyncTaskExecutor}.
 * Typically used with the {@link org.springframework.scheduling.annotation.Async} annotation.
 *
 * <p>In terms of target method signatures, any parameter types are supported.
 * However, the return type is constrained to either {@code void} or
 * {@code java.util.concurrent.Future}. In the latter case, the Future handle
 * returned from the proxy will be an actual asynchronous Future that can be used
 * to track the result of the asynchronous method execution. However, since the
 * target method needs to implement the same signature, it will have to return
 * a temporary Future handle that just passes the return value through
 * (like Spring's {@link org.springframework.scheduling.annotation.AsyncResult}
 * or EJB 3.1's {@code javax.ejb.AsyncResult}).
 *
 * <p>When the return type is {@code java.util.concurrent.Future}, any exception thrown
 * during the execution can be accessed and managed by the caller. With {@code void}
 * return type however, such exceptions cannot be transmitted back. In that case an
 * {@link AsyncUncaughtExceptionHandler} can be registered to process such exceptions.
 *
 * <p>As of Spring 3.1.2 the {@code AnnotationAsyncExecutionInterceptor} subclass is
 * preferred for use due to its support for executor qualification in conjunction with
 * Spring's {@code @Async} annotation.
 *
 * @author Juergen Hoeller
 * @author Chris Beams
 * @author Stephane Nicoll
 * @see org.springframework.scheduling.annotation.Async
 * @see org.springframework.scheduling.annotation.AsyncAnnotationAdvisor
 * @see org.springframework.scheduling.annotation.AnnotationAsyncExecutionInterceptor
 * @since 3.0
 */
public class AsyncExecutionInterceptor extends AsyncExecutionAspectSupport implements MethodInterceptor, Ordered {

    private AsyncMonitor asyncMonitor;

    /**
     * Create a new instance with a default {@link AsyncUncaughtExceptionHandler}.
     *
     * @param defaultExecutor the {@link Executor} (typically a Spring {@link AsyncTaskExecutor}
     *                        or {@link java.util.concurrent.ExecutorService}) to delegate to;
     *                        as of 4.2.6, a local executor for this interceptor will be built otherwise
     */
    public AsyncExecutionInterceptor(@Nullable Executor defaultExecutor) {
        super(defaultExecutor);
    }

    /**
     * Create a new {@code AsyncExecutionInterceptor}.
     *
     * @param defaultExecutor  the {@link Executor} (typically a Spring {@link AsyncTaskExecutor}
     *                         or {@link java.util.concurrent.ExecutorService}) to delegate to;
     *                         as of 4.2.6, a local executor for this interceptor will be built otherwise
     * @param exceptionHandler the {@link AsyncUncaughtExceptionHandler} to use
     */
    public AsyncExecutionInterceptor(@Nullable Executor defaultExecutor, AsyncUncaughtExceptionHandler exceptionHandler) {
        super(defaultExecutor, exceptionHandler);
    }

    public void afterSetProperties() {
        if (AsyncTaskConfig.ASYNC_TASK_LOG_SELECTION == 3) asyncMonitor = SpringUtil.getBean(AsyncMonitor.class);
    }

    /**
     * Intercept the given method invocation, submit the actual calling of the method to
     * the correct task executor and return immediately to the caller.
     *
     * @param invocation the method to intercept and make asynchronous
     * @return {@link Future} if the original method returns {@code Future}; {@code null}
     * otherwise.
     */
    @Override
    @Nullable
    public Object invoke(final MethodInvocation invocation) throws Throwable{
        afterSetProperties();
        Class<?> targetClass = (invocation.getThis() != null ? AopUtils.getTargetClass(invocation.getThis()) : null);
        Method specificMethod = ClassUtils.getMostSpecificMethod(invocation.getMethod(), targetClass);
        final Method userDeclaredMethod = BridgeMethodResolver.findBridgedMethod(specificMethod);

        AsyncTaskExecutor executor = determineAsyncExecutor(userDeclaredMethod);
        if (executor == null) {
            throw new IllegalStateException(
                    "No executor specified and no default executor set on AsyncExecutionInterceptor either");
        }

        // 加入request_id， 方便调试
        //——————————————————————————————————————————————————————————————————————
        Long requestId = CurrentEnv.getContent().getId();
        HttpServletRequest request = Optional
                .of(((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest())
                .orElseThrow(AsyncTaskConfig.REQUEST_NULL_POINTER_EXCEPTION_SUPPLIER);

        if (AsyncTaskConfig.ASYNC_TASK_LOG_SELECTION == 2)
            AsyncTaskConfig.printTaskStarted(userDeclaredMethod, requestId);
        //——————————————————————————————————————————————————————————————————————

        Callable<Object> task =
                        //1.TaskDecorator
                        AsyncTaskConfig.ASYNC_TASK_LOG_SELECTION == 1
                        ? () -> {
                    // 异步线程添加【request_id】和【request对象】
                    CurrentEnv.setRequestID(requestId);
                    try {
                        Object result = invocation.proceed();
                        if (result instanceof Future) {
                            return ((Future<?>) result).get();
                        }
                    } catch (ExecutionException ex) {
                        handleError(ex.getCause(), userDeclaredMethod, invocation.getArguments());
                    } catch (Throwable ex) {
                        handleError(ex, userDeclaredMethod, invocation.getArguments());
                    } finally {
                        CurrentEnv.setRequestID(null);
                    }
                    return null;
                }
                        //2.AsyncUncaughtExceptionHandler
                        : AsyncTaskConfig.ASYNC_TASK_LOG_SELECTION == 2
                        ? () -> {
                    // 异步线程添加【request_id】和【request对象】
                    CurrentEnv.setRequestID(requestId);
                    AsyncTaskConfig.RequestContext.set(request);
                    try {
                        Object result = invocation.proceed();
                        if (result instanceof Future) {
                            return ((Future<?>) result).get();
                        }
                    } catch (ExecutionException ex) {
                        handleError(ex.getCause(), userDeclaredMethod, invocation.getArguments());
                    } catch (Throwable ex) {
                        handleError(ex, userDeclaredMethod, invocation.getArguments());
                    } finally {
                        CurrentEnv.setRequestID(null);
                        AsyncTaskConfig.RequestContext.reset();
                    }
                    return null;
                }
                        //3.AsyncMonitor
                        : () -> {
                    // 异步线程添加【request_id】和【task对象】
                    CurrentEnv.setRequestID(requestId);
                            AsyncTaskConfig.RequestContext.set(request);
                    AsyncMonitor.AsyncTaskInfo taskInfo = asyncMonitor.createTask(requestId, userDeclaredMethod);
                    try {
                        taskInfo.setStatus(AsyncMonitor.AsyncTaskStatusEnum.RUNNING);
                        Object result = invocation.proceed();
                        if (result instanceof Future) {
                            return ((Future<?>) result).get();
                        }
                    } catch (ExecutionException ex) {
                        asyncMonitor.submitFailedTask(taskInfo);
                        handleError(ex.getCause(), userDeclaredMethod, invocation.getArguments());
                    } catch (Throwable ex) {
                        asyncMonitor.submitFailedTask(taskInfo);
                        handleError(ex, userDeclaredMethod, invocation.getArguments());
                    } finally {
                        asyncMonitor.submitSuccessTask(taskInfo);
                        AsyncTaskConfig.RequestContext.reset();
                        CurrentEnv.setRequestID(null);
                    }
                    return null;
                };

        return doSubmit(task, executor, invocation.getMethod().getReturnType());
    }

    /**
     * This implementation is a no-op for compatibility in Spring 3.1.2.
     * Subclasses may override to provide support for extracting qualifier information,
     * e.g. via an annotation on the given method.
     *
     * @return always {@code null}
     * @see #determineAsyncExecutor(Method)
     * @since 3.1.2
     */
    @Override
    @Nullable
    protected String getExecutorQualifier(Method method) {
        return null;
    }

    /**
     * This implementation searches for a unique {@link org.springframework.core.task.TaskExecutor}
     * bean in the context, or for an {@link Executor} bean named "taskExecutor" otherwise.
     * If neither of the two is resolvable (e.g. if no {@code BeanFactory} was configured at all),
     * this implementation falls back to a newly created {@link SimpleAsyncTaskExecutor} instance
     * for local use if no default could be found.
     *
     * @see #DEFAULT_TASK_EXECUTOR_BEAN_NAME
     */
    @Override
    @Nullable
    protected Executor getDefaultExecutor(@Nullable BeanFactory beanFactory) {
        Executor defaultExecutor = super.getDefaultExecutor(beanFactory);
        return (defaultExecutor != null ? defaultExecutor : new SimpleAsyncTaskExecutor());
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}

package org.springframework.boot.autoconfigure.data.redis;

import io.lettuce.core.RedisClient;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.data.redis.LettuceClientConfigurationBuilderCustomizer;
import org.springframework.boot.autoconfigure.data.redis.LettuceConnectionConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;

import java.util.List;

/**
 * @author 王彬安
 * @create 2019-01-16 14:50
 */
@Configuration
@ConditionalOnClass(RedisClient.class)
public class MyRedisConnectionConfiguration extends LettuceConnectionConfiguration {
    public MyRedisConnectionConfiguration(RedisProperties properties,
                                          ObjectProvider<RedisSentinelConfiguration> sentinelConfigurationProvider,
                                          ObjectProvider<RedisClusterConfiguration> clusterConfigurationProvider,
                                          ObjectProvider<List<LettuceClientConfigurationBuilderCustomizer>> builderCustomizers) {
        super(properties, sentinelConfigurationProvider, clusterConfigurationProvider, builderCustomizers);
    }
}
